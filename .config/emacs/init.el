;;; init.el --- Init file -*- lexical-binding: t -*-

;;
;; :tocdepth: 3
;;
;; %%%%%%%%%%%%%%%%%%%%%%
;; Emacs (Evil) Vim Style
;; %%%%%%%%%%%%%%%%%%%%%%
;;

;; Set the default font to 'MY_MONO_FONT' if available.
;; This cannot run in `early-init.el', so run first here.
(when (display-graphic-p)
  (let ((my-mono-font (or (getenv "MY_MONO_FONT") "")))
    (unless (string-equal my-mono-font "")
      (let ((my-mono-font-no-attrs (car (split-string my-mono-font ":"))))
        (cond
         ((find-font (font-spec :name my-mono-font-no-attrs))
          (set-face-attribute 'default nil :font my-mono-font))
         (t
          (message (format "'MY_MONO_FONT' (%s) not found!" my-mono-font))))))))


;; Useful for package development.
(defun printf (&rest args)
  (princ (apply #'format args) #'external-debugging-output))

(defmacro with-timer (title &rest forms)
  "Run the given FORMS, counting the elapsed time.
A message including the given TITLE and the corresponding elapsed
time is displayed."
  (declare (indent 1))
  (let ((nowvar (make-symbol "now"))
        (body
         `(progn
            ,@forms)))
    `(let ((,nowvar (current-time)))
       (printf "%s..." ,title)
       (prog1 ,body
         (let ((elapsed (float-time (time-subtract (current-time) ,nowvar))))
           (printf "%s... done (%.3fs)\n" ,title elapsed))))))

(setq native-comp-jit-compilation-deny-list nil)

;; ----------------------------------------------------------------------------
;; Custom Override Variables
;; #########################
;;
;; See:
;; https://www.reddit.com/r/emacs/comments/asil1y/batch_processing_and_printing_to_stdout_using/
;;

;; For native-comp branch.
(when (fboundp 'native-compile-async)
  (setq comp-deferred-compilation t)
  (setq comp-deferred-compilation-black-list '()))

;; ----------------------------------------------------------------------------
;; Application Options
;; ###################

;; handy troubleshooting
; (toggle-debug-on-error)

;; Disable toolbar.
(tool-bar-mode -1)
;; Disable menu.
(menu-bar-mode -1)
;; Disable scroll-bar.
(scroll-bar-mode -1)
;; Startup message.
(defun display-startup-echo-area-message ()
  (message ""))

;; Disable frames resizing implicitly. Why?
;; Resizing the Emacs frame can be a terribly expensive part of changing the font.
;; By inhibiting this,
;; we easily halve startup times with fonts that are larger than the system default.
(setq frame-inhibit-implied-resize t)

;; No blinking.
(blink-cursor-mode 0)

;; Window Title.
;; buffer name, modified status, frame name (short & simple).
(setq-default frame-title-format "emacs %f %&")

;; ----------------
;; UTF-8 Everywhere
;; ================
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; ------------------------
;; Misc Application Options
;; ========================

(setq select-enable-primary nil)
(setq select-enable-clipboard nil)
(setq x-select-enable-primary nil)
(setq x-select-enable-clipboard nil)
(setq xclip-select-enable-clipboard nil)

;; Show absolute line numbers even when narrowing.
(setq-default display-line-numbers-widen t)

;; Generally speaking, it's much nicer to sort find output.
(setq find-program (concat user-emacs-directory "bin/find_sorted"))

;; Don't group undo steps.
(fset 'undo-auto-amalgamate 'ignore)

;; Increase undo limits, to be *functionally* unlimited.
(setq undo-limit 67108864) ;; 64mb
(setq undo-strong-limit 100663296) ;; x 1.5 (96mb)
(setq undo-outer-limit 1006632960) ;; x 10 (960mb), (Emacs uses x100), but this seems too high.

;; Garbage-collect on focus-out, Emacs should feel snappier.
(add-function :after after-focus-change-function
              (lambda ()
                (unless (frame-focus-state)
                  (garbage-collect))))

(add-hook 'window-setup-hook #'delete-other-windows)

;; Maximize on Startup
;; note: this is a workaround for flickering on font resize with a
;; tiling window manager because Emacs thinks it can resize the window.
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))

;; allow y/n
(defalias 'yes-or-no-p 'y-or-n-p)

;; Allow region actions without being asked.
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; allow `narrow-to-region' (without being asked first).
(put 'narrow-to-region 'disabled nil)

(add-to-list 'load-path (concat user-emacs-directory "local"))

(when (display-graphic-p)
  ;; Use the default font for fixed-pitch text, otherwise changing the default font
  ;; leaves explicitly fixed-pitch text as-is (in markdown for e.g.).

  (custom-set-faces

   '(fixed-pitch ((t :inherit font-lock-string-face)))

   ;; Use inset boxes, don't get in the way of the showing the active region.
   '(lazy-highlight
     ((t
       (:box
        (:line-width (-1 . -1) :color "#808080")
        :foreground unspecified
        :background unspecified))))
   '(isearch
     ((t
       (:box
        (:line-width (-1 . -1) :color "#E0E0E0")
        :foreground unspecified
        :background unspecified))))))

;; ~~~~~~~~~~
;; No Backups

;; Disable backup.
(setq backup-inhibited t)
;; Disable auto save.
(setq auto-save-default nil)

(setq backup-directory-alist `(("." . "/tmp/emacs-backup")))

;; Show text instead of popups.
(setq use-dialog-box nil)

;; Shared with vim.
(setq ispell-personal-dictionary "~/.vim/spell/en.utf-8.add")

;; ~~~~~~~~~~~
;; Compilation

;; Always reuse existing compilation window.
(push '("\\*compilation\\*" . (nil (reusable-frames . t))) display-buffer-alist)
;; Scroll to first error.
(setq compilation-scroll-output 'first-error)


;; Don't say anything on mode-line mouseover.
(setq mode-line-default-help-echo nil)
;; Don't show where buffer starts/ends.
(setq indicate-buffer-boundaries nil)
;; Don't show empty lines.
(setq indicate-empty-lines nil)
;; Keep cursors and highlights in current window only.
(setq cursor-in-non-selected-windows nil)
;; Don't highlight inactive windows.
(setq highlight-nonselected-windows nil)
;; Disable bidirectional text support for slight performance bonus.
(setq bidi-display-reordering nil)

;; Don't blink-too distracting.

(setq blink-matching-paren nil)
(setq uniquify-buffer-name-style nil)
(setq visible-cursor nil)

;; Quiet warning, see:
;; https://github.com/syl20bnr/spacemacs/issues/192
(setq ad-redefinition-action 'accept)

;; Don't show buffer list on startup.
;; If you open multiple files, you can manually switch between them!
(setq inhibit-startup-buffer-menu t)

;; Wide when over tabs (not important, just slight preference)
(setq x-stretch-cursor t)
;; Hide cursor while typing.
(setq make-pointer-invisible t)

;; Never split the window (nice for rtags and compile errors).
;; https://stackoverflow.com/a/3389904/432509
(setq split-width-threshold nil)

;; Don't put two spaces after full-stop.
(setq sentence-end-double-space nil)

;; ---------
;; Scrolling
;; ---------
;;
;; Emacs redraw while scrolling tends to lazy-update,
;; at times this can *"get behind"*, causing annoying lags.
;; These settings favor immediate updates.

;; Scroll N lines to screen edge.
(setq scroll-margin 2)

(defconst my-scroll-margin-recenter-div 3)

(defconst my-num-processors (num-processors))
;; Avoid using too much memory.
(defconst my-num-processors-limited (/ my-num-processors 2))

;; Only 'jump' when moving this far off the screen
;; scroll-conservatively 10000

;; Ensure when we move outside the screen we always recenter
;; (less hassle than attempting to make all jumping commands call recenter)
(setq scroll-conservatively scroll-margin)

;; Keyboard scroll one line at a time.
(setq scroll-step 1)
;; Mouse scroll N lines.
(setq mouse-wheel-scroll-amount '(6 ((shift) . 1)))
;; Don't accelerate scrolling.
(setq mouse-wheel-progressive-speed nil)
;; Don't use timer when scrolling (a little less overhead).
(setq mouse-wheel-inhibit-click-time nil)

;; Select symbols instead of words.
(setq mouse-1-double-click-prefer-symbols t)

;; Preserve line/column (nicer page up/down).
(setq scroll-preserve-screen-position t)
;; Move the cursor to top/bottom even if the screen is viewing top/bottom (for page up/down).
(setq scroll-error-top-bottom t)

;; Center after going to the next error.
(setq next-error-recenter (quote (4)))

;; Always redraw immediately when scrolling,
;; more responsive and doesn't hang!
;; http://emacs.stackexchange.com/a/31427/2418
(setq fast-but-imprecise-scrolling nil)

;; Delay while in insert mode.
(with-eval-after-load 'evil
  (add-hook 'evil-insert-state-entry-hook (lambda () (setq jit-lock-defer-time 0.3456789)) nil t)
  (add-hook 'evil-insert-state-exit-hook (lambda () (setq jit-lock-defer-time 0)) nil t))

;; Font locking settings
(cond
 (nil
  ;; Lazy font locking.
  (setq jit-lock-defer-time 0.25)
  (setq jit-lock-contextually t))

 (nil
  (setq jit-lock-defer-time 0.0)
  (setq jit-lock-contextually t)))

;; Avoid prompt, just follow symbolic-links.
(setq vc-follow-symlinks t)
;; Needed so symlinks can be used & ignored by `vc-diff' related commands.
(setq vc-git-diff-switches (list "--ignore-submodules=all"))

;; Ensure scrolling always moves at least one line, needed for overlays that add lines
;; to the document contents (such as doc-string overlays).
(with-eval-after-load 'mwheel
  (defun mwheel-scroll-at-least-one-line (old-fn event &optional arg)
    (interactive (list last-input-event current-prefix-arg))
    (let* ((win (posn-window (event-start event)))
           (scroll-pos-prev (window-start win)))
      (prog1 (apply old-fn event arg)
        (when (eq scroll-pos-prev (window-start win))
          (let ((event-sym (car event)))
            (cond
             ((member event-sym (list 'wheel-up 'double-wheel-up 'triple-wheel-up))
              (save-excursion
                (goto-char scroll-pos-prev)
                (when (zerop (forward-line -1))
                  (set-window-start win (line-beginning-position)))))
             ((member event-sym (list 'wheel-down 'double-wheel-down 'triple-wheel-down))
              (save-excursion
                (goto-char scroll-pos-prev)
                (when (zerop (forward-line 1))
                  (set-window-start win (line-beginning-position)))))))))))
  (advice-add 'mwheel-scroll :around #'mwheel-scroll-at-least-one-line))

;; ----------------
;; Platform (Linux)
;; ================

(cond
 (t
  (setq select-enable-clipboard t)
  ;; Treat clipboard input as UTF-8 string first; compound text next, etc.
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))
  ;; Selecting sets primary clipboard.
  ;; NOTE: don't use this because it it's impossible to select then paste.
  (setq select-enable-primary nil))
 (nil
  (setq select-enable-clipboard nil)))


;; -----
;; IMenu
;; -----

(with-eval-after-load 'imenu

  ;; Recenter buffer after jumping.
  (add-hook
   'imenu-after-jump-hook
   (lambda () (recenter (max scroll-margin (/ (window-height) my-scroll-margin-recenter-div)))))

  (setq-default imenu-auto-rescan t)
  (setq-default imenu-auto-rescan-maxout (* 1024 1024))
  (setq-default imenu--rescan-item '("" . -99)))


;; ----
;; Term
;; ----

(with-eval-after-load 'term
  (defface term '((t (:foreground "#AAAAAA" :background "#000000" :extend t)))
    "Term font"
    :group 'basic-faces)

  (defun my-term-handle-exit (&optional process-name msg)
    (message "%s | %s" process-name msg)
    (kill-buffer (current-buffer)))

  (advice-add 'term-handle-exit :after 'my-term-handle-exit))

;; ---------
;; XRef Mode
;; ---------

(with-eval-after-load 'xref
  (add-hook
   'xref--xref-buffer-mode-hook
   (lambda ()
     (hl-line-mode)

     (when local-cfg/use-evil
       (evil-set-initial-state 'xref--xref-buffer-mode 'emacs))

     (define-key
      xref--xref-buffer-mode-map (kbd "j")
      (lambda ()
        (interactive)
        (xref--search-property 'xref-item)))
     (define-key
      xref--xref-buffer-mode-map (kbd "k")
      (lambda ()
        (interactive)
        (xref--search-property 'xref-item t)))

     (define-key xref--xref-buffer-mode-map (kbd "C-j") 'xref-next-line)
     (define-key xref--xref-buffer-mode-map (kbd "C-k") 'xref-prev-line)

     (define-key xref--xref-buffer-mode-map (kbd "C-l") 'xref-show-location-at-point)
     (define-key xref--xref-buffer-mode-map (kbd "RET") 'xref-quit-and-goto-xref)
     (define-key xref--xref-buffer-mode-map (kbd "M-RET") 'xref-quit-and-goto-xref))))


;; ----------------------------------------------------------------------------
;; Local Options
;; #############
;;
;; Options we may want to tweak within this configuration.
;; These options can cause conflicts, allow to easily switch off!

;; -------
;; Display
;; =======

(defconst local-cfg/use-evil nil)
(defconst local-cfg/use-bray t)

;; Highlight the current line.
(defvar local-cfg/use-hline nil)
;; Highlight the current block.
(defvar local-cfg/use-hl-block nil)
;; Don't draw line highlight before indentation.
(defvar local-cfg/use-hline-indent nil)
;; Show indentation guides.
(defvar local-cfg/use-highlight-indent-hig t) ;; highlight-indent-mode
(defvar local-cfg/use-highlight-indent-vim t) ;; visual-indent-mode
(defvar local-cfg/use-highlight-indent-scope t) ;; hl-indent-scope-mode
;; Show line-numbers (toggle with key shortcuts)
(defvar local-cfg/use-linum nil)
;; Color numbers (emacs doesn't do this by default)
(defvar local-cfg/use-number-color t)
;; Custom mode-line (no left-right align)
(defvar local-cfg/use-custom-mode-line t)
;; Show spaces and tabs
(defvar local-cfg/use-show-whitespace nil)
(defvar local-cfg/use-which-key t)
(defvar local-cfg/use-scroll-on-jump t)

(defvar local-cfg/evil-keyboard-remap t)
(defvar local-cfg/evil-surround t)

;; IMenu side-bar (nice but can be slow).
(defvar local-cfg/use-imenu-sidebar nil)
;; Highlight-symbol mode.
(defvar local-cfg/use-highlight-idle-mode t)
(defvar local-cfg/use-jit-lock-stealth t)

;; --------
;; Behavior
;; ========

;; Support toggle as a global
(defvar local-cfg/use-ispell t)
;; Disable as storing previous history can lock up emacs
;; (rarely but it's very annoying when it does).
(defvar local-cfg/use-gui-clipboard-history nil)
;; Check spelling for on-screen text.
(defvar local-cfg/use-spelling-flyspell nil)
(defvar local-cfg/use-spelling-flyspell-visible nil) ;; own extension to flyspell.
(defvar local-cfg/use-spelling-spell-fu t)

(defvar local-cfg/use-treesitter nil)

(defvar local-cfg/use-c-comment-pkg t)
(defvar local-cfg/use-comment-keywords t)


;; use only one of these!
(defvar local-cfg/use-ivy t)
(defvar local-cfg/use-vertico nil)

(defvar local-cfg/use-neotree t)
;; `lsp-treemacs' seems interesting, but currently not that useful as the outliner
;; doesn't follow the cursor.
(defvar local-cfg/use-treemacs nil)

(defvar local-cfg/use-company t)
(defvar local-cfg/use-company-preview nil)
(defvar local-cfg/use-git-gutter nil)
(defvar local-cfg/use-diff-hl t)

;; use only one of these!
(defvar local-cfg/use-search-evil t)
(defvar local-cfg/use-search-isearch nil)

;; support folding code-blocks
(defvar local-cfg/use-code-folding nil)

;; interactive scroll.
(defvar local-cfg/use-scroll-on-drag t)

;; evil-mode default undo
(defvar local-cfg/use-undo-emacs nil)
(defvar local-cfg/use-undo-fu t)
(defvar local-cfg/use-undo-fu-session t)

(defvar local-cfg/use-magit t)

;; Use vc-relative buffer name
(defvar local-cfg/use-buffer-name-vc-relative t)

;; Options are: [`oblivion', `inkpot']
;; (defvar local-cfg/theme 'oblivion)
(defvar local-cfg/theme 'inkpot)

;; --------------------
;; Language Integration
;; ====================

(defconst local-cfg/use-lsp nil)
(defconst local-cfg/use-lsp-ccls nil) ;; Instead of `clangd'.

(defconst local-cfg/use-eglot t)
(defconst local-cfg/use-eglot-ccls nil) ;; Instead of `clangd'.

(defvar local-cfg/use-yasnippet t)
(defvar local-cfg/use-eldoc-mode nil)


(defvar local-cfg/use-dap t)

;; (eval-when-compile
;;   (setq local-cfg/use-hl-block t))

(when local-cfg/use-custom-mode-line
  (defun my-mode-line-group-number (str size char)
    "Format NUM as string grouped to SIZE with CHAR."
    (let ((pt (length str)))
      (while (> pt size)
        (setq
         str (concat (substring str 0 (- pt size)) char (substring str (- pt size)))
         pt (- pt size)))
      str))

  (defconst my-mode-line-location-no-column
    (list
     ;; Right align.
     "" 'mode-line-format-right-align
     '(:eval
       (format "%4s      %6s%%"
               (my-mode-line-group-number (format-mode-line "%l") 3 ",")
               (format-mode-line "%p")))))

  (defconst my-mode-line-location
    (list
     ;; Right align.
     'mode-line-format-right-align
     '(:eval
       (format "%4s %%4c %6s%%"
               (my-mode-line-group-number (format-mode-line "%l") 3 ",")
               (format-mode-line "%p")))))

  (defconst my-mode-line-location-with-jit-lock-stealth
    (list
     ;; Right align.
     'mode-line-format-right-align
     '(:eval
       ;; For better readability, padded "%6l %4c %p".
       (format "%s %4s %%4c %6s%%"
               jit-lock-stealth-progress-info
               (my-mode-line-group-number (format-mode-line "%l") 3 ",")
               (format-mode-line "%p"))))))

;; ----------------------------------------------------------------------------
;; Packages
;; ########

;; (setq use-package-verbose t)

(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
  (add-to-list 'package-archives '("gnu-devel" . "https://elpa.gnu.org/devel/") t))

;; Initialize when non-interactive so other commands succeed.
(when noninteractive
  (require 'package)
  (package-initialize))

;; This is only needed once, near the top of the file
;; (eval-when-compile
;;   (require 'evil)
;;   (require 'evil-states)
;;   (require 'ediff)
;;   (require 'use-package))

(setq use-package-always-ensure t)
(setq use-package-always-defer t)

;; Used so we can update from the command line.
(when noninteractive
  (use-package package-utils))

;; (use-package treesit-auto
;;   :config
;;   (add-to-list 'treesit-auto-fallback-alist '(bash-ts-mode . sh-mode))
;;   (setq treesit-auto-install 'prompt))
;; (global-treesit-auto-mode)

;; --------------
;; Local Packages
;; ==============

(add-to-list 'load-path (concat user-emacs-directory "i42-misc"))

(use-package i42-region-strip-indentation
  :commands (i42-region-strip-indentation)
  :ensure nil)

(use-package i42-save-buffer-always
  :commands (i42-save-buffer-always)
  :ensure nil)

(use-package i42-close-all-other-windows
  :commands (i42-close-all-other-windows)
  :ensure nil)

(use-package i42-doc-jump-section
  :commands (i42-doc-jump-section)
  :ensure nil)

(use-package i42-insert-surround-word-or-region
  :commands
  (i42-insert-surround-word-or-region-quote-backtick
   i42-insert-surround-word-or-region-quote-single
   i42-insert-surround-word-or-region-quote-double
   i42-insert-surround-word-or-region-bracket-parens
   i42-insert-surround-word-or-region-bracket-square
   i42-insert-surround-word-or-region-bracket-brace)
  :ensure nil)

(use-package i42-mouse-yank-secondary
  :commands (i42-mouse-yank-secondary-select-or-word-at-cursor i42-mouse-yank-secondary-and-deselect)
  :ensure nil)

(use-package i42-recomplete-contextual
  :commands (i42-recomplete-contextual i42-recomplete-contextual-case-cycle)
  :ensure nil)

(use-package i42-diff-revert-hunks
  :commands (i42-diff-revert-hunks-at-point i42-diff-revert-hunks-all)
  :ensure nil)

(use-package i42-accum-clipboard
  :commands (i42-accum-clipboard-cut i42-accum-clipboard-paste)
  :ensure nil)

(use-package i42-diff-at-point-with-other-rev
  :commands (i42-diff-at-point-with-other-rev)
  :ensure nil)

(use-package i42-toggle-display-line-numbers
  :commands (i42-toggle-display-line-numbers i42-toggle-display-line-numbers-relative)
  :ensure nil)

(use-package i42-scroll-and-clamp
  :commands (i42-scroll-and-clamp-up-command i42-scroll-and-clamp-down-command)
  :ensure nil)

(use-package i42-transpose-structured
  :commands (i42-transpose-structured-forward i42-transpose-structured-backward)
  :ensure nil)

(use-package i42-comment-multi-line-toggle
  :commands (i42-comment-multi-line-toggle)
  :ensure nil)

(use-package i42-backspace-whitespace-to-tab-stop
  :commands (i42-backspace-whitespace-to-tab-stop)
  :ensure nil)

(use-package i42-delete-surround-at-point
  :commands (i42-delete-surround-at-point i42-delete-surround-at-point-with-bracket-before)
  :ensure nil)

(use-package i42-path-visibility-setup
  :commands (i42-path-visibility-setup)
  :ensure nil)

(use-package i42-sort-line-in-block
  :commands (i42-sort-line-in-block)
  :ensure nil)

(use-package i42-imenu-cycle
  :commands (i42-imenu-cycle-next i42-imenu-cycle-prev)
  :config (setq i42-imenu-cycle-recente-div my-scroll-margin-recenter-div)
  :ensure nil)

(use-package i42-neotree-project-dir-toggle
  :commands (i42-neotree-project-dir-toggle)
  :ensure nil)

(use-package i42-scratch-buffer-from-file
  :commands (i42-scratch-buffer-from-file)
  :ensure nil)

;; -----
;; Theme
;; =====

(cond
 ((eq local-cfg/theme 'inkpot)
  (setq inkpot-theme-use-box nil)
  (setq inkpot-theme-use-black-background nil)
  ;; (use-package inkpot-theme)
  (add-to-list 'custom-theme-load-path "/src/emacs/inkpot-theme")
  (load-theme 'inkpot t)

  ;; Modify so they don't look so similar to selection.
  (custom-set-faces
   '(show-paren-match
     ((t
       (:box
        (:line-width (-2 . -2) :color "#4e4e8f")
        :foreground unspecified
        :background unspecified))))))

 ((eq local-cfg/theme 'oblivion)
  ;; (use-package oblivion-theme)
  (add-to-list 'custom-theme-load-path "/src/emacs/oblivion-theme")
  (load-theme 'oblivion t)))

(when local-cfg/use-treesitter
  (use-package treesit-auto
    :commands (global-treesit-auto-mode))
  (global-treesit-auto-mode))


;; Exception, load immediately to avoid flicker.
;; Otherwise this would be last.

;; Load this first since it's needed for key bindings.
(when local-cfg/use-scroll-on-jump
  (use-package scroll-on-jump
    :commands
    (scroll-on-jump
     scroll-on-jump-interactive scroll-on-jump-advice-add scroll-on-jump-with-scroll-interactive)
    :ensure nil
    :load-path "/src/emacs/scroll-on-jump"

    :config

    (setq scroll-on-jump-duration 0.5)
    (setq scroll-on-jump-curve 'smooth-out)
    (setq scroll-on-jump-curve-power 4.0)

    (when local-cfg/use-custom-mode-line
      (setq scroll-on-jump-mode-line-format my-mode-line-location-no-column)))

  (with-eval-after-load 'diff-hl
    (scroll-on-jump-advice-add diff-hl-next-hunk)
    (scroll-on-jump-advice-add diff-hl-previous-hunk))

  (with-eval-after-load 'goto-chg
    (scroll-on-jump-advice-add goto-last-change)
    (scroll-on-jump-advice-add goto-last-change-reverse))

  (with-eval-after-load 'evil
    (scroll-on-jump-advice-add evil-goto-line)
    (scroll-on-jump-advice-add evil-jump-item)
    (scroll-on-jump-advice-add evil-jump-forward)
    (scroll-on-jump-advice-add evil-jump-backward)
    (scroll-on-jump-advice-add evil-ex-search-next)
    (scroll-on-jump-advice-add evil-ex-search-previous)
    (scroll-on-jump-advice-add evil-forward-paragraph)
    (scroll-on-jump-advice-add evil-backward-paragraph)
    (scroll-on-jump-advice-add evil-goto-mark)

    ;; Nifty, but ultimately a bit annoying.
    ;; (scroll-on-jump-advice-add evil-previous-line)
    ;; (scroll-on-jump-advice-add evil-next-line)

    ;; Actions that themselves scroll.
    (scroll-on-jump-with-scroll-advice-add evil-scroll-down)
    (scroll-on-jump-with-scroll-advice-add evil-scroll-up)
    (scroll-on-jump-with-scroll-advice-add evil-scroll-line-to-center)
    (scroll-on-jump-with-scroll-advice-add evil-scroll-line-to-top)
    (scroll-on-jump-with-scroll-advice-add evil-scroll-line-to-bottom)))

(unless local-cfg/use-scroll-on-jump
  (defmacro scroll-on-jump-interactive (sym)
    `(lambda ()
       (interactive)
       (call-interactively ,sym)))
  (defmacro scroll-on-jump (sym)
    `(progn
       ,@sym)))

(when local-cfg/use-highlight-idle-mode
  (use-package idle-highlight-mode
    :commands (idle-highlight-mode)
    :ensure nil
    :load-path "/src/emacs/idle-highlight-mode"

    :config
    (setq idle-highlight-idle-time 0.135)
    (setq idle-highlight-exclude-point t)

    (custom-set-faces '(idle-highlight ((t (:foreground "#000000" :background "#AAAA00")))))))

(use-package elisp-autofmt
  :commands (elisp-autofmt-mode)

  :load-path "/src/emacs/elisp-autofmt")

;; Needs investigation.
;; (use-package macrursors
;;   :commands (macrursors-start)
;;   :ensure nil
;;   :load-path "/src/emacs-macrursors"

;;   :config
;;   ;; (dolist (mode '(corfu-mode goggles-mode beacon-mode))
;;   ;;   (add-hook 'macrursors-pre-finish-hook mode)
;;   ;;   (add-hook 'macrursors-post-finish-hook mode))
;;   (define-prefix-command 'macrursors-mark-map)
;;   (global-set-key (kbd "C-c SPC") #'macrursors-select)
;;   (global-set-key (kbd "C->") #'macrursors-mark-next-instance-of)
;;   (global-set-key (kbd "C-<") #'macrursors-mark-previous-instance-of)
;;   (global-set-key (kbd "C-;") 'macrursors-mark-map)
;;   (define-key macrursors-mark-map (kbd "C-;") #'macrursors-mark-all-lines-or-instances)
;;   (define-key macrursors-mark-map (kbd ";") #'macrursors-mark-all-lines-or-instances)
;;   (define-key macrursors-mark-map (kbd "l") #'macrursors-mark-all-lists)
;;   (define-key macrursors-mark-map (kbd "s") #'macrursors-mark-all-symbols)
;;   (define-key macrursors-mark-map (kbd "e") #'macrursors-mark-all-sexps)
;;   (define-key macrursors-mark-map (kbd "f") #'macrursors-mark-all-defuns)
;;   (define-key macrursors-mark-map (kbd "n") #'macrursors-mark-all-numbers)
;;   (define-key macrursors-mark-map (kbd ".") #'macrursors-mark-all-sentences)
;;   (define-key macrursors-mark-map (kbd "r") #'macrursors-mark-all-lines))


(use-package lisp-extra-font-lock

  :commands (lisp-extra-font-lock-mode))

(use-package nameless
  :commands (nameless-mode)
  :ensure t
  :config
  ;; Only my package!
  (setq nameless-global-aliases nil)
  (setq nameless-affect-indentation-and-filling nil))

(use-package rainbow-delimiters
  :commands (rainbow-delimiters-mode)
  :config
  (custom-set-faces
   '(rainbow-delimiters-depth-1-face ((t (:foreground "#AAAAAA" :weight black))))
   '(rainbow-delimiters-depth-2-face ((t (:foreground "#FFFFFF" :weight black))))
   '(rainbow-delimiters-depth-3-face ((t (:foreground "#ffff00" :weight black))))
   '(rainbow-delimiters-depth-4-face ((t (:foreground "#ff00ff" :weight black))))
   '(rainbow-delimiters-depth-5-face ((t (:foreground "#00ffff" :weight black))))
   '(rainbow-delimiters-depth-6-face ((t (:foreground "#ff0000" :weight black))))
   '(rainbow-delimiters-depth-7-face ((t (:foreground "#00ff00" :weight black))))
   '(rainbow-delimiters-depth-8-face ((t (:foreground "#0088ff" :weight black))))
   '(rainbow-delimiters-depth-9-face ((t (:foreground "#6600ff" :weight black))))))

;; -------------------------
;; Evil Mode (Vim Emulation)
;; =========================

(when local-cfg/use-evil

  (use-package evil
    :commands (evil-local-mode evil-define-key)
    :init
    (when local-cfg/use-search-evil
      (setq evil-search-module 'evil-search))
    :config

    (when local-cfg/evil-keyboard-remap
      (load (file-name-concat user-emacs-directory "local" "evilkeys.el") nil t)
      (evilkeys-setup nil t))


    ;; Don't register for redo.
    (evil-declare-abort-repeat 'i42-save-buffer-always)
    ;; Don't repeat this command.
    (evil-declare-not-repeat 'i42-save-buffer-always)

    (let ((my-generic--no-repeat
           '(my-generic-run
             my-generic-build
             my-generic-goto-thing-at-point
             my-generic-usage-of-thing-at-point
             my-generic-jump-next
             my-generic-jump-prev
             my-generic-doc-jump-next
             my-generic-doc-jump-prev
             my-generic-doc-jump-section)))

      (mapc #'evil-declare-not-repeat my-generic--no-repeat)))

  ;; (use-package evil-visual-mark-mode)

  ;; The package `anzu' is nice for additional mode-line info while searching (N of TOTAL).
  (use-package evil-anzu
    :defer t)
  (use-package anzu
    :commands (anzu-mode))

  (with-eval-after-load 'evil
    (add-hook 'evil-local-mode-hook (lambda () (anzu-mode)))
    (require 'evil-anzu)))

(when local-cfg/use-bray
  ;; NOTE: could try to enable `anzu' id didn't work with ISEARCH last I tested.
  ;; The problem is it doesn't activate while cycling "next/prev" only when entering text.
  ;; (use-package anzu
  ;;   :commands (anzu-mode))

  (use-package bray
    :commands (bray-mode)
    :ensure nil
    :load-path "/src/emacs/bray"))

(use-package undo-hl
  :commands (undo-hl-mode)
  :ensure nil
  :load-path "/src/emacs-undo-hl"

  :config
  (setq pulse-iterations 20)
  (setq pulse-delay 0.03)
  (setq undo-hl-undo-commands
        (list 'vundo-backward 'vundo-forward 'vundo-stem-root 'vundo-stem-end)))

;; Interesting!
(use-package repeat-fu
  :commands (repeat-fu-mode repeat-fu-execute)
  :ensure nil
  :load-path "/src/emacs/repeat-fu")

(use-package vundo
  :commands (vundo)

  :ensure nil
  :load-path "/src/emacs-vundo"

  :config

  (defvar my-real-vundo-buf nil)

  (defun my-vundo-forward-pre-command-hook ()
    (let ((buf (current-buffer)))
      (when (bound-and-true-p my-real-vundo-buf)
        (unless (eq buf my-real-vundo-buf)
          (with-current-buffer my-real-vundo-buf
            (run-hooks 'pre-command-hook))))))

  (add-hook
   'vundo-pre-enter-hook
   (lambda ()
     (setq my-real-vundo-buf (current-buffer))
     (add-hook 'pre-command-hook 'my-vundo-forward-pre-command-hook)
     (undo-hl-mode 1)))
  (add-hook
   'vundo-post-exit-hook
   (lambda ()
     (remove-hook 'pre-command-hook 'my-vundo-forward-pre-command-hook)
     (undo-hl-mode -1)
     (makunbound 'my-real-vundo-buf)))

  ;; Remove & unhelpful diff-finished message.
  (add-hook
   'vundo-diff-setup-hook
   (lambda ()
     (save-excursion
       (goto-char (point-max))
       (forward-line -2)
       (when (looking-at-p "\nDiff finished\\.  ")
         (delete-region (point) (point-max))))))


  (add-hook
   'vundo-diff-setup-hook
   (lambda ()
     (font-lock-mode -1)
     (setq-local diff-ansi-method 'immediate)
     (save-excursion
       (goto-char (point-min))
       (forward-line 1)
       (diff-ansi-region (point) (point-max)))))

  (setq vundo-compact-display t)

  ;; Don't keep the diff buffer around.
  (setq vundo-diff-quit 'kill)

  (define-key vundo-mode-map (kbd "l") #'vundo-forward)
  (define-key vundo-mode-map (kbd "<right>") #'vundo-forward)
  (define-key vundo-mode-map (kbd "h") #'vundo-backward)
  (define-key vundo-mode-map (kbd "<left>") #'vundo-backward)
  (define-key vundo-mode-map (kbd "M-l") #'vundo-next-root)
  (define-key vundo-mode-map (kbd "M-<right>") #'vundo-next-root)
  (define-key vundo-mode-map (kbd "j") #'vundo-next)
  (define-key vundo-mode-map (kbd "<down>") #'vundo-next)
  (define-key vundo-mode-map (kbd "k") #'vundo-previous)
  (define-key vundo-mode-map (kbd "<up>") #'vundo-previous)
  (define-key vundo-mode-map (kbd "<home>") #'vundo-stem-root)
  (define-key vundo-mode-map (kbd "<end>") #'vundo-stem-end)
  (define-key vundo-mode-map (kbd "q") #'vundo-quit)
  (define-key vundo-mode-map (kbd "C-g") #'vundo-quit)
  (define-key vundo-mode-map (kbd "RET") #'vundo-confirm))


(when local-cfg/use-undo-fu
  (use-package undo-fu
    :commands (undo-fu-only-undo undo-fu-only-redo undo-fu-only-redo-all)
    :ensure nil
    :load-path "/src/emacs/undo-fu"))


(when local-cfg/use-undo-fu-session
  (use-package undo-fu-session
    :commands (undo-fu-session-mode)
    :ensure nil
    :load-path "/src/emacs/undo-fu-session"

    :config

    ;; Don't write redo/unused-branches.
    (setq undo-fu-session-linear t)
    (setq undo-fu-session-compression 'zst)

    ;; Not essential, just handy.
    (advice-add
     'undo-fu-session--recover-safe
     :around
     #'(lambda (old-fn &rest args)
         (let ((result (apply old-fn args)))
           (when (and result (eq 1 (point)))
             ;; Check we have undo information.
             (when (and buffer-undo-list (not (eq t buffer-undo-list)))
               ;; We could forward errors, its rather involved.
               (without-message
                   ;; In the case of a real error, show it as a message.
                   (ignore-errors
                     (goto-last-change 0))))
             result))))

    (setq undo-fu-session-incompatible-files
          (list
           ;; Ignore files in the temporary directory.
           (lambda (filename)
             (string-match-p (concat "\\`" (regexp-quote temporary-file-directory)) filename))
           ;; GIT
           "/COMMIT_EDITMSG\\'" ;; git-commit.
           "/git-rebase-todo\\'" ;; git-rebase.
           ;; SVN
           "/svn-commit\\(?:\\.[[:digit:]]+\\)?\\.tmp\\'" ;; svn-commit.
           ))))

(when local-cfg/use-buffer-name-vc-relative
  (use-package buffer-name-relative
    :commands (buffer-name-relative-mode)
    :ensure nil
    :load-path "/src/emacs/buffer-name-relative"

    :config
    (setq buffer-name-relative-prefix (cons "[" "]"))
    (setq buffer-name-relative-prefix-map
          '(("/src/blender" . "BL")
            ("/src/blender_exensions" . "EX")
            ("/src/donelist_blender" . "DL"))))
  (buffer-name-relative-mode))


(use-package revert-buffer-all
  :commands (revert-buffer-all)
  :ensure nil
  :load-path "/src/emacs/revert-buffer-all")

(when local-cfg/use-which-key
  (use-package which-key
    :defer t
    :config
    (progn
      (setq which-key-sort-order 'which-key-key-order-alpha)
      (which-key-mode))))

(use-package highlight-operators
  :commands (highlight-operators-mode)
  :config
  (setq highlight-operators-regexp
        (regexp-opt '("+" "-" "*" "/" "%" "!" "&" "^" "~" "|" "=" "<" ">"))))

(when local-cfg/use-evil
  (use-package evil-numbers
    :commands (evil-numbers/inc-at-pt evil-numbers/dec-at-pt)
    :ensure nil
    :load-path "/src/emacs-evil-numbers")

  (when local-cfg/evil-surround
    (use-package evil-surround
      :commands (evil-surround-mode evil-Surround-edit evil-Surround-region evil-surround-edit))))

(when local-cfg/use-bray
  ;; (use-package holy-numbers
  ;;   :commands (holy-numbers/inc-at-pt holy-numbers/dec-at-pt)
  ;;   :ensure nil
  ;;   :load-path "/src/emacs-holy-numbers")

  (use-package shift-number
    :commands (shift-number-up shift-number-down)
    :ensure nil
    :load-path "/src/emacs/shift-number"))

(when local-cfg/use-magit
  (use-package magit
    :init
    ;; Don't use magit for interactive rebase
    ;; (has own entire key-map, doesn't allow text-file editing).
    (setq auto-mode-alist (rassq-delete-all 'git-rebase-mode auto-mode-alist))

    :config
    ;; Don't gain focus after opening commit.
    (setq-default magit-display-buffer-noselect t)
    (setq-default magit-popup-show-common-commands nil)
    (setq-default magit-commit-arguments '("--all"))

    ;; Cycle between
    (with-eval-after-load 'magit-diff
      (with-eval-after-load 'magit-log
        (define-key magit-revision-mode-map (kbd "TAB") 'other-window)
        (define-key magit-log-mode-map (kbd "TAB") 'other-window)

        (define-key magit-revision-mode-map (kbd "C-t") 'toggle-window-split)
        (define-key magit-log-mode-map (kbd "C-t") 'toggle-window-split)))

    (with-eval-after-load 'magit-diff
      (when local-cfg/use-scroll-on-drag
        (define-key magit-revision-mode-map [down-mouse-2] 'scroll-on-drag))


      (define-key magit-revision-mode-map (kbd "j") 'evil-next-line)
      (define-key magit-revision-mode-map (kbd "k") 'evil-previous-line)

      (define-key magit-revision-mode-map (kbd "n") 'evil-search-next)
      (define-key magit-revision-mode-map (kbd "N") 'evil-search-previous)

      (define-key magit-revision-mode-map (kbd "/") 'evil-search-forward)
      (define-key magit-revision-mode-map (kbd "?") 'evil-search-backward))

    (with-eval-after-load 'magit-log
      (define-key magit-log-mode-map (kbd ";") 'magit-commit-mark-toggle-read)
      (define-key magit-log-mode-map (kbd "C-;") 'magit-commit-mark-toggle-urgent)
      (define-key magit-log-mode-map (kbd "M-;") 'magit-commit-mark-toggle-star)

      (define-key magit-log-mode-map (kbd "j") 'magit-section-forward)
      (define-key magit-log-mode-map (kbd "k") 'magit-section-backward)

      (defun my-magit-log-toggle-limit-to-author ()
        "Toggle an isolated log of the current author."
        (interactive)
        (let* ((args (magit-log-arguments))
               (arg-prefix "--author=")
               (log-setup-buffer-args
                (list
                 (magit-log-read-revs t)
                 (cond
                  ((seq-find (apply-partially #'string-prefix-p arg-prefix) (car args))
                   (seq-remove (apply-partially #'string-prefix-p arg-prefix) (car args)))
                  (t
                   (let ((rev
                          (or (magit-commit-at-point)
                              (magit-section-value-if 'module-commit)
                              (thing-at-point 'git-revision t))))
                     (unless rev
                       (error "Could not find rev at point"))
                     (let ((author
                            ;; NOTE: including the email broke, just the name works.
                            ;; (magit-rev-format "%an <%ae>" rev)
                            (magit-rev-format "%an" rev)))
                       (append (car args) (list (concat arg-prefix author)))))))
                 nil)))
          (apply #'magit-log-setup-buffer log-setup-buffer-args)))


      (defun my-magit-sorted-git-dirs (cwd)
        (let ((default-directory cwd)
              (dirs (list)))
          (with-temp-buffer
            (call-process "git" nil t nil "ls-files")
            (let ((dir-prev "")
                  (step-prev 0)
                  (dirs-hash (make-hash-table :test 'equal)))
              (goto-char (point-min))
              (while (zerop step-prev)
                (let ((line (buffer-substring-no-properties (pos-bol) (pos-eol))))
                  (unless (string-empty-p line)
                    (let ((dir (file-name-directory line)))
                      (when (and dir (not (string-equal dir dir-prev)))
                        (setq dir-prev dir)
                        ;; Use a hash table as this list _can_ contain multiple items.
                        (unless (gethash dir dirs-hash)
                          (puthash dir t dirs-hash)
                          (push dir dirs)
                          ;; Ensure all parent directories are added too.
                          (while (and dir
                                      (setq dir
                                            (file-name-directory
                                             ;; Remove trailing slash.
                                             (directory-file-name dir))))
                            (cond
                             ((gethash dir dirs-hash)
                              ;; Break.
                              (setq dir nil))
                             (t
                              (puthash dir t dirs-hash)
                              (push dir dirs)))))))))
                (setq step-prev (forward-line 1)))))

          (sort dirs 'string-greaterp)))

      (defun my-magit-log-toggle-limit-to-directory ()
        "Toggle an isolated log of the current author."
        (interactive)
        (let* ((args (magit-log-arguments))
               (args-0 (car args))
               (args-1 (car (cdr args)))
               (log-setup-buffer-args
                (progn
                  (cond
                   (args-1
                    (setq args-1 nil))
                   (t
                    (let* ((default-index 0)
                           (prompt "Directory:")
                           (content
                            (let ((dirs (my-magit-sorted-git-dirs (magit-toplevel)))
                                  (content-wip (list)))
                              (dolist (item dirs)
                                (push (cons item item) content-wip))
                              content-wip))
                           (choice
                            (funcall completing-read-function
                                     prompt
                                     content
                                     nil
                                     t
                                     nil
                                     nil
                                     (nth default-index content))))
                      (pcase-let ((`(,_text . ,dir) (assoc choice content)))
                        (setq args-1 (list dir))))))

                  (list (magit-log-read-revs t) args-0 args-1))))

          (apply #'magit-log-setup-buffer log-setup-buffer-args)))

      (define-key magit-log-mode-map (kbd "C-a") 'my-magit-log-toggle-limit-to-author)
      (define-key magit-log-mode-map (kbd "C-d") 'my-magit-log-toggle-limit-to-directory)

      ;; Evil search bindings.
      (define-key magit-log-mode-map (kbd "/") 'evil-search-forward)
      (define-key magit-log-mode-map (kbd "?") 'evil-search-backward)

      (define-key magit-log-mode-map (kbd "n") 'evil-search-next)
      (define-key magit-log-mode-map (kbd "N") 'evil-search-previous)

      (define-key magit-log-mode-map (kbd "C-j") 'magit-commit-mark-next-unread)
      (define-key magit-log-mode-map (kbd "C-k") 'magit-commit-mark-prev-unread)

      (when local-cfg/use-scroll-on-drag
        (define-key magit-log-mode-map [down-mouse-2] 'scroll-on-drag))))


  (add-hook 'magit-mode-hook 'magit-commit-mark-mode)
  (use-package magit-commit-mark
    :after (magit)
    :commands (magit-commit-mark-mode)
    :ensure nil
    :load-path "/src/emacs/magit-commit-mark"

    :config

    ;; Immediately mark unread when skipping (nice to quickly mark merge commits read for eg).
    (setq magit-commit-mark-on-skip-to-unread t)
    (setq magit-commit-mark-sha1-length 16))


  (use-package diff-ansi
    :commands (diff-ansi-mode diff-ansi-buffer)
    :ensure nil
    :load-path "/src/emacs/diff-ansi"

    :config

    (setq diff-ansi-method 'multiprocess)
    (setq diff-ansi-verbose-progress t)

    (setq diff-ansi-extra-args-for-delta
          (append
           (list
            ;; Some fonts don't contain the default characters, causing odd alignment.
            ;; "--wrap-right-symbol=¶" "--wrap-left-symbol=§"
            ;; "--true-color" "always"
            )
           diff-ansi-extra-args-for-delta)))
  (add-hook 'magit-mode-hook 'diff-ansi-mode))

(use-package mode-line-idle
  :commands (mode-line-idle)
  :ensure nil
  :load-path "/src/emacs/mode-line-idle")

;; --------------------
;; Application Plug-Ins
;; ====================

(use-package run-stuff
  :commands (run-stuff-command-on-region-or-line)
  :ensure nil
  :load-path "/src/emacs/run-stuff"

  :config
  (cond
   ((is-wayland)
    (setq run-stuff-terminal-command "foot"))
   (t
    (setq run-stuff-terminal-command "urxvt"))))

(when local-cfg/use-neotree
  (use-package neotree
    :commands (neo-smart-open)
    :config
    (setq neo-theme 'ascii)
    (setq neo-window-width 40)

    ;; Disable line-numbers minor mode for neotree,
    ;; enable line highlight.
    (add-hook
     'neo-after-create-hook
     (lambda (&rest _)
       (display-line-numbers-mode -1)
       (hl-line-mode 1)))))

(when (and local-cfg/use-treemacs local-cfg/use-lsp)
  (use-package treemacs)
  (when local-cfg/use-lsp
    (use-package lsp-treemacs)))


(use-package imenu-list
  :commands (imenu-list-minor-mode)

  :config
  (advice-add
   #'imenu-list--action-goto-entry
   :override
   (lambda (event)
     (let ((ilist-buffer (get-buffer imenu-list-buffer-name)))
       (with-current-buffer ilist-buffer
         (imenu-list-goto-entry)))))

  (setq imenu-list-idle-update-delay 0.2)
  (setq imenu-list-size 40))


;; Open a diff using ''
(use-package diff-at-point
  :commands (diff-at-point-open-and-goto-hunk diff-at-point-goto-source-and-close)
  :ensure nil
  :load-path "/src/emacs/diff-at-point")

(when local-cfg/use-ivy
  (use-package ivy
    :commands (counsel-M-x ivy-completing-read ivy-read ivy-mode)
    :config
    ;; Case sensitive by default.
    (setq ivy-case-fold-search-default nil)
    ;; does not count candidates
    (setq ivy-count-format "")
    ;; Use half window height for all ivy searches.
    (setq ivy-height-alist `((t . ,(lambda (_caller) (/ (window-height) 2))))))

  (use-package swiper
    :commands (swiper)
    :config
    ;; Jump to start of match instead of end.
    ;; Generally more useful. Also used for counsel.
    (setq swiper-goto-start-of-match t))

  (use-package counsel
    :commands (counsel-rg)
    :config

    (setq counsel-fzf-cmd (concat counsel-fzf-cmd " --tiebreak=length --exact +i"))
    (setq counsel-find-file-at-point t))

  (use-package counsel-at-point
    :commands (counsel-at-point-rg counsel-at-point-file-jump counsel-at-point-imenu)
    :ensure nil
    :load-path "/src/emacs/counsel-at-point"))

(when local-cfg/use-vertico
  (use-package vertico
    :init

    (vertico-mode t)

    (setq vertico-count 50)

    (setq vertico-resize t)

    :config

    (define-key vertico-map (kbd "C-j") 'vertico-next)
    (define-key vertico-map (kbd "C-k") 'vertico-previous)

    ;; TODO: implement these keys.

    ;; (define-key vertico-map (kbd "C-h") 'minibuffer-keyboard-quit)
    ;; (define-key vertico-minibuffer-map (kbd "C-l") 'vertico-select-current-candidate)

    ;; ;; Open and next.
    ;; ;; (define-key vertico-map (kbd "C-M-j") 'vertico-next-candidate-and-call)
    ;; ;; (define-key vertico-map (kbd "C-M-k") 'vertico-previous-candidate-and-call)

    ;; (define-key vertico-map (kbd "<C-return>") 'vertico-select-current-candidate)

    ;; ;; Paste is too handy.
    ;; (define-key vertico-map (kbd "C-v") 'yank)

    ;; ;; So we can switch away.
    ;; (define-key vertico-map (kbd "C-w") 'evil-window-map)
    )

  (use-package orderless
    :ensure t
    :custom
    (completion-styles '(orderless basic))
    (completion-category-overrides '((file (styles basic partial-completion)))))

  (use-package consult
    :commands (consult-ripgrep consult-project-buffer)

    :config

    (setq consult-preview-key '(:debounce 0.1 any))))


;; ----------------------
;; Filetype Mode Packages
;; ======================

;; Statically Typed
(use-package glsl-mode
  :mode "\\.glsl\\'")
(use-package haskell-mode
  :mode "\\.hs\\'")

(use-package git-lite-commit
  :commands (git-lite-commit-mode)
  :mode "COMMIT_EDITMSG\\'"
  :ensure nil
  :load-path "/src/emacs/git-lite-commit"

  :config
  ;; Don't delimit on "_"
  ;; (modify-syntax-entry ?_ "w")

  :hook
  ((git-lite-commit-mode)
   .
   (lambda ()

     (setq-local spell-fu-check-range #'git-lite-commit-spell-fu-range-limit)
     (spell-fu-mode)

     ;; Spelling.
     (setq-local hl-prog-extra-list
                 ;; Extend the default list to include the bug tracker ID.
                 (list

                  ;; (list
                  ;;  " \\([[:alnum:][:blank:]]+\\):$"
                  ;;  1
                  ;;  'comment
                  ;;  '(:weight bold :inherit font-lock-comment-face))

                  ;; Match <email@name.foo> email address.
                  (list "<\\([[:alnum:]\\._-]+@[a-z0-9._-]+\\)>" 1 nil 'font-lock-constant-face)
                  ;; Match `some.text` as a constant.
                  (list "`[^`\n]+`" 0 nil 'font-lock-constant-face)
                  ;; Match http://XYZ (URL)
                  (list "\\<http[s]?://[^[:blank:]\n]*" 0 nil 'font-lock-constant-face)
                  ;; Bug reports.
                  (list "[#!][[:digit:]]+\\>" 0 nil 'font-lock-delimiter-face)))
     (hl-prog-extra-mode)


     (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

     (when local-cfg/use-evil
       (evil-exit-emacs-state))

     (unless noninteractive

       (display-fill-column-indicator-mode 1)

       ;; Scale to fit.
       (default-font-presets-scale-fit)))))

(use-package rust-mode
  :mode "\\.rs\\'")

(when local-cfg/use-lsp
  (setq lsp-prefer-flymake nil)

  ;; Don't make links from headers (for e.g.).
  (setq lsp-enable-links nil)

  ;; Nice but slows down highlighting of comments and strings,
  ;; generally makes C/C++ feel less responsive.
  (setq lsp-semantic-tokens-enable t)
  (setq lsp-idle-delay 0.0)

  ;; Disable highlighting.
  (setq lsp-enable-symbol-highlighting nil)

  (unless local-cfg/use-yasnippet
    (setq lsp-enable-snippet nil))

  (when local-cfg/use-lsp-ccls
    (use-package ccls
      :config

      (setq ccls-args
            (list
             "--init"
             (concat
              "{"
              "\"compilationDatabaseDirectory\": \"/src/cmake_debug/\", "
              "\"cache\": {\"directory\": \"/home/ideasman42/.cache/ccls\"}, "
              (format "\"index\": {\"threads\": %d}" my-num-processors-limited)
              "}")
             ;; "--log-file=C:/Users/Jose/ccls.log" "--log-file-append=true" "--v=2"
             ))))

  (use-package lsp-mode
    :config

    ;; (eval-when-compile (require 'lsp-mode))

    (setq lsp-imenu-index-symbol-kinds (list 'Function))

    ;; Takes up space.
    (setq lsp-headerline-breadcrumb-enable nil)

    (setq lsp-enable-file-watchers nil)
    (setq lsp-eldoc-render-all t)

    (unless local-cfg/use-eldoc-mode
      (setq lsp-eldoc-enable-hover nil))

    ;; Don't edit the file, ever! (clangd adds includes unless this is set).
    (setq lsp-completion-enable-additional-text-edit nil)

    ;; When showing an imenu list, order by position (not name / kind).
    (setq lsp-imenu-sort-methods '(position))

    ;; Too noisy (prints on loading buffers)
    ;; re-enable for debugging lsp mode.
    (fset 'lsp--info 'ignore)

    (custom-set-faces
     '(lsp-face-highlight-write ((t (:background "#AA0000" :foreground "black"))))
     '(lsp-face-highlight-read ((t (:background "#00AA00" :foreground "black"))))
     '(lsp-face-highlight-textual ((t (:background "#AAAAAA" :foreground "black")))))

    :hook

    ;; python-mode
    ((c-mode c++-mode c-ts-mode c++-ts-mode)
     .
     (lambda ()

       (cond
        (local-cfg/use-lsp-ccls
         (setq lsp-disabled-clients '(clangd)))
        (t ;; `clangd'
         (setq lsp-disabled-clients '(ccls))
         ;; TODO: detect compile-commands dir.
         (setq-default lsp-clients-clangd-args
                       (list
                        (format "-j=%d" my-num-processors-limited)
                        "--log=error"
                        "--compile-commands-dir=/src/cmake_debug"))))

       ;; Shows "refs" next to symbols, may be nice but seems to be slow.
       (setq lsp-lens-enable nil)


       (when local-cfg/use-yasnippet
         (require 'yasnippet))

       ;; Use eldoc with lsp mode, it works well.
       (when local-cfg/use-eldoc-mode
         (eldoc-mode 1))

       (lsp)

       ;; Use our C++ comment rendering!
       (when local-cfg/use-c-comment-pkg
         (defun lsp--render-markdown--actually-doxygen ()
           ;; Disable font locking until it's needed.
           (font-lock-mode -1)

           ;;Insert comments so highlighting is correct.
           (goto-char (point-min))
           (insert "/**\n")
           ;; Insert a tab before the comment to keep some white-space under the
           ;; comment (visually separates with signature).
           (goto-char (point-max))
           (insert "\n\t*/")
           (cond
            (local-cfg/use-treesitter
             (c-ts-mode))
            (t
             (c-mode)))

           (when local-cfg/use-c-comment-pkg
             (prog-face-refine-mode))
           (hl-prog-extra-mode)

           ;; Copy settings from the current buffer.
           (let ((buf (window-buffer (selected-window))))
             (dolist (var '(hl-prog-extra-list))
               (set (make-local-variable var) (buffer-local-value var buf))))
           (hl-prog-extra-refresh)

           (font-lock-flush)
           ;; Don't show the comment characters.
           (narrow-to-region (+ (point-min) 4) (- (point-max) 2)))
         (advice-add 'lsp--render-markdown :override #'lsp--render-markdown--actually-doxygen))))))

;; (when local-cfg/use-lsp
;;   (use-package company-lsp
;;     :commands company-lsp))

(when (and local-cfg/use-dap local-cfg/use-lsp)
  (use-package dap-mode
    :commands (dap-mode dap-debug)

    :config
    (setq dap-auto-configure-mode t)
    (setq dap-auto-configure-features '(sessions locals tooltip))))

(with-eval-after-load 'dap-mode
  ;; Uses F5.
  ;; (setq-local my-generic-run 'my-dap-debug-or-continue)
  (define-key dap-mode-map (kbd "M-<f5>") 'dap-stop-thread)
  (define-key dap-mode-map (kbd "<f9>") 'dap-breakpoint-toggle)
  (define-key dap-mode-map (kbd "<f10>") 'dap-next)
  (define-key dap-mode-map (kbd "<f11>") 'dap-step-in)
  (define-key dap-mode-map (kbd "<S-f11>") 'dap-step-out))

(when local-cfg/use-eglot
  (use-package eglot
    :config
    ;; Quiet output.
    (setq eglot-report-progress nil)

    (define-key eglot-mode-map (kbd "C-;") 'doc-show-inline-mode)
    (define-key eglot-mode-map (kbd "C-M-;") 'eglot-inlay-hints-mode)

    (setq eglot-stay-out-of (cons 'eldoc eglot-stay-out-of))

    :hook

    (((python-mode python-ts-mode)
      .
      (lambda ()
        (require 'eglot)
        (add-to-list
         'eglot-server-programs
         (cons
          (list
           'python-mode
           ;; Tree-sitter modes.
           'python-ts-mode)
          (list "pylsp")))

        (setq-default eglot-workspace-configuration
                      (list
                       (cons
                        :pylsp
                        (list
                         :configurationSources ["flake8"]
                         :plugins
                         (list
                          :flake8 (list :enabled t)
                          :pycodestyle (list :enabled nil)
                          :mccabe (list :enabled nil)

                          :jedi
                          (list
                           ;; Always add `./modules' because it's a handy convention.
                           :extra_paths
                           (let ((path buffer-file-name))
                             (when path
                               (setq path
                                     (file-name-concat (expand-file-name
                                                        (file-name-parent-directory path))
                                                       "modules"))
                               (unless (file-directory-p path)
                                 (setq path nil)))
                             (cond
                              (path
                               (vconcat (list path)))
                              (t
                               (vconcat (list)))))))))))

        (eglot-ensure)))
     ((c-mode c++-mode c-ts-mode c++-ts-mode)
      .
      (lambda ()
        (require 'eglot)
        (setq completion-category-defaults nil)

        (customize-set-variable 'eglot-autoshutdown t)
        (customize-set-variable 'eglot-extend-to-xref t)
        (customize-set-variable
         'eglot-ignored-server-capabilities
         (quote (:documentFormattingProvider :documentRangeFormattingProvider)))

        (cond
         (local-cfg/use-eglot-ccls
          (add-to-list
           'eglot-server-programs
           `((c-mode
              c++-mode
              ;; Tree-sitter modes.
              c-ts-mode c++-ts-mode)
             .
             ("ccls"
              "--init"
              ;; "--log-file=C:/Users/Jose/ccls.log" "--log-file-append=true" "--v=2"
              ,(concat
                "{"
                "\"compilationDatabaseDirectory\": \"/src/cmake_debug/\", "
                "\"cache\": {\"directory\": \"/home/ideasman42/.cache/ccls\"}, "
                (format "\"index\": {\"threads\": %d}" my-num-processors-limited)
                "}")
              ,(format "-j=%d" my-num-processors-limited)
              "--header-insertion=never"
              "--log=error"
              "--compile-commands-dir=/src/cmake_debug"))))
         (t
          (add-to-list
           'eglot-server-programs
           `((c-mode
              c++-mode
              ;; Tree-sitter modes.
              c-ts-mode c++-ts-mode)
             .
             ("clangd"
              ,(format "-j=%d" my-num-processors-limited)
              "--header-insertion=never"
              "--log=error"
              "--compile-commands-dir=/src/cmake_debug")))))

        (when local-cfg/use-yasnippet
          (require 'yasnippet))

        ;; Use eldoc with eglot mode, it works well.
        (when local-cfg/use-eldoc-mode
          (eldoc-mode 1))

        (eglot-ensure))))))

(use-package clang-format
  :commands (clang-format-buffer clang-format-on-save-mode))

(use-package cmake-mode
  :mode "\\.cmake\\'")

;; Dynamically Typed
(use-package lua-mode
  :mode "\\.lua\\'")
;; Build Systems
(use-package fish-mode
  :mode "\\.fish\\'")
;; Shells
(use-package haskell-mode
  :mode "\\.hs\\'")
;; Markup
(use-package markdown-mode
  :mode "\\.md\\'")
;; Configuration
(use-package nix-mode
  :mode "\\.nix\\'")
;; Data
(use-package toml-mode
  :mode "\\.toml\\'")

;; ----------------
;; Display Packages
;; ================

(when local-cfg/use-jit-lock-stealth
  (setq jit-lock-stealth-time 1.25)
  (setq jit-lock-stealth-nice 0.5) ;; Default 0.5 seconds between font locking.
  (setq jit-lock-chunk-size 4096)

  (use-package jit-lock-stealth-progress
    :commands (jit-lock-stealth-progress-mode)
    :ensure nil
    :load-path "/src/emacs/jit-lock-stealth-progress"

    :config
    (when local-cfg/use-custom-mode-line
      (setq jit-lock-stealth-progress-info "      ")
      (setq jit-lock-stealth-progress-info-format "%5.1f%%%%")
      (setq jit-lock-stealth-progress-add-to-mode-line nil)))

  (with-eval-after-load 'jit-lock
    (jit-lock-stealth-progress-mode)))

(use-package hl-indent-scope
  :commands (hl-indent-scope-mode)

  :config

  (setq hl-indent-scope-fill-empty-lines t)

  (add-hook
   'hl-indent-scope-mode-hook
   (lambda ()
     (cond
      ((memq major-mode (list 'python-mode 'python-ts-mode))
       ;; It's practically always an error when blocks don't use fixed indentation.
       (setq hl-indent-scope-fixed-width t))
      ((memq major-mode (list 'cmake-mode 'cmake-ts-mode))
       ;; It's practically always an error when blocks don't use fixed indentation.
       (setq hl-indent-scope-fixed-width t))
      (t
       (setq hl-indent-scope-fixed-width nil)))))

  :load-path "/src/emacs/hl-indent-scope"
  :ensure nil)

(use-package fancy-compilation
  :commands (fancy-compilation-mode)

  :load-path "/src/emacs/fancy-compilation"
  :ensure nil)

(with-eval-after-load 'compile
  (fancy-compilation-mode))


;; local-cfg/use-hl-block
(when local-cfg/use-hl-block
  (use-package hl-block-mode
    :commands (hl-block-mode)
    :ensure nil
    :load-path "/src/emacs/hl-block-mode"

    :config

    ;; (setq hl-block-color-tint "#880088")
    ;; (setq hl-block-multi-line t)
    ;; (setq hl-block-bracket nil)
    (setq hl-block-single-level t)
    (setq hl-block-style 'bracket)))

(when local-cfg/use-highlight-indent-hig
  (use-package highlight-indent-guides
    :commands (highlight-indent-guides-mode)
    :config
    (setq highlight-indent-guides-method 'fill)
    (setq highlight-indent-guides-auto-enabled nil)

    ;; (setq highlight-indent-guides-character ?▏)
    (setq highlight-indent-guides-character ?▎)))

(when local-cfg/use-number-color
  (use-package highlight-numbers
    :commands (highlight-numbers-mode)))

(use-package rainbow-mode
  :commands (rainbow-mode))

(when local-cfg/use-git-gutter
  (use-package git-gutter
    :commands (git-gutter-mode)
    :config
    (setq git-gutter:ask-p nil)
    (setq git-gutter:modified-sign " ")
    (setq git-gutter:added-sign " ")
    (setq git-gutter:deleted-sign " ")
    (setq git-gutter:unchanged-sign " ")
    (custom-set-faces
     '(git-gutter:unchanged ((t (:background "black"))))
     '(git-gutter:added ((t (:background "green" :foreground "black"))))
     '(git-gutter:deleted ((t (:background "red" :foreground "black"))))
     '(git-gutter:separator ((t (:background "cyan" :foreground "black"))))
     '(git-gutter:modified ((t (:background "magenta" :foreground "black")))))))

(when local-cfg/use-diff-hl
  (use-package diff-hl
    :commands (diff-hl-mode)))


;; Nice package to show `eldoc' in a window.
;; This is much less disruptive than popping up a mini-buffer which gets annoying.
(use-package eldoc-box
  :commands (eldoc-box-hover-mode)
  :config
  ;; Allow using the full width.
  (defun my-eldoc-box-max-width ()
    (window-body-width nil t))
  (setq eldoc-box-max-pixel-width #'my-eldoc-box-max-width)

  (defun my-eldoc-box-toggle-show-more ()
    (interactive)
    (eldoc-box-quit-frame)
    (let ((win (get-buffer-window "*eldoc*" t)))
      (cond
       (win
        (eldoc-box-hover-mode 1)
        (delete-window win))
       (t
        (eldoc-box-hover-mode -1)
        (call-interactively 'eldoc)))))


  (defun my-eldoc-box-max-height ()
    (* 3 (line-pixel-height)))

  (setq eldoc-box-max-pixel-height #'my-eldoc-box-max-height)

  (defun my-eldoc-box-buffer-hook ()
    (setq my-eldoc-box-show-more nil))

  (add-hook 'eldoc-box-buffer-hook #'my-eldoc-box-buffer-hook)

  ;; Place bottom-right (above mode-line).
  (defun my-eldoc-box-default-upper-corner-position (width height)
    (cons
     (- (window-body-width nil t) width)
     ;; Add pixels because of minor pixels because of mode-line outline.
     (- (window-body-height nil t) (+ 2 height))))
  (setq eldoc-box-position-function #'my-eldoc-box-default-upper-corner-position))
;; Always use with `eldoc-mode'.
(add-hook 'eldoc-mode-hook
          (lambda ()
            (when eldoc-mode
              (eldoc-box-hover-mode)))
          t)


;; ----------------
;; Editing Packages
;; ================

(use-package message-links
  :commands (message-links-convert-links-all message-links-convert-link-at-point)
  :load-path "/src/emacs-message-links"
  :ensure nil)

(when local-cfg/use-yasnippet
  (use-package yasnippet))

(use-package mono-complete
  :commands (mono-complete-mode)
  :ensure nil
  :load-path "/src/emacs/mono-complete"

  :config

  (setq mono-complete-preview-delay 0.175)

  (setq mono-complete-backend-word-predict-input-paths-size-limit 0)
  (setq mono-complete-use-function-defs nil)
  (setq mono-complete-fallback-command 'move-end-of-line)

  (setq mono-complete-backends
        (lambda (is-context)
          "Back-end defaults.
When IS-CONTEXT is nil: return all back-ends that may be used,
otherwise return all back-ends which may be returned for the current buffer."
          (cond
           (is-context
            (let* ((result (list))
                   (state (syntax-ppss))
                   ;; Inside string or comment.
                   (is-comment-or-string (or (nth 3 state) (nth 4 state))))

              (unless is-comment-or-string
                (push 'whole-line result))

              (when is-comment-or-string
                (when (bound-and-true-p spell-fu-mode)
                  (push 'spell-fu result)))

              (push 'dabbrev result)

              (unless is-comment-or-string
                (push 'capf result))

              (when is-comment-or-string
                (push 'word-predict result))

              result))
           (t
            (list 'word-predict 'filesystem 'dabbrev 'capf 'spell-fu 'whole-line))))))

(use-package utimeclock
  :commands (utimeclock-toggle utimeclock-from-context-summary)
  :ensure nil
  :load-path "/src/emacs/utimeclock"
  :config (setq utimeclock-12-hour-clock t))

(use-package recomplete
  :commands (recomplete-ispell-word recomplete-case-style)
  :ensure nil
  :load-path "/src/emacs/recomplete")

(use-package cycle-at-point
  :commands (cycle-at-point)
  :ensure nil
  :load-path "/src/emacs/cycle-at-point")

(use-package expand-region
  :commands (er/expand-region er/contract-region))

(when local-cfg/use-company

  (defun my-company-complete-common-use-context ()
    (interactive)
    (let ((faces-found (face-at-point nil t))
          (faces-comment-list
           '(font-lock-comment-face font-lock-comment-delimiter-face font-lock-doc-face)))
      (cond
       ((seq-intersection faces-found faces-comment-list)
        (let ((company-backends (list 'company-ispell)))
          (company-complete-common)))

       ;; Only use lsp mode when it is enabled, avoids confusion.
       ((or (bound-and-true-p lsp-mode) (bound-and-true-p eglot-mode))
        (cond
         ;; XXX: this doesn't work so well, it inserts space
         ;; if no completion can be found which is annoying!
         (nil
          (let ((company-backends (list 'company-capf)))
            (company-complete-common)))
         (t
          (call-interactively 'company-capf))))

       ;; Use whatever is available.
       (t
        (company-complete-common)))))

  (use-package company
    :commands (company-mode company-complete-common company-dabbrev)
    :config

    ;; Be case sensitive.
    (setq company-dabbrev-downcase nil)
    (setq company-dabbrev-ignore-case nil)

    ;; Number of items in the list to show at once.
    (setq company-tooltip-limit 40)
    ;; this can be slow, only perform explicit complete
    (setq company-idle-delay nil)
    (setq company-tooltip-idle-delay nil)

    (when local-cfg/use-company-preview
      (setq company-idle-delay 0.25))

    ;; We're already holding Ctrl, allow navigation at the same time.
    (define-key company-search-map (kbd "C-j") 'company-select-next)
    (define-key company-search-map (kbd "C-k") 'company-select-previous)

    (define-key company-active-map (kbd "C-j") 'company-select-next)
    (define-key company-active-map (kbd "C-k") 'company-select-previous)

    (define-key company-active-map (kbd "C-h") 'company-abort)
    (define-key company-active-map (kbd "<C-return>") 'company-complete-selection)
    (define-key company-active-map (kbd "C-l") 'company-complete-selection)))

(use-package drag-stuff
  :commands (drag-stuff-up drag-stuff-down)
  :config (drag-stuff-global-mode 1))

(use-package shrink-whitespace
  :commands (shrink-whitespace))

(use-package titlecase
  :commands (titlecase-dwim))

;; -----------------------
;; Infrastructure Packages
;; =======================

(use-package sidecar-locals
  :commands (sidecar-locals-mode)
  :ensure nil
  :load-path "/src/emacs/sidecar-locals")


;; -----------------
;; Spelling Packages
;; =================

(when local-cfg/use-spelling-spell-fu
  (use-package spell-fu
    :commands (spell-fu-mode)
    :ensure nil
    :load-path "/src/emacs/spell-fu"

    :config
    (setq spell-fu-delimit-by-face t)
    (setq spell-fu-idle-delay 0.235)
    (setq spell-fu-word-delimit-camel-case t)))

;; -------------------
;; Navigation Packages
;; ===================

(use-package spatial-navigate
  :commands
  (spatial-navigate-backward-horizontal-bar
   spatial-navigate-forward-horizontal-bar
   spatial-navigate-backward-vertical-bar
   spatial-navigate-forward-vertical-bar
   spatial-navigate-backward-horizontal-box
   spatial-navigate-forward-horizontal-box
   spatial-navigate-backward-vertical-box
   spatial-navigate-forward-vertical-box)

  :ensure nil
  :load-path "/src/emacs/spatial-navigate")

(use-package avy
  :commands (avy-goto-word-1)
  :config (setq avy-case-fold-search nil))

(use-package bookmark-in-project
  :commands
  (bookmark-in-project-jump
   bookmark-in-project-jump-next
   bookmark-in-project-jump-previous
   bookmark-in-project-delete-all
   bookmark-in-project-toggle)
  :ensure nil
  :load-path "/src/emacs/bookmark-in-project"

  :config

  ;; Always save after every change.
  (setq bookmark-save-flag 0))


;; ----------------------------------------------------------------------------
;; Local Functionality
;; ###################
;;
;; These are installed locally, (files in home dir)
;; not part of a package manager.

;; Clipboard History
;; =================
;;
;; Support accessing older states of the clipboard.
(when local-cfg/use-gui-clipboard-history
  (require 'gui-clipboard-history-mode)
  (gui-clipboard-history-mode 1))

;; -------------------------------
;; Cycle Split Horizontal/Vertical
;; ===============================

(defun toggle-window-split ()
  (interactive)
  (when (= (count-windows) 2)
    (let* ((this-win-buffer (window-buffer))
           (next-win-buffer (window-buffer (next-window)))
           (this-win-edges (window-edges (selected-window)))
           (next-win-edges (window-edges (next-window)))
           (this-win-2nd
            (not
             (and (<= (car this-win-edges) (car next-win-edges))
                  (<= (cadr this-win-edges) (cadr next-win-edges)))))
           (splitter
            (cond
             ((= (car this-win-edges) (car (window-edges (next-window))))
              'split-window-horizontally)
             (t
              'split-window-vertically))))
      (delete-other-windows)
      (let ((first-win (selected-window)))
        (funcall splitter)
        (when this-win-2nd
          (other-window 1))
        (set-window-buffer (selected-window) this-win-buffer)
        (set-window-buffer (next-window) next-win-buffer)
        (select-window first-win)
        (when this-win-2nd
          (other-window 1))))))


;; --------------------------
;; Spell-Fu Doxy Highlighting
;; ==========================

;; Mini local package for fancy comment highlighting.
;; This might be possible to replace with contextual highlighting - needs investigation.

;; -------------------------
;; Doxy Keyword Highlighting
;; =========================

;; Minor mode for highlighting doxygen comments.

(when local-cfg/use-c-comment-pkg
  (use-package prog-face-refine
    :commands (prog-face-refine-mode)

    :ensure nil
    :load-path "/src/emacs/prog-face-refine"))

(when local-cfg/use-comment-keywords
  (use-package hl-prog-extra
    :commands (hl-prog-extra-mode)
    :ensure nil
    :load-path "/src/emacs/hl-prog-extra"))

(use-package doc-show-inline
  :commands (doc-show-inline-mode doc-show-inline-printf doc-show-inline-goto)
  :ensure nil
  :load-path "/src/emacs/doc-show-inline"

  :config
  ;; Skip doxygen groups.
  ;; (setq doc-show-inline-exclude-regexp "\\(\\\\{\\|\\\\}\\)")
  (setq doc-show-inline-exclude-blank-lines 1)

  ;; Ensure the comment region is spell checked.

  (add-hook
   'doc-show-inline-buffer-hook
   (lambda ()
     ;; Custom settings for this project.

     ;; Enable additional highlighting.
     (hl-prog-extra-mode)

     (sidecar-locals-mode)
     ;; Spell check the comments.
     (spell-fu-mode 1)

     ;; Use special DOXYGEN highlighting.
     (cond
      (local-cfg/use-c-comment-pkg
       (prog-face-refine-mode)))))

  (add-hook 'doc-show-inline-fontify-hook (lambda (beg end) (spell-fu-region beg end))))


;; ----------------------------------------------------------------------------
;; Local Library Functions
;; #######################
;;

(defun is-wayland ()
  (and (eq (window-system) 'pgtk) (string-equal (or (getenv "XDG_SESSION_TYPE") "") "wayland")))


;; Could be moved externally.

;; --------------------------------
;; Shell Command on Region & Select
;; ================================

;; While Emacs `shell-command-on-region' operates on the selection,
;; the resulting text isn't selected.
;; This is a utility command that keeps the selection for the
;; resulting output. With support for `evil-mode' (when in use).

(defun shell-command-on-region-and-select
    (start
     end
     command
     &optional
     output-buffer
     replace
     error-buffer
     display-error-buffer
     region-noncontiguous-p)
  "Wrapper for 'shell-command-on-region', re-selecting the output.

Useful when called with a selection, so it can be modified in-place."
  (interactive)
  (let ((buffer-size-init (buffer-size)))
    (shell-command-on-region start end command
                             output-buffer
                             replace
                             error-buffer
                             display-error-buffer
                             region-noncontiguous-p)
    (setq deactivate-mark nil)
    (setq end (+ end (- (buffer-size) buffer-size-init)))
    (set-mark start)
    (goto-char end)
    (activate-mark)
    ;; needed for evil line mode
    (when (and (boundp evil-state) (string= evil-state "visual"))
      (when (eq (evil-visual-type) evil-visual-line)
        (evil-visual-select start end 'line)))))

;; --------------------
;; Run xdg-open on Text
;; ====================

(defun xdg-open-wrapper (thing-to-open)
  (with-temp-buffer
    (cond
     ((zerop (call-process "xdg-open-i42" nil (current-buffer) nil thing-to-open))
      (message "Open: '%s'" thing-to-open))
     (t
      (message "Error: '%s'" (buffer-string))))))

(defun xdg-open-thing-at-point-in-comments ()
  (let ((faces-found (face-at-point nil t))
        (faces-comment-list
         '(font-lock-comment-face font-lock-comment-delimiter-face font-lock-doc-face)))
    (cond
     ((seq-intersection faces-found faces-comment-list)
      ;; Run thing-at-point on our own regex.
      (save-match-data
        (when (thing-at-point-looking-at
               (concat
                "\\("
                "T[[:digit:]]+\\|" ;; Bug report.
                "\\(https?\\|ftp\\)://\\S-+" ;; URL.
                "\\)"))
          (xdg-open-wrapper (match-string-no-properties 0))
          t)))
     (t
      nil))))

(defun xdg-open-or-find-definition ()
  (interactive)
  (cond
   ((xdg-open-thing-at-point-in-comments)
    t)
   ((bound-and-true-p lsp-mode)
    (lsp-find-definition))
   (t
    ;; Used for `eglot' or other modes.
    (call-interactively 'xref-find-definitions))))

;; -------------------
;; Reusable Popup Menu
;; ===================
;;
;; See: https://emacs.stackexchange.com/a/35033/2418

(defun my-custom-popup (prompt default-index content)
  "Pop up menu
Takes args: prompt, default-index, content).
Where the content is any number of (string, function) pairs,
each representing a menu item."
  (cond
   ;; Ivy (optional)
   ((and local-cfg/use-ivy (fboundp 'ivy-read))
    (ivy-read
     prompt content
     :preselect
     (cond
      ((= default-index -1)
       nil)
      (t
       default-index))
     :require-match t
     :action
     (lambda (x)
       (pcase-let ((`(,_text . ,action) x))
         (funcall action)))
     :caller #'my-custom-popup))

   ;; Fallback to completing read.
   (t
    (let ((choice
           (funcall completing-read-function
                    prompt
                    content
                    nil
                    t
                    nil
                    nil
                    (nth default-index content))))
      (pcase-let ((`(,_text . ,action) (assoc choice content)))
        (funcall action))))))

(setq inhibit-startup-screen t)
(setq initial-scratch-message nil)

(setq initial-buffer-choice
      (lambda ()
        (cond
         ((buffer-file-name)
          (current-buffer))
         (t
          (i42-scratch-buffer-from-file (concat user-emacs-directory "scratch.org"))))))

;; Flash the mode-line as a visual bell.
;; (stackexchange-mode-line-visual-bell)
(setq visible-bell t)

;; ----------------------------------------------------------------------------
;; Display Options
;; ###############

;; show parens
(show-paren-mode 1)
(setq show-paren-delay 0.2)
(setq show-paren-highlight-openparen t)
(setq show-paren-when-point-inside-paren t)

;; Surprisingly this slows down startup.
;; (cond
;;   ((and local-cfg/use-git-gutter (not local-cfg/use-linum)) ;; Show left fringe.
;;     (fringe-mode '(2 . 0)))
;;   (t ;; Hide the fringe entirely.
;;     (set-fringe-mode 0)))

;; no startup screen
(setq inhibit-startup-screen t)

;; disable word-wrap
(set-default 'truncate-lines t)

;; line numbers
(when local-cfg/use-linum
  (global-display-line-numbers-mode t))

;; ----------
;; Font Cycle
;; ==========

(when (display-graphic-p)

  (use-package default-font-presets
    :commands
    (default-font-presets-forward
     default-font-presets-backward
     default-font-presets-scale-increase
     default-font-presets-scale-decrease
     default-font-presets-scale-reset
     default-font-presets-scale-fit)
    :ensure nil
    :load-path "/src/emacs/default-font-presets"

    :config
    (setq default-font-presets-list
          (list
           ;; Short list.
           "Berkeley Mono-14"
           "Best Choice-14"
           "Calling Code-14"
           "Kiwari Mono-14"
           "SouvenirMonoCode-14"
           "TopazNG Code-16"

           ;; Good but disable.

           ;; "Cascadia Code-13:spacing=90"
           ;; "Hermit Medium 13"
           ;; "IBM 3270 17"
           ;; "JetBrains Mono-13:spacing=100"
           ;; "IBM Plex Mono 13"
           ;; "PragmataPro-16"
           ;; "Source Code Pro Medium 14"
           ;; "Spleen 16x32 24"
           ;; "Triskweline Medium 11"
           ;; "Victor Mono-15"
           ;; "agave-14.9"

           ;; Not installed.

           ;; "Anonymous Pro Medium 14"
           ;; "Consolas 15"
           ;; "Fantasque Sans Mono Medium 14"
           ;; "Fira Code Medium 13"
           ;; "GohuFont Medium 11"
           ;; "GohuFont Medium 8"
           ;; "Inconsolata-g 13" ;; broken
           ;; "Input MonoLight 15"
           ;; "Iosevka Medium 17"
           ;; "Luculent Medium 16"
           ;; "Luculent-14"
           ;; "Meslo LG S DZ Medium 13"
           ;; "Monoid Medium 13"
           ;; "ProFontIIx-14"
           ;; "Source Code Pro Medium 12" ;; 14 for 100 width
           ;; "Source Code Pro Medium 14" ;; for 4k vertical monitor
           ;; "Source Code Pro Semi-Bold 10" ;; 14 for 100 width
           ;; "Sudo 14"
           ;; "System1-iso" ;; cause continuous errors
           ;; "System2-iso"
           ;; "Tamzen Medium 16"
           ;; "k knxt Medium 11"
           ))))

;; --------------
;; Line Highlight
;; ==============

(with-eval-after-load 'hl-line
  (when local-cfg/use-hline
    (when local-cfg/use-hline-indent
      (eval-when-compile
        (require 'hl-line))

      ;; EOL hl-line, less intrusive.
      (defun my-hl-line-range-function ()
        (cons (line-end-position) (line-beginning-position 2)))

      ;; don't draw over indentation (annoying/distracting with indent guides)
      ;; (defun my-hl-line-range-function ()
      ;;   (cons
      ;;     (save-excursion
      ;;       (back-to-indentation)
      ;;       (point))
      ;;     (line-beginning-position 2)))
      (setq hl-line-range-function #'my-hl-line-range-function))))

;; -----------
;; White Space
;; ===========

;; highlight non-aligning indent offset
(defun highlight-indent-offset ()
  "Draw the highlight line after indentation."
  (font-lock-add-keywords
   nil
   `((,(lambda (limit)
         (re-search-forward
          ;; evaluates to "^ \\{4\\}*\\( \\{1,3\\}\\)[^ ]" when tab width is 4.
          (format "^ \\{%d\\}*\\( \\{1,%d\\}\\)[^ ]" tab-width (- tab-width 1))
          limit t))
      1 'whitespace-trailing))))

;; White Space: XXX, we can't use this else realtime toggle doesn't work.
;; (whitespace-mode t)

;; make whitespace-mode use just basic coloring
(defun local-cfg/display-whitespace-on ()
  (whitespace-mode 1)
  (setq whitespace-style
        ;; spaces space space-mark lines-tail
        '(face tabs tab tab-mark space-before-tab))

  (cond
   ((eq local-cfg/theme 'inkpot)
    (custom-set-faces
     '(whitespace-line ((t (:background "#660000"))))
     '(whitespace-tab ((t (:background "#14141a"))))))

   ((eq local-cfg/theme 'oblivion)
    (custom-set-faces
     '(whitespace-line ((t (:background "#660000"))))
     '(whitespace-tab ((t (:background "#23292b")))))))

  (setq-local whitespace-display-mappings
              '((space-mark 32 [183] [46]) ; middle-dot
                ;; (tab-mark 9 [9654 9] [92 9]) ; arrow
                ;; (tab-mark 9 [9615 9] [92 9]) ; Left one eighth block
                ;; (tab-mark 9 [9614 9] [92 9]) ; Left one quater block
                ;; (tab-mark 9 [126 9] [92 9]) ; ~
                (tab-mark ?\t [?\u00BB ?\t] [?\\ ?\t]) ; double arrow (more common in fonts).
                ))
  (whitespace-toggle-options '(whitespace-style)))

(defun local-cfg/display-whitespace-off ()
  (whitespace-mode 0))

;; for key binding
(defun show-whitespace-toggle ()
  (interactive)
  (cond
   (local-cfg/use-show-whitespace
    (setq local-cfg/use-show-whitespace nil))
   (t
    (setq local-cfg/use-show-whitespace t)))

  (cond
   (local-cfg/use-show-whitespace
    (local-cfg/display-whitespace-on))
   (t
    (local-cfg/display-whitespace-off))))

(defun my-insert-into-list (list el n)
  "Insert into list LIST an element EL at index N.

If N is 0, EL is inserted before the first element.

The resulting list is returned.  As the list contents is mutated
in-place, the old list reference does not remain valid."
  (let* ((padded-list (cons nil list))
         (c (nthcdr n padded-list)))
    (setcdr c (cons el (cdr c)))
    (cdr padded-list)))

;; ---------
;; Mode Line
;; =========
;;
;; A fairly simple mode-line (not too different to the default)
;; with buffer and position info on the left, active modes on the right.
;;
;; The aim is to keep all important info on the left to see at a glance,
;; other details on the right in case they're needed.


;; Show column in status, not the line.
(when local-cfg/use-linum
  (setq line-number-mode nil))
(setq column-number-mode t)

;; ----------
;;
(defvar my-which-func-timer nil)

;; Defer running `which-func-ff-hook' because eglot may not be initialized.
(defun my-which-func-ff-hook-advice (orig-fun &rest args)
  ;; Only run the IMENU for buffers with a filename, avoids issues with temporary buffers.
  (when buffer-file-name
    (let* ((this-buf (current-buffer))
           (this-timer my-which-func-timer))
      (when this-timer
        (cancel-timer this-timer))
      (setq-local my-which-func-timer
                  (run-with-idle-timer 0.7654321 nil
                                       #'(lambda (buf args)
                                           (when (buffer-live-p buf)
                                             (with-current-buffer buf
                                               (apply orig-fun args))))
                                       this-buf args)))))

(with-eval-after-load 'which-func
  (advice-add 'which-func-ff-hook :around #'my-which-func-ff-hook-advice))

;; Doesn't need to run on startup, turn on global mode a bit later.
(run-with-idle-timer 0.54 nil #'(lambda () (which-function-mode 1)))

;; Write a function to do the spacing.
;; disable for now.
(when local-cfg/use-custom-mode-line

  ;; We might want to add these.
  ;; mode-line-frame-identification mode-line-modes mode-line-misc-info

  ;; Without this, there `mode-line-idle' may not load at all.
  ;; Ideally it could load lazily but the hassle isn't worth it.
  (require 'mode-line-idle)
  (setq which-func-update-delay 0.7891)

  (setq-default mode-line-format
                (append
                 (list
                  ;; Left align.
                  "%e "
                  ;; Buffer ID.
                  (propertized-buffer-identification "%8b")

                  ;; Insertion index for mode (index=2).
                  "[%*] "

                  ;; Current function (when enabled).
                  '(:eval
                    (when (bound-and-true-p which-function-mode)
                      (list "(" which-func-current ") ")))

                  '(:eval
                    (when my-mode-line-idle
                      (mode-line-idle 0.23 my-mode-line-idle "???"))))

                 (cond
                  (local-cfg/use-jit-lock-stealth
                   my-mode-line-location-with-jit-lock-stealth)
                  (t
                   my-mode-line-location))))

  (when local-cfg/use-evil
    (with-eval-after-load 'evil
      (setq-default mode-line-format
                    (my-insert-into-list
                     (default-value 'mode-line-format) (list t 'evil-mode-line-tag) 2))))

  (when local-cfg/use-bray
    (setq-default mode-line-format
                  (my-insert-into-list
                   (default-value 'mode-line-format) 'bray-modeline-string 2))))

;; MMB, copy filepath
(define-key
 mode-line-buffer-identification-keymap [mode-line mouse-2]
 (lambda ()
   (interactive)
   (let ((filename
          (cond
           ((eq major-mode 'dired-mode)
            default-directory)
           (t
            (buffer-file-name)))))
     (cond
      (filename
       (cond
        ((is-wayland)
         (gui-set-selection 'CLIPBOARD filename))
        (t
         (gui-set-selection 'PRIMARY filename)))
       (message "Copied to primary clipboard"))
      (t
       (message "Unable to copy the filename")
       (sit-for 2))))))

;; --------------------
;; Display for Packages
;; ====================

;; ----------------------------------------------------------------------------
;; Editing Options
;; ###############

;; ----------------
;; Case Sensitivity
;; ================

;; case sensitive search
(setq-default case-fold-search nil)

;; Don't adjust case for search (ivy uses this for case-insensitive grep/ag/rg).
(setq-default search-upper-case nil)

(when local-cfg/use-search-evil
  (setq evil-ex-search-case 'sensitive))

;; center on search next/previous (convenient)

;; auto-complete used by evil
(setq dabbrev-case-fold-search nil)

;; paste at cursor instead of cursor
(setq mouse-yank-at-point t)

;; Don't delimit on _'s.
;; note that some modes need this to be set for their own syntax table.
;; (modify-syntax-entry ?_ "w")

;; -----------
;; Indentation
;; ===========

;; yes, both are needed!
(setq default-tab-width 4)
(setq tab-width 4)
(setq default-fill-column 80)
(setq fill-column 80)
(setq-default evil-indent-convert-tabs nil)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq-default evil-shift-round nil)

;; needs to be done before python hook
(setq-default python-indent-guess-indent-offset nil)

;; --------
;; Packages
;; ========

;; spelling
(when (not noninteractive)
  ;; Too slow!
  ;;(add-hook 'flyspell-mode-hook 'flyspell-buffer)

  (setq flyspell-issue-message-flag nil)
  (setq flyspell-issue-welcome-flag nil))

;; display bookmarks
;; .. disable, it's a little annoying.
;; (evil-visual-mark-mode)

;; Extend evil mode
(with-eval-after-load 'evil-ex
  (evil-ex-define-cmd "retab" 'untabify))

;; Being able to hook into which-function is very useful.
;; Add my own generic way to do this, so I can have mode-local which-function additions.
(defvar-local my-mode-line-idle (list))


;; ----------------------------------------------------------------------------
;; Local Generic Functions
;; #######################
;;
;; Modes can set these functions when they're supported.

(defmacro my-with-advice (advice &rest body)
  "Execute BODY with ADVICE temporarily enabled.

Advice are triplets of (SYMBOL HOW FUNCTION),
see `advice-add' documentation."
  (declare (indent 1))
  (let ((advice-list advice)
        (body-let nil)
        (body-advice-add nil)
        (body-advice-remove nil)
        (item nil))
    (unless (listp advice-list)
      (error "Advice must be a list"))
    (cond
     ((null advice-list)
      (error "Advice must be a list containing at least one item"))
     (t
      (while (setq item (pop advice-list))
        (unless (and (listp item) (eq 3 (length item)))
          (error "Each advice must be a list of 3 items"))
        (let ((fn-sym (gensym))
              (fn-advise (pop item))
              (fn-advice-ty (pop item))
              (fn-body (pop item)))
          ;; Build the calls for each type.
          (push (list fn-sym fn-body) body-let)
          (push (list 'advice-add fn-advise fn-advice-ty fn-sym) body-advice-add)
          (push (list 'advice-remove fn-advise fn-sym) body-advice-remove)))
      (setq body-let (nreverse body-let))
      (setq body-advice-add (nreverse body-advice-add))

      ;; Compose the call.
      `(let ,body-let
         (unwind-protect
             (progn
               ,@body-advice-add
               ,@body)
           ,@body-advice-remove))))))

(defmacro my-generic-fn-with-fallback (var description &rest body)
  (declare (indent 2))
  `(progn
     (defun ,var ()
       ,description
       (interactive)
       (call-interactively ,var))
     (defvar-local ,var ,@body
       "Value storing a function, called by a function of the same name.")))

(defmacro my-generic-fn (var description)
  `(my-generic-fn-with-fallback ,var ,description
     (lambda ()
       (interactive)
       (message "Mode <%s> doesn't define <%s>, doing nothing!" major-mode ',var))))


(my-generic-fn-with-fallback my-generic-line-break-below "Line break below"
  'vi-open-line-below)
(my-generic-fn-with-fallback my-generic-line-break-above "Line break above"
  'vi-open-line-above)
(my-generic-fn-with-fallback my-generic-line-break-split "Line break split current line"
  'indent-new-comment-line)

(my-generic-fn my-generic-run "Run the current buffer")
(my-generic-fn my-generic-build "Build the current buffer")
(my-generic-fn-with-fallback my-generic-goto-thing-at-point "Go to the symbol at the cursor"
  'xref-find-definitions)
(my-generic-fn-with-fallback my-generic-usage-of-thing-at-point
    "Show references to the symbol at the cursor"
  'xref-find-references)
(my-generic-fn-with-fallback my-generic-eval-selection "Evaluate the selection"
  'eval-region-as-py)

(my-generic-fn-with-fallback my-generic-rename-thing-at-point "Rename to the symbol at the cursor"
  'lsp-rename)

(my-generic-fn-with-fallback my-generic-jump-next "Jump forward"
  'evil-jump-forward)
(my-generic-fn-with-fallback my-generic-jump-prev "Jump backward"
  'evil-jump-backward)

(my-generic-fn my-generic-doc-jump-next "Jump forward (docs)")
(my-generic-fn my-generic-doc-jump-prev "Jump backward (docs)")

(my-generic-fn-with-fallback my-generic-doc-jump-section "Jump to sections in this buffer"
  'imenu)

(my-generic-fn my-generic-comment-block "Insert a comment block")
(my-generic-fn my-generic-comment-block-docstring "Insert a comment block (doc-string)")

(my-generic-fn-with-fallback my-generic-fix-action "Fix/correct error at point"
  (cond
   (local-cfg/use-eglot
    'eglot-code-action-quickfix)
   (t
    'lsp-execute-code-action)))


;; ----------------------------------------------------------------------------
;; File Type Hooks
;; ###############

;; -------------------------
;; Programming Overlay Modes
;; =========================
;;
;; Special case, keep overlays last
;; can't use general hook here, each mode must call.

(add-hook
 'after-change-major-mode-hook
 (lambda ()

   ;; Global modes:
   ;; Enable these modes for all non-special buffers (files).
   (when (and (not (minibufferp)) (not (derived-mode-p 'special-mode)))

     ;; Run this before key bindings.
     ;; Modes which must NOT activate evil-mode.

     (when local-cfg/use-evil
       (when (not
              (memq
               major-mode
               (list
                ;; Lisp REPL (`ielm').
                'inferior-emacs-lisp-mode
                ;; Terminal.
                'term-mode)))
         (evil-local-mode 1)))

     (when local-cfg/use-bray
       (when (eq major-mode 'git-lite-commit-mode)
         (setq bray-state-init (list 'normal 'insert)))

       (bray-mode))

     ;; Mono-complete.
     (cond
      ((or (derived-mode-p 'prog-mode) (memq major-mode (list 'rst-mode 'markdown-mode 'org-mode)))

       ;; "" ;; Project root.
       ;; "/src/blender/"
       ;; "/home/ideasman42/nltk_data/corpora/gutenberg/"
       ;; "/home/ideasman42/nltk_data/corpora/inaugural/"
       ;; "/scratch/src/emacs/"
       ;; "/src/emacs/"
       (setq mono-complete-backend-word-predict-input-paths (list "/scratch/src/ngram_mega_pack"))

       (mono-complete-mode))
      ((eq major-mode 'git-lite-commit-mode)
       (setq mono-complete-backend-word-predict-input-paths
             (list "/scratch/src/ngram_mega_pack/blender_commit_log.txt"))

       (setq mono-complete-backends (lambda (is-context) (list 'word-predict 'dabbrev)))
       (mono-complete-mode)))

     (when local-cfg/use-ivy
       (ivy-mode 1))

     (when local-cfg/use-undo-fu-session
       (undo-fu-session-mode 1))

     (when local-cfg/use-company
       (company-mode))

     (when local-cfg/use-vertico
       (vertico-mode t))

     (when local-cfg/use-git-gutter
       (git-gutter-mode))

     (when local-cfg/use-diff-hl
       (diff-hl-mode))

     (when local-cfg/use-hline
       (hl-line-mode t))

     (unless buffer-read-only
       (repeat-fu-mode))

     (when local-cfg/use-hl-block
       (setq-local hl-block-delay 0.05)
       (cond
        ((memq
          major-mode
          (list
           'c-mode 'c++-mode 'glsl-mode 'rust-mode 'emacs-lisp-mode 'python-mode
           ;; Tree-sitter modes.
           'c-ts-mode 'c++-ts-mode 'python-ts-mode))
         (hl-block-mode t))))

     (unless (memq major-mode (list 'text-mode 'compilation-mode))
       (font-lock-mode)

       (when (derived-mode-p 'prog-mode)
         (when local-cfg/use-number-color
           (unless (memq major-mode (list 'text-mode 'compilation-mode 'diff-mode))
             (highlight-numbers-mode 1))))))

   ;; Local hooks.
   (when local-cfg/use-evil
     (when local-cfg/use-highlight-idle-mode
       ;; Only while in insert mode.
       (add-hook 'evil-insert-state-entry-hook (lambda () (idle-highlight-mode 0)) nil t)
       (add-hook 'evil-insert-state-exit-hook (lambda () (idle-highlight-mode 1)) nil t)))

   (when (derived-mode-p 'text-mode)

     (cond
      (local-cfg/use-show-whitespace
       (local-cfg/display-whitespace-on))
      (t
       (local-cfg/display-whitespace-off))))


   (when local-cfg/use-imenu-sidebar
     (cond
      ((derived-mode-p 'diff-mode)
       (imenu-list-minor-mode 0))

      ((derived-mode-p 'prog-mode)
       (imenu-list-minor-mode 1))))

   (when (derived-mode-p 'prog-mode)

     (when local-cfg/use-highlight-idle-mode
       (idle-highlight-mode))

     ;; No so nice for lisp as '-' is used in names.
     (unless (derived-mode-p 'emacs-lisp-mode)
       (highlight-operators-mode 1))

     (cond
      (local-cfg/use-show-whitespace
       (local-cfg/display-whitespace-on))
      (t
       (local-cfg/display-whitespace-off)))

     (when local-cfg/use-code-folding

       ;; Shows annoying "Showing all" message for every file loaded otherwise.
       (let ((inhibit-message t)
             (message-log-max nil))
         (hs-minor-mode))

       (hs-hide-initial-comment-block)))

   (when local-cfg/use-ispell
     (when (not buffer-read-only)
       (when (or (derived-mode-p 'prog-mode) (derived-mode-p 'text-mode))
         (my-generic-spell-check-enable))))

   ;; Rule out mini-buffer & file browsing.
   (when (or (derived-mode-p 'prog-mode) (derived-mode-p 'text-mode) (derived-mode-p 'conf-mode))
     (display-fill-column-indicator-mode))))


;; -----------
;; Mini-Buffer
;; ===========

(add-hook 'minibuffer-mode-hook (lambda () (modify-syntax-entry ?- "w")))

(add-hook
 'minibuffer-setup-hook
 (lambda ()

   ;; Disabled globally but useful in the mini-buffer.
   (cond
    (local-cfg/use-bray
     (local-set-key (kbd "C-r") 'bray-ex-yank-replace-from-clipboard-only)

     (local-set-key (kbd "C-<backspace>") 'bray-ex-backward-kill-symbol)
     (local-set-key (kbd "C-<delete>") 'bray-ex-kill-symbol))
    (t
     (local-set-key (kbd "C-<backspace>") 'backward-kill-word)
     (local-set-key (kbd "C-<delete>") 'kill-word)))))


;; --------
;; Spelling
;; ========

;; Dictionary for completion (generated on demand).
(setq ispell-complete-word-dict
      (expand-file-name (concat user-emacs-directory "aspell_words.txt")))

(defun my-generic-ispell-company-complete-setup ()
  ;; Only apply this locally.
  (make-local-variable 'company-backends)
  (setq company-backends (list 'company-ispell))

  (when ispell-complete-word-dict
    (let* ((has-dict-complete (file-exists-p ispell-complete-word-dict))
           (has-dict-personal
            (and ispell-personal-dictionary (file-exists-p ispell-personal-dictionary)))
           (is-dict-outdated
            (and has-dict-complete
                 has-dict-personal
                 (time-less-p
                  (nth 5 (file-attributes ispell-complete-word-dict))
                  (nth 5 (file-attributes ispell-personal-dictionary))))))

      (when (or (not has-dict-complete) is-dict-outdated)
        (with-temp-buffer

          ;; Optional: insert personal dictionary, stripping header and inserting a newline.
          (when has-dict-personal
            (insert-file-contents ispell-personal-dictionary)
            (goto-char (point-min))
            (when (looking-at "personal_ws\-")
              (delete-region (line-beginning-position) (1+ (line-end-position))))
            (goto-char (point-max))
            (unless (eq ?\n (char-after))
              (insert "\n")))

          (call-process "aspell" nil t nil "-d" "en_US" "dump" "master")
          ;; Case insensitive sort is important for the lookup.
          ;; Require before `sort-fold-case' is needed, so lexical binding works as expected.
          (require 'sort)
          (let ((sort-fold-case t))
            (sort-lines nil (point-min) (point-max)))
          (write-region nil nil ispell-complete-word-dict))))))

(defun my-generic-spell-check-enable ()
  (when local-cfg/use-spelling-flyspell
    (when (or (derived-mode-p 'prog-mode) (derived-mode-p 'text-mode))
      (cond
       ((derived-mode-p 'prog-mode)
        (flyspell-prog-mode))
       (t
        (flyspell-mode)))))

  (when local-cfg/use-spelling-flyspell-visible
    (when (or (derived-mode-p 'prog-mode) (derived-mode-p 'text-mode))
      (require 'flyspell-visible-mode)
      (flyspell-visible-mode)))

  (when local-cfg/use-spelling-spell-fu
    (when (or (derived-mode-p 'prog-mode) (derived-mode-p 'text-mode))
      (cond
       ((bound-and-true-p git-lite-commit-mode)
        (setq spell-fu-faces-exclude (list 'font-lock-comment-face 'font-lock-constant-face)))
       ((eq major-mode 'markdown-mode)
        (setq spell-fu-faces-exclude
              '(markdown-code-face
                markdown-html-attr-value-face
                markdown-html-tag-name-face
                markdown-inline-code-face
                markdown-plain-url-face
                markdown-pre-face
                markdown-url-face)))
       ((eq major-mode 'org-mode)
        (setq spell-fu-faces-exclude '(org-meta-line org-link org-code))))


      (spell-fu-mode t))))

(defun my-generic-spell-check-disable ()
  (when local-cfg/use-spelling-flyspell
    (flyspell-mode 0)
    (flyspell-delete-all-overlays))

  (when local-cfg/use-spelling-flyspell-visible
    (flyspell-visible-mode 0)
    (flyspell-delete-all-overlays))

  (when local-cfg/use-spelling-spell-fu
    (when (or (derived-mode-p 'prog-mode) (derived-mode-p 'text-mode))
      (spell-fu-mode 0))))

;; ---------
;; Languages
;; =========

(with-eval-after-load 'cc-mode
  ;; Generic doxygen formatting
  ;; Matches: /** doxy */  but not /***/
  (cond
   (t
    (defconst custom-font-lock-keywords
      `((,(lambda (limit) (c-font-lock-doc-comments "/\\*\\*[^\\*]" limit nil)))))
    (setq-default c-doc-comment-style (quote (custom))))))

;; Operator Fonts

;; Scripting
(dolist (mode-iter '(python-mode lua-mode toml-mode))
  (font-lock-add-keywords mode-iter '(("\\([\]\[}{)(:;]\\)" 0 'font-lock-delimiter-face keep))))

;; Lisp-Like
(dolist (mode-iter '(emacs-lisp-mode))
  (font-lock-add-keywords
   mode-iter '(("\\([\]\[}{)(:;\\'`]\\)" 1 'font-lock-delimiter-face keep))))

;; CMake
(dolist (mode-iter '(cmake-mode))
  (font-lock-add-keywords mode-iter '(("\\([\]\[<>)(:;]\\)" 0 'font-lock-delimiter-face keep))))

(autoload 'i42-major-mode-c-generic "i42-major-mode-c-generic")
(add-hook 'c-mode-hook #'i42-major-mode-c-generic)
(add-hook 'c++-mode-hook #'i42-major-mode-c-generic)
(add-hook 'objc-mode #'i42-major-mode-c-generic)
(add-hook 'objc++-mode #'i42-major-mode-c-generic)
(add-hook 'c-ts-mode-hook #'i42-major-mode-c-generic)
(add-hook 'c++-ts-mode-hook #'i42-major-mode-c-generic)

(autoload 'i42-major-mode-glsl "i42-major-mode-glsl")
(add-hook 'glsl-mode-hook #'i42-major-mode-glsl)

(autoload 'i42-major-mode-haskell "i42-major-mode-haskell")
(add-hook 'haskell-mode-hook #'i42-major-mode-haskell)

(autoload 'i42-major-mode-rust "i42-major-mode-rust")
(add-hook 'rust-mode-hook #'rust-mode-hook)

(autoload 'i42-major-mode-nix "i42-major-mode-nix")
(add-hook 'nix-mode-hook #'i42-major-mode-nix)

;; keep toml similar to rust
(autoload 'i42-major-mode-toml "i42-major-mode-toml")
(add-hook 'toml-mode-hook #'i42-major-mode-toml)

;; -------
;; Scripts
;; =======

(autoload 'i42-major-mode-lua "i42-major-mode-lua")
(add-hook 'lua-mode-hook #'i42-major-mode-lua)

(autoload 'i42-major-mode-python "i42-major-mode-python")
(add-hook 'python-mode-hook 'i42-major-mode-python)
(add-hook 'python-ts-mode-hook 'i42-major-mode-python)

(autoload 'i42-major-mode-sh "i42-major-mode-sh")
(add-hook 'sh-mode-hook #'i42-major-mode-sh)

(add-hook
 'fish-mode-hook
 (lambda ()
   (setq-local fill-column 120)
   (setq-local tab-width 4)
   (setq-local evil-shift-width 4)
   (setq-local indent-tabs-mode nil)
   (highlight-indent-offset)

   (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

   (i42-path-visibility-setup
    :exclude-paths '("*/\\.*" "./*__pycache__/*")
    :include-names '("*.fish"))

   ;; Generic functions.
   (setq my-generic-doc-jump-section 'i42-doc-jump-section)))


;; ---------------

;; ---------------


(autoload 'i42-major-mode-emacs-lisp "i42-major-mode-emacs-lisp")
(add-hook 'emacs-lisp-mode-hook #'i42-major-mode-emacs-lisp)

;; ------------
;; Build-System
;; ============

(autoload 'i42-major-mode-makefile "i42-major-mode-makefile")
(add-hook 'makefile-mode-hook #'i42-major-mode-makefile)

(autoload 'i42-major-mode-cmake "i42-major-mode-cmake")
(add-hook 'cmake-mode-hook #'i42-major-mode-cmake)
(add-hook 'cmake-ts-mode-hook #'i42-major-mode-cmake)

;; ------
;; Markup
;; ======

(autoload 'i42-major-mode-org "i42-major-mode-org")
(add-hook 'org-mode-hook #'i42-major-mode-org)

(autoload 'i42-major-mode-rst "i42-major-mode-rst")
(add-hook 'rst-mode-hook #'i42-major-mode-rst)

(autoload 'i42-major-mode-markdown "i42-major-mode-markdown")
(add-hook 'markdown-mode-hook #'i42-major-mode-markdown)

(autoload 'i42-major-mode-css "i42-major-mode-css")
(add-hook 'css-mode-hook #'i42-major-mode-css)

;; ----
;; Text
;; ====

(autoload 'i42-major-mode-html "i42-major-mode-html")
(add-hook 'html-mode-hook #'i42-major-mode-html)

;; CSV

(defvar i42-csv-mode-hook nil)

;;Highlight various elements
(setq i42-csv-hlite '(("," . font-lock-keyword-face)))

(define-derived-mode
 i42-csv-mode
 fundamental-mode
 "CSV"
 "Major mode for editing Workflow Process Description Language files."

 (i42-path-visibility-setup
  :exclude-paths '("*/\\.*" "./*__pycache__/*")
  :include-names '("*.csv" "*.rules"))

 (setq font-lock-defaults '((i42-csv-hlite)))

 ;; Don't delimit on ":"
 (modify-syntax-entry ?: "w"))

(add-to-list 'auto-mode-alist '("\\.csv\\'" . i42-csv-mode))

(add-hook
 'compilation-mode-hook
 (lambda () (define-key compilation-mode-map [down-mouse-2] 'scroll-on-drag)))

(when local-cfg/use-code-folding

  (with-eval-after-load 'hideshow

    ;; Configuration for nicer hide-show behavior.
    (defun my-hs-display-code-line-counts (ov)
      (overlay-put
       ov 'display
       (concat
        " "
        (propertize (format "(.. %d)" (count-lines (overlay-start ov) (overlay-end ov)))
                    'face ;; Stand out!
                    'diff-file-header)
        " ")))

    (defun my-c-comment-end-skip-backward (pos)
      (cond
       ((and (eq (char-before pos) ?/) (eq (char-before (- pos 1)) ?*))
        (- pos 2))
       (t
        pos)))

    (defun my-c-comment-hs-make-overlay-advice-fn (old-fn b e kind &optional b-ofs e-ofs)
      (when (and (eq kind 'comment)
                 ;; Check the language uses C/C++ comments.
                 (eq
                  major-mode
                  (list
                   'c-mode 'c++-mode 'objc-mode 'glsl-mode 'rust-mode
                   ;; Tree-sitter modes.
                   'c-ts-mode 'c++-ts-mode)))

        ;; Check for trailing "*/" at the beginning.
        ;; This can happen at the beginning position when multiple comments
        ;; are collapsed, and the first comment is a single-line comment.
        (setq b (my-c-comment-end-skip-backward b))
        (when b-ofs
          (setq b-ofs (max b b-ofs)))

        ;; Check for trailing "*/".
        (setq e (my-c-comment-end-skip-backward e))
        (when e-ofs
          (setq e-ofs (min e e-ofs))))
      (funcall old-fn b e kind b-ofs e-ofs))

    (defun my-hs-toggle-hiding-all ()
      (interactive)
      (let ((l_prev (length (overlays-in (point-min) (point-max)))))
        (save-excursion
          (hs-show-all)
          (hs-hide-initial-comment-block)
          (let ((l_curr (length (overlays-in (point-min) (point-max)))))
            (when (eq l_prev l_curr)
              (hs-hide-all)))))
      (recenter))

    (defun my-comment-bounds ()
      (interactive)
      (let ((state (syntax-ppss))
            (point-init (point)))

        ;; We may be at the comment start, which isn't considered a comment.
        (unless (nth 4 state)
          (save-excursion
            (skip-syntax-forward "^-")
            (let ((state-test (syntax-ppss)))
              (when (and (nth 4 state-test) (<= point-init (nth 8 state-test)))
                (setq state state-test)))))

        (when (nth 4 state)
          (let ((start (nth 8 state)))
            (save-excursion
              (goto-char start)
              (forward-comment 1)
              (skip-syntax-backward ">")
              (cons start (point)))))))

    (defun my-hs-hide-initial-comment-block (orig-fun &rest args)
      (my-with-advice (('hs-inside-comment-p
                        :around
                        (lambda (fn)
                          (save-excursion
                            (save-restriction
                              (goto-char (point-min))
                              (skip-chars-forward " \t\n\f")
                              (let ((bounds (my-comment-bounds)))
                                (when bounds
                                  (narrow-to-region (car bounds) (cdr bounds))))
                              (funcall fn))))))
        (apply orig-fun args)))

    (defun my-hs-toggle-hiding (old-fn &rest args)
      (let ((bounds (my-comment-bounds)))
        (save-restriction
          (when bounds
            (narrow-to-region (car bounds) (cdr bounds)))
          (funcall old-fn args))))

    (setq-default hs-set-up-overlay #'my-hs-display-code-line-counts)
    (advice-add 'hs-toggle-hiding :around #'my-hs-toggle-hiding)
    (advice-add 'hs-make-overlay :around #'my-c-comment-hs-make-overlay-advice-fn)
    (advice-add 'hs-hide-initial-comment-block :around #'my-hs-hide-initial-comment-block)))


;; ----------------------------------------------------------------------------
;; Utility Macros
;; ##############

;; -----------------------
;; Without Yes/No Question
;; =======================

(defmacro without-yes-or-no (&rest body)
  "Override `yes-or-no-p' & `y-or-n-p',
not to prompt for input and return t."
  (declare (indent 0))
  `(cl-letf* (((symbol-function 'yes-or-no-p) (lambda (&rest _) t))
              ((symbol-function 'y-or-n-p) (lambda (&rest _) t)))
     ,@body))

;; ---------------
;; Without Message
;; ===============

(defmacro without-message (&rest body)
  "Run body, ignoring message."
  (declare (indent 1))
  `(cl-letf (((symbol-function 'message) 'ignore))
     ,@body))

;; ------------------------------------
;; KeyMap Macro (kmacro) Collapses Undo
;; ====================================

(defmacro kmacro-fn (keys)
  `(lambda (&optional arg)
     ""
     (interactive "p")
     (with-undo-amalgamate
       (kmacro-exec-ring-item (quote (,keys 0 "%d")) arg))))

(defmacro kmacro-call-x (keys)
  `(with-undo-amalgamate
     (kmacro-exec-ring-item (quote (,keys 0 "%d")) arg)))

;; -------------------------
;; Project Wide AutoComplete
;; =========================

(defun my-complete-find-in-project (&optional msg-prefix)
  (interactive)
  (unless msg-prefix
    (setq msg-prefix "Complete in Project"))
  (unless buffer-file-truename
    (user-error "%s: Buffer has no file" msg-prefix))
  (let (
        ;; Messages that don't log.
        (message-log-max nil)

        (filepath buffer-file-truename)
        (file-ext (file-name-extension buffer-file-truename))
        (this-word (thing-at-point 'word 'no-properties))
        (complete-best-guess-bin (concat user-emacs-directory "bin/complete_best_guess")))
    (unless this-word
      (user-error "%s: no word prefix" msg-prefix))
    (message "Completing...")
    (let ((base-path (vc-call-backend (vc-responsible-backend filepath) 'root filepath)))
      (let ((result
             (with-temp-buffer
               (call-process complete-best-guess-bin
                             nil (current-buffer) nil
                             ;; Command args.
                             base-path this-word (concat "." file-ext))
               (buffer-string))))
        (unless result
          (user-error "%s: no result found for '%s'" msg-prefix this-word))
        (insert result)
        (message nil)))))


;; ----------------------------------------------------------------------------
;; Explicit Mode Init
;; ##################
;;

;; ;; Needed so `completing-read' defaults to ivy.
;; (eval-after-load 'evil (ivy-mode))


(defun doxygen-preview ()
  "Show the doxygen for the current file."
  (interactive)
  ;; (call-process-shell-command
  (shell-command
   (concat
    user-emacs-directory
    "bin/doxygen_single_file"
    " --browse doc/doxygen/Doxyfile "
    (buffer-file-name))))

(when (and local-cfg/use-treemacs local-cfg/use-lsp)
  (define-key
   global-map (kbd "<f9>")
   (lambda ()
     (interactive)
     (cond
      ((bound-and-true-p lsp-treemacs-sync-mode)
       (lsp-treemacs-sync-mode -1)
       (treemacs-quit))
      (t
       (lsp-treemacs-symbols)
       (lsp-treemacs-sync-mode 1))))))

(when local-cfg/use-scroll-on-jump
  (scroll-on-jump-advice-add i42-imenu-next)
  (scroll-on-jump-advice-add i42-imenu-prev))

;; -------------------
;; Return Key Behavior
;; -------------------

;; enter insert newline
(defun vi-open-line-above ()
  "Insert a newline above the current line and put point at beginning."
  (interactive)
  (unless (bolp)
    (beginning-of-line))
  (newline)
  (forward-line -1))

(defun vi-open-line-below ()
  "Insert a newline below the current line and put point at beginning."
  (interactive)
  (unless (eolp)
    (end-of-line))
  (newline))

(defun generic-lookup-mouse (start-event)
  "Follow link/goto line with mouse."
  (interactive "e")
  (save-excursion
    (mouse-set-point start-event)
    (call-interactively 'my-generic-goto-thing-at-point)))

(when local-cfg/use-scroll-on-drag
  (use-package scroll-on-drag
    :commands (scroll-on-drag)
    :ensure nil
    :load-path "/src/emacs/scroll-on-drag"

    :config
    ;; Clamping is generally nice.
    (setq scroll-on-drag-clamp t)

    (when local-cfg/use-custom-mode-line
      (setq scroll-on-drag-mode-line-format my-mode-line-location-no-column))))

(add-hook
 'ediff-mode-hook
 (lambda ()
   ;; Vertical split: (for ediff)
   ;; https://stackoverflow.com/a/20514750/432509
   :init
   (setq ediff-window-setup-function 'ediff-setup-windows-plain)
   (setq ediff-split-window-function 'split-window-horizontally)

   :config

   (ediff-set-diff-options 'ediff-diff-options "-w")

   (defun my-ediff-hook ()
     ;; (ediff-setup-keymap)
     (define-key ediff-mode-map (kbd "<down>") 'ediff-next-difference)
     (define-key ediff-mode-map (kbd "<up>") 'ediff-previous-difference)
     (define-key
      ediff-mode-map (kbd "<left>")
      (lambda ()
        (interactive)
        (ediff-operate-on-windows (lambda (arg) (scroll-right arg)) 32)))
     (define-key
      ediff-mode-map (kbd "<right>")
      (lambda ()
        (interactive)
        (ediff-operate-on-windows (lambda (arg) (scroll-left arg)) 32)))
     (define-key
      ediff-mode-map (kbd "<prior>")
      (lambda ()
        (interactive)
        (setq last-command-event ?V)
        (ediff-scroll-vertically)))
     (define-key
      ediff-mode-map (kbd "<next>")
      (lambda ()
        (interactive)
        (setq last-command-event ?v)
        (ediff-scroll-vertically)))
     (define-key ediff-mode-map (kbd "<escape>") 'ediff-quit)
     (define-key ediff-mode-map "e" 'ediff-quit)
     (ediff-next-difference))

   (defun my-ediff-disable-quit-question (orig-fun &rest args)
     (cl-letf ((orig-y-or-n-p (symbol-function 'y-or-n-p))
               ((symbol-function 'y-or-n-p)
                (lambda (prompt)
                  (cond
                   ((string-prefix-p "Quit this Ediff session" prompt)
                    t)
                   (t
                    (funcall orig-y-or-n-p prompt))))))
       (apply orig-fun args)))
   (advice-add 'ediff-quit :around #'my-ediff-disable-quit-question)

   (add-hook 'ediff-startup-hook 'my-ediff-hook)))

(autoload 'i42-major-mode-diff "i42-major-mode-diff")
(add-hook 'diff-mode-hook #'i42-major-mode-diff)

;; Paste selection into the last edit-point
;; Useful so you can quick exit insert mode, find the text and select it
;; then place it back where you were in insert mode - in a single step.
;;
;; Use '`^' to get us back to the last insert point:

(defun paste-last-insert-from-region ()
  (interactive)
  (let ((text (buffer-substring-no-properties (region-beginning) (region-end))))
    (evil-exit-visual-state)
    (evil-goto-mark ?^)
    (evil-insert-state 1)
    (insert text)))

(defun paste-last-insert-from-symbol ()
  (interactive)
  (let ((text (thing-at-point 'symbol)))
    (evil-exit-visual-state)
    (evil-goto-mark ?^)
    (evil-insert-state 1)
    (insert text)))


(defun eval-region-as-py ()
  "Evaluate selection as a python expression, replacing it with the result."
  (interactive)
  (shell-command-on-region-and-select
   (region-beginning)
   (region-end)
   "python -c 'import sys; sys.stdout.write(str((eval(sys.stdin.read()))))'"
   0
   t))

(defun eval-region-as-lisp ()
  (interactive)
  (let ((value (eval (elisp--preceding-sexp))))
    (kill-sexp -1)
    (insert (format "%S" value))))

(defun eval-array-wrap-at-n ()
  "Wraps array at a specific length.
TODO take length as arg."
  (interactive)
  (shell-command-on-region-and-select
   (region-beginning) (region-end) (concat user-emacs-directory "bin/fmt_stdio_array_wrap_at_n")
   0 t))


(defun eval-c-define-to-enum ()
  "Converts '#define to enum block."
  (interactive)
  ;; (shell-command-on-region-and-select
  (shell-command-on-region (region-beginning)
                           (region-end)
                           (concat user-emacs-directory "bin/fmt_stdio_c_define_block_to_enum")
                           0
                           t))

(defun eval-region-as-py-numtoflag ()
  "Evaluate selection as a number,
eg: 0x400 or 128 and replace it with a bit-shifted literal, eg: (1 << 10)"
  (interactive)
  (shell-command-on-region-and-select
   (region-beginning) (region-end) (concat user-emacs-directory "bin/fmt_stdio_num_to_flag")
   0 t))

(defun eval-region-as-py-abbrev-float-32 ()
  "Simplifies a number by removing decimal places while keeping the number the same 32 but float."
  (interactive)
  (shell-command-on-region-and-select
   (region-beginning) (region-end) "~/.config/.emacs/bin/fmt_stdio_abbrev_float_32"
   0 t))

(defun insert-4-spaces ()
  "Insert tab width as spaces"
  (interactive)
  (insert (make-string tab-width ?\s)))


(defun kill-word-sans-delimiter-impl (arg)
  (let ((table (make-syntax-table)))
    (dolist (ch (list ?- ?_))
      (modify-syntax-entry ch "_" table))
    (with-syntax-table table
      (kill-word arg))))

(defun kill-word-backward-sans-delimiter (arg)
  (interactive "p")
  (kill-word-sans-delimiter-impl (- arg)))

(defun kill-word-forward-sans-delimiter (arg)
  (interactive "p")
  (kill-word-sans-delimiter-impl arg))

;; ---------------------------
;; Comment Block (insert mode)
;; ---------------------------
;;
;; Add multi-line comment, leaving the cursor in the middle.

;; TODO, other languages? - add as needed.
(defun my-insert-comment-block-c-style ()
  "Insert C comment block."
  (interactive)
  (insert "/*  */")
  (forward-char -3))

(defun my-insert-comment-block-c-style-doxygen ()
  "Insert C comment block."
  (interactive)
  (insert "/**  */")
  (forward-char -3))

(defun my-generic-comment-block ()
  "Enter multi-line comment."
  (interactive)
  (cond
   ((memq
     major-mode
     (list
      'c-mode 'c++-mode 'glsl-mode 'css-mode 'rust-mode
      ;; Tree-sitter modes.
      'c-ts-mode 'c++-ts-mode))
    (my-insert-comment-block-c-style))
   ((memq
     major-mode
     (list
      'python-mode
      ;; Tree-sitter modes.
      'python-ts-mode))
    (insert-comment-block-python))))

(defun my-generic-comment-block-doc ()
  "Enter multi-line comment."
  (interactive)
  (cond
   ((memq
     major-mode
     (list
      'c-mode 'c++-mode 'glsl-mode 'css-mode
      ;; Tree-sitter modes.
      'c-ts-mode 'c++-ts-mode))
    (my-insert-comment-block-c-style-doxygen))))

;; ---------------
;; Toggle Comments
;; ---------------
(defun my-comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region."
  (interactive)
  (let ((beg nil)
        (end nil))
    (cond
     ((region-active-p)
      (setq beg (region-beginning))
      (setq end (region-end)))
     (t
      (setq beg (line-beginning-position))
      (setq end (line-end-position))))

    (comment-or-uncomment-region beg end)))

(defun c-comment-from-cxx ()
  "Convert C++ to C Comment blocks."
  (interactive)
  (shell-command-on-region-and-select
   (region-beginning)
   (region-end)
   "/src/blender/tools/utils_ide/qtcreator/externaltools/qtc_cpp_to_c_comments.py"
   0
   t))


(defun tab-expand-when-non-leading ()
  "Expand tabs into spaces when not the first characters on the line."
  (interactive)
  (shell-command-on-region-and-select (region-beginning) (region-end)
                                      (concat
                                       user-emacs-directory "bin/tab_expand_when_non_leading.py")
                                      0 t))

;; -----------------
;; Keys for Packages
;; =================
;;
(when local-cfg/use-search-isearch
  (define-key isearch-mode-map (kbd "<down>") 'isearch-ring-advance)
  (define-key isearch-mode-map (kbd "<up>") 'isearch-ring-retreat))


;; --------
;; Ivy Keys
;; --------

(when local-cfg/use-ivy
  (with-eval-after-load 'ivy
    (define-key ivy-minibuffer-map (kbd "C-j") 'next-line)
    (define-key ivy-minibuffer-map (kbd "C-k") 'previous-line)

    (define-key ivy-minibuffer-map (kbd "C-h") 'minibuffer-keyboard-quit)
    (define-key ivy-minibuffer-map (kbd "C-l") 'ivy-done)

    (define-key ivy-minibuffer-map (kbd "C-M-h") 'minibuffer-keyboard-quit)
    (define-key ivy-minibuffer-map (kbd "C-M-l") 'ivy-done)

    ;; Open and next.
    (define-key ivy-minibuffer-map (kbd "C-M-j") 'ivy-next-line-and-call)
    (define-key ivy-minibuffer-map (kbd "C-M-k") 'ivy-previous-line-and-call)

    (define-key ivy-minibuffer-map (kbd "<C-return>") 'ivy-done)

    ;; Paste is too handy.
    (when local-cfg/use-bray
      (define-key ivy-minibuffer-map (kbd "C-e") 'bray-ex-copy-clipboard-only)
      (define-key ivy-minibuffer-map (kbd "C-r") 'bray-ex-yank-replace-from-clipboard-only))

    (when local-cfg/use-evil
      (define-key ivy-minibuffer-map (kbd "C-v") 'yank)

      ;; So we can switch away.
      (define-key ivy-minibuffer-map (kbd "C-w") 'evil-window-map))))

;; ------------
;; Package-Menu
;;
;; Doesn't get basic evil-mode keys set!
;;
(with-eval-after-load 'package
  (add-hook
   'package-menu-mode-hook
   (lambda ()
     (define-key package-menu-mode-map (kbd "j") 'next-line)
     (define-key package-menu-mode-map (kbd "k") 'previous-line)
     (define-key package-menu-mode-map (kbd "/") 'evil-ex-search-forward)
     (define-key package-menu-mode-map (kbd "?") 'evil-ex-search-backward)
     (define-key package-menu-mode-map (kbd "n") 'evil-ex-search-next)
     (define-key package-menu-mode-map (kbd "N") 'evil-ex-search-previous))))


;; ----------------------------------------------------------------------------
;; Global Menus
;; ############
;;
;; Context sensitive global menu.
;;

(defun my-popup ()
  "Pop up my menu. Or he can hit RET immediately to select the default item."
  (interactive)
  (my-custom-popup
   "Select:" 4
   `(("Project: Blender/C" . (lambda () (find-file "/src/blender/source/creator/creator.c")))
     ("Project: Blender Debug" .
      (lambda ()
        (gdb "/src/blender/blender.bin --env-system-scripts /src/blender/release/scripts")))
     ("Project: Blender/Python"
      .
      (lambda () (find-file "/src/blender/release/scripts/modules/bpy_types.py")))


     ("Delete Trailing (entire buffer)" . delete-trailing-whitespace)
     ("Expand Non Leading Tabs (selection)" . tab-expand-when-non-leading)
     ,@
     (when (memq
            major-mode
            (list
             'c-mode 'c++-mode
             ;; Tree-sitter modes.
             'c-ts-mode 'c++-ts-mode))
       '(("C/C++ ASM preview" .
          (lambda ()
            (shell-command
             (concat
              "/src/blender/tools/utils_ide/qtcreator/externaltools/"
              "qtc_assembler_preview.py "
              "/src/cmake_debug "
              (buffer-file-name)))))))

     ("Emacs INIT" . (lambda () (find-file user-init-file)))
     ("Emacs REPL" . ielm)
     ("Emacs TERM" . (lambda () (term (getenv "SHELL")))))))


(when local-cfg/use-scroll-on-jump
  (with-eval-after-load 'bookmark-in-project
    (scroll-on-jump-advice-add bookmark-in-project-jump-next)
    (scroll-on-jump-advice-add bookmark-in-project-jump-previous)))


;; ----------------------------------------------------------------------------
;; Key Bindings
;; ############

;; First unbind keys!


;; Allow C-m (normally RET).
(when (display-graphic-p)
  (define-key input-decode-map [?\C-m] [C-m]))

;; disable view-hello-file key shortcut
(define-key help-map (kbd "h") nil)

(defun my-setup-global-keys ()
  (dolist (key
           (list
            ;; electric indent mode, gets in way of evil-style keys for plugins
            "\C-j" "\C-k" "\C-m" "\C-u" "\C-w"
            "\C-e" ;; Was `move-end-of-line'
            "\C-a" ;; Was `move-beginning-of-line'
            "\C-r" ;; Was `isearch-backward'
            "\C-z" ;; Was `suspend-frame'.
            "\C-d" ;; Was `delete-char'.
            "\C-y" ;; Was `yank'.
            "\M-f" ;; Was `forward-word'.
            "\M-u" ;; Free.
            "\C-n" ;; Was `next-line'.
            "\C-p" ;; Was `previous-line'.
            "\C-s" ;; Was `isearch-forward'.
            "\M-t" ;; Was `transpose-words'.
            "\M-e" ;; Was `forward-sentence'.
            "\C-t" ;; Was `transpose-chars'.
            "\C-\M-t" ;; Was `transpose-sexps'.
            "\M-z" ;; Was zap-to-char (native emacs).
            "\M-\ " ;; Was just-one-space (native emacs).
            "\C-_" ;; Was undo.
            "\C-v" ;; Was `scroll-up-command'.
            "\C-M-j" ;; Was `mark-defun'.
            "\C-M-h" ;; Was `mark-defun'.
            "\C-M-k" ;; Was `kill-sexp'.
            "\C-M-l" ;; Was `reposition-window'.
            "\M-\^?" ;; Was `backward-kill-word' (M-<delete>)
            "\M-j" ;; Was `default-indent-new-line'.
            "\M-k" ;; Was `kill-sentence'.
            "\M-w" ;; Was `kill-ring-save'.
            "\t" ;; Was `indent-for-tab-command'.

            (kbd "C-<backspace>") ;; Was `backward-kill-word'.
            (kbd "C-<delete>") ;; Was `backward-kill-word'.
            (kbd "C-<right>") ;; Was `right-word'.
            (kbd "C-<left>") ;; Was `left-word'.

            (kbd "C-/") ;; Was undo.

            (kbd "<home>") (kbd "<end>") (kbd "S-<home>") (kbd "S-<end>")

            (kbd "C-<backspace>") ;; Was backward-kill-word.
            (kbd "C-<delete>") ;; Was backward-kill-word.
            (kbd "C-/") ;; Was undo.
            (kbd "C-<right>") ;; Was `right-word'.
            (kbd "C-<left>") ;; Was `left-word'.

            ;; F-Keys.
            [f1] [f2] [f3] [f4] [f5] [f6] [f7] [f8] [f9] [f10] [f11] [f12]

            ;; Use find for our own purpose.
            [find]))
    (global-unset-key key))


  ;; ----------------
  ;; Application Keys
  ;; ================

  (global-set-key (kbd "C-M-w") 'make-frame-command)
  (global-set-key
   (kbd "<C-escape>")
   (lambda ()
     (interactive)
     (kill-this-buffer)))

  (when local-cfg/use-bray
    (global-set-key (kbd "C-<right>") 'bray-ex-next-symbol)
    (global-set-key (kbd "C-<left>") 'bray-ex-back-symbol)

    (global-set-key (kbd "<home>") 'bray-ex-move-beginning-of-line)
    (global-set-key (kbd "<end>") 'bray-ex-move-end-of-line))

  ;; scale entire UI, solves annoying mismatches
  (global-set-key (kbd "C-=") 'default-font-presets-scale-increase)
  (global-set-key (kbd "C--") 'default-font-presets-scale-decrease)
  (global-set-key (kbd "C-0") 'default-font-presets-scale-reset)
  (global-set-key (kbd "C-*") 'default-font-presets-scale-fit)

  ;; For X11.
  (global-set-key (kbd "<C-mouse-4>") 'default-font-presets-scale-increase)
  (global-set-key (kbd "<C-mouse-5>") 'default-font-presets-scale-decrease)
  ;; For PGTK.
  (global-set-key (kbd "<C-wheel-up>") 'default-font-presets-scale-increase)
  (global-set-key (kbd "<C-wheel-down>") 'default-font-presets-scale-decrease)

  ;; somehow these get mixed up, support both down and click, ignore drag (annoying error)
  (global-set-key (kbd "<C-mouse-1>") 'generic-lookup-mouse)
  (global-set-key (kbd "<C-down-mouse-1>") 'generic-lookup-mouse)
  (global-set-key (kbd "<C-drag-mouse-1>") (lambda () (interactive)))

  (global-set-key (kbd "C-b") 'my-generic-build)

  (global-set-key (kbd "C-s") 'i42-save-buffer-always)

  (global-set-key (kbd "<f1>") 'doxygen-preview)

  (global-set-key (kbd "<f6>") 'next-error)
  (global-set-key (kbd "<S-f6>") 'previous-error)


  (global-set-key (kbd "<C-iso-lefttab>") 'bs-cycle-previous)
  (global-set-key (kbd "<C-tab>") 'bs-cycle-next)

  (when (display-graphic-p)
    (global-set-key (kbd "<M-prior>") 'default-font-presets-forward)
    (global-set-key (kbd "<M-next>") 'default-font-presets-backward))

  (when local-cfg/use-ivy
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-k") 'counsel-at-point-file-jump)
    (global-set-key (kbd "M-S-k") 'ivy-switch-buffer))

  (when local-cfg/use-vertico
    (global-set-key (kbd "M-x") 'execute-extended-command)
    (global-set-key (kbd "C-k") 'consult-find) ;; Should use FFIP?
    (global-set-key (kbd "M-S-k") 'consult-project-buffer))

  (when local-cfg/use-neotree
    ;; (global-set-key (kbd "M-e") 'neotree-toggle)
    (global-set-key (kbd "M-e") 'i42-neotree-project-dir-toggle))


  (global-set-key (kbd "C-M-j") 'bookmark-in-project-jump-next)
  (global-set-key (kbd "C-M-k") 'bookmark-in-project-jump-previous)
  (global-set-key (kbd "C-M-h") 'bookmark-in-project-toggle)
  (global-set-key (kbd "C-M-l") 'bookmark-in-project-jump)

  ;; Handy in-document next/prev (based on doxy groups for eg)
  (global-set-key
   (kbd "C-}")
   (lambda ()
     (interactive)
     (scroll-on-jump (call-interactively 'my-generic-doc-jump-next))))
  (global-set-key
   (kbd "C-{")
   (lambda ()
     (interactive)
     (scroll-on-jump (call-interactively 'my-generic-doc-jump-prev))))
  ;; alternate access (page up/down)
  (global-set-key
   (kbd "<C-next>")
   (lambda ()
     (interactive)
     (scroll-on-jump (call-interactively 'my-generic-doc-jump-next))))
  (global-set-key
   (kbd "<C-prior>")
   (lambda ()
     (interactive)
     (scroll-on-jump (call-interactively 'my-generic-doc-jump-prev))))

  (when local-cfg/use-git-gutter
    ;; Diff next/prev
    (global-set-key (kbd "<C-M-next>") (scroll-on-jump-interactive 'git-gutter:next-hunk))
    (global-set-key (kbd "<C-M-prior>") (scroll-on-jump-interactive 'git-gutter:previous-hunk)))


  (when local-cfg/use-diff-hl
    (global-set-key (kbd "<C-M-next>") (scroll-on-jump-interactive 'diff-hl-next-hunk))
    (global-set-key (kbd "<C-M-prior>") (scroll-on-jump-interactive 'diff-hl-previous-hunk)))

  ;; handy, Same as S-{}

  ;; (global-set-key (kbd "<S-next>") 'evil-forward-paragraph)
  ;; (global-set-key (kbd "<S-prior>") 'evil-backward-paragraph)

  ;; Re-center is nicer
  (global-set-key (kbd "<S-next>") 'evil-forward-paragraph)
  (global-set-key (kbd "<S-prior>") 'evil-backward-paragraph)

  (global-set-key (kbd "<next>") 'i42-scroll-and-clamp-up-command)
  (global-set-key (kbd "<prior>") 'i42-scroll-and-clamp-down-command)


  (global-set-key (kbd "<f12>") 'my-popup)

  (global-set-key (kbd "<f5>") 'my-generic-run)
  (global-set-key (kbd "<f4>") 'imenu-list-minor-mode))

(when local-cfg/use-evil

  (defmacro evil-define-key-with-modelist (state-arg mode-list-arg key-arg action-arg)
    `
    ;; `evil-define-key*' must be loaded first.
    (with-eval-after-load 'evil-core
      (let ((state ,state-arg)
            (mode-list ,mode-list-arg)
            (key ,key-arg)
            (action ,action-arg))
        (dolist (mode-item mode-list)
          (evil-define-key* state mode-item key action)))))

  (defmacro evil-define-key-for-cc-modes (state-arg key-arg action-arg)
    `(progn
       (with-eval-after-load 'cc-mode
         (evil-define-key-with-modelist
          ,state-arg (list c-mode-map c++-mode-map) ,key-arg ,action-arg))
       ;; Tree-sitter modes.
       (with-eval-after-load 'c-ts-mode
         (evil-define-key-with-modelist
          ,state-arg (list c-ts-mode-map c++-ts-mode-map) ,key-arg ,action-arg))
       (with-eval-after-load 'glsl-mode
         (evil-define-key-with-modelist ,state-arg (list glsl-mode-map) ,key-arg ,action-arg))))

  ;; jump words when C is held.
  (defun evil-forward-word-end-plusone ()
    "Forward word end, then right one."
    (interactive)
    (evil-forward-word-end)
    (forward-char))

  (defun my-evil-keys ()

    ;; simulate return in insert mode
    (evil-define-key 'normal 'global (kbd "<S-return>") 'my-generic-line-break-split)
    (evil-define-key 'normal 'global (kbd "<C-return>") 'my-generic-line-break-above)

    ;; (evil-define-key 'normal 'global (kbd "RET") 'vi-open-line-below)
    (evil-define-key 'normal 'global (kbd "RET") 'my-generic-line-break-below)


    (evil-define-key 'normal 'global (kbd "M-d") 'diff-at-point-open-and-goto-hunk)

    ;; go to next
    (evil-define-key
     'normal 'global (kbd "M-<right>")
     (lambda ()
       (interactive)
       (scroll-on-jump (call-interactively 'my-generic-jump-next))))
    ;; go back
    (evil-define-key
     'normal 'global (kbd "M-<left>")
     (lambda ()
       (interactive)
       (scroll-on-jump (call-interactively 'my-generic-jump-prev))))
    ;; lookup declaration
    (evil-define-key
     'normal 'global (kbd "M-RET")
     (lambda ()
       (interactive)
       (scroll-on-jump (call-interactively 'my-generic-goto-thing-at-point))))

    ;; lookup usage
    (evil-define-key 'normal 'global (kbd "C-S-u") 'my-generic-usage-of-thing-at-point)

    (evil-define-key 'normal 'global (kbd "C-S-h") 'goto-last-change)
    (evil-define-key 'normal 'global (kbd "C-S-l") 'goto-last-change-reverse)

    (when local-cfg/use-scroll-on-drag
      (evil-define-key
       'normal 'global [down-mouse-2]
       (lambda ()
         (interactive)
         (unless (scroll-on-drag)
           (or (mouse-yank-secondary t) (mouse-yank-primary t))))))

    (evil-define-key
     'insert 'global (kbd "<mouse-3>") 'i42-mouse-yank-secondary-select-or-word-at-cursor)

    (evil-define-key 'normal 'global (kbd "C-a") 'evil-numbers/inc-at-pt)
    (evil-define-key 'normal 'global (kbd "C-x") 'evil-numbers/dec-at-pt)

    (evil-define-key 'normal 'global (kbd "C-n") 'i42-toggle-display-line-numbers)
    (evil-define-key 'normal 'global (kbd "C-M-n") 'i42-toggle-display-line-numbers-relative)

    (evil-define-key 'normal 'global (kbd "M-f") 'avy-goto-word-1)

    (when local-cfg/use-ivy
      ;; C-f is a vim shortcut for scrolling, but I rarely use it!
      ;; Fill in search based on the cursor.
      (evil-define-key 'normal 'global (kbd "C-f") 'counsel-at-point-rg)
      (evil-define-key 'visual 'global (kbd "C-f") 'counsel-at-point-rg)

      (evil-define-key 'normal 'global (kbd "C-M-f") 'swiper))

    (when local-cfg/use-vertico
      ;; Fill in search based on the cursor.
      (evil-define-key 'normal 'global (kbd "C-f") 'consult-ripgrep)
      (evil-define-key 'visual 'global (kbd "C-f") 'consult-ripgrep))

    (evil-define-key 'normal 'global (kbd "M-<down>") 'drag-stuff-down)
    (evil-define-key 'normal 'global (kbd "M-<up>") 'drag-stuff-up)

    ;; Next/previous imenu items C-S-j/k.
    (evil-define-key 'normal 'global (kbd "C-S-j") 'i42-imenu-cycle-next)
    (evil-define-key 'normal 'global (kbd "C-S-k") 'i42-imenu-cycle-prev)

    (evil-define-key 'normal 'global (kbd "<cancel>") 'i42-imenu-cycle-next)
    (evil-define-key 'normal 'global (kbd "<Open>") 'i42-imenu-cycle-prev)


    ;; --------
    ;; Surround
    ;; --------
    ;;
    ;; Uses macros, see: http://emacs.stackexchange.com/questions/70 for how to define.


    (fset 'delete-surround-backtick (kmacro-fn [?d ?s ?`]))
    (fset 'delete-surround-single_quote (kmacro-fn [?d ?s ?']))
    (fset 'delete-surround-double_quote (kmacro-fn [?d ?s ?\"]))
    (fset 'delete-surround-paren (kmacro-fn [?d ?s ?\(]))
    (fset 'delete-surround-bracket (kmacro-fn [?d ?s ?\[]))
    (fset 'delete-surround-brace (kmacro-fn [?d ?s ?{]))

    (evil-define-key 'normal 'global (kbd "M-`") 'delete-surround-backtick)
    (evil-define-key 'normal 'global (kbd "M-'") 'delete-surround-single_quote)
    (evil-define-key 'normal 'global (kbd "M-\"") 'delete-surround-double_quote)

    (evil-define-key 'normal 'global (kbd "M-(") 'delete-surround-paren)
    (evil-define-key 'normal 'global (kbd "M-)") 'delete-surround-paren)
    (evil-define-key 'normal 'global (kbd "M-{") 'delete-surround-brace)
    (evil-define-key 'normal 'global (kbd "M-}") 'delete-surround-brace)
    (evil-define-key 'normal 'global (kbd "M-s") 'i42-delete-surround-at-point)
    (when (display-graphic-p)
      (evil-define-key 'normal 'global (kbd "M-[") 'delete-surround-bracket)
      (evil-define-key 'normal 'global (kbd "M-]") 'delete-surround-bracket))

    (fset 'insert-surround-backtick (kmacro-fn [?S ?`]))
    (fset 'insert-surround-single_quote (kmacro-fn [?S ?']))
    (fset 'insert-surround-double_quote (kmacro-fn [?S ?\"]))
    (fset 'insert-surround-paren (kmacro-fn [?S ?\)]))
    (fset 'insert-surround-bracket (kmacro-fn [?S ?\]]))
    (fset 'insert-surround-brace (kmacro-fn [?S ?}]))

    (evil-define-key 'visual 'global (kbd "M-`") 'insert-surround-backtick)
    (evil-define-key 'visual 'global (kbd "M-'") 'insert-surround-single_quote)
    (evil-define-key 'visual 'global (kbd "M-\"") 'insert-surround-double_quote)
    (evil-define-key 'visual 'global (kbd "M-(") 'insert-surround-paren)
    (evil-define-key 'visual 'global (kbd "M-)") 'insert-surround-paren)
    (evil-define-key 'visual 'global (kbd "M-[") 'insert-surround-bracket)
    (evil-define-key 'visual 'global (kbd "M-]") 'insert-surround-bracket)
    (evil-define-key 'visual 'global (kbd "M-{") 'insert-surround-brace)
    (evil-define-key 'visual 'global (kbd "M-}") 'insert-surround-brace)

    (evil-define-key 'visual 'global (kbd "+") 'er/expand-region)
    (evil-define-key 'visual 'global (kbd "_") 'er/contract-region)
    (evil-define-key
     'normal 'global (kbd "+")
     (lambda ()
       (interactive)
       (call-interactively 'evil-visual-char)
       (call-interactively 'er/expand-region)))

    ;; end surround

    ;; -----------------------------------
    ;; Pointer Paste / Secondary Selection
    ;; -----------------------------------

    ;; use secondary selection in insert mode
    (evil-define-key 'insert 'global (kbd "<down-mouse-1>") 'mouse-drag-secondary)
    (evil-define-key 'insert 'global (kbd "<drag-mouse-1>") 'mouse-drag-secondary)
    (evil-define-key 'insert 'global (kbd "<mouse-1>") 'mouse-start-secondary)
    ;;(evil-define-key 'insert 'global (kbd "<mouse-2>") 'mouse-yank-secondary)
    (evil-define-key 'insert 'global (kbd "<mouse-2>") 'i42-mouse-yank-secondary-and-deselect)
    (evil-define-key 'insert 'global (kbd "<M-down-mouse-1>") 'evil-mouse-drag-region)
    (evil-define-key 'insert 'global (kbd "<M-drag-mouse-1>") 'evil-mouse-drag-region)
    (evil-define-key 'insert 'global (kbd "<M-mouse-1>") 'mouse-set-point)
    (evil-define-key 'insert 'global (kbd "<M-mouse-2>") 'mouse-yank-primary)


    (when local-cfg/use-code-folding
      (evil-define-key 'normal 'global (kbd "TAB") 'hs-toggle-hiding)
      (evil-define-key 'normal 'global (kbd "<backtab>") 'my-hs-toggle-hiding-all))


    ;; Handy way to insert a comment block in insert mode.
    (evil-define-key 'insert 'global (kbd "C-M-/") 'my-generic-comment-block)
    (evil-define-key 'insert 'global (kbd "C-M-?") 'my-generic-comment-block-docstring)

    (evil-define-key 'normal 'global (kbd "C-SPC") 'my-comment-or-uncomment-region-or-line)
    (unless (display-graphic-p)
      (evil-define-key 'normal 'global (kbd "C-@") 'my-comment-or-uncomment-region-or-line))

    (evil-define-key 'normal diff-mode-map (kbd "C-SPC") 'diff-ansi-buffer)
    (unless (display-graphic-p)
      (evil-define-key 'normal diff-mode-map (kbd "C-@") 'diff-ansi-buffer))


    (evil-define-key-for-cc-modes 'visual (kbd "C-/") 'c-comment-from-cxx)

    (evil-define-key 'visual 'global (kbd "C-?") 'i42-comment-multi-line-toggle)


    ;; -----------------
    ;; Motion State Keys
    ;; -----------------
    ;;
    ;; Similar to normal mode?

    ;; jump between sections of a file
    (evil-define-key
     'motion 'global (kbd "C-j")
     (cond
      (local-cfg/use-ivy
       ;; 'counsel-imenu
       'counsel-at-point-imenu)
      (t
       'imenu)))

    (evil-define-key 'motion 'global (kbd "C-l") 'my-generic-doc-jump-section)

    ;; Paste second last.
    (when local-cfg/use-gui-clipboard-history
      (evil-define-key 'normal 'global (kbd "M-p") 'gui-clipboard-history-paste-penultimate)
      (evil-define-key 'insert 'global (kbd "M-p") 'gui-clipboard-history-paste-penultimate))

    ;; ----------------
    ;; Visual Mode Keys
    ;; ================
    ;;

    (evil-define-key 'visual 'global (kbd "C-i") 'narrow-to-region)
    (evil-define-key 'normal 'global (kbd "M-i") 'widen)

    ;; See `i42-upcase-initials-region', it's a fancier version of this function.
    ;; (evil-define-key 'visual 'global (kbd "C-M-u") 'upcase-initials-region)

    (evil-define-key 'visual 'global (kbd "M-h") 'evil-shift-left)
    (evil-define-key 'visual 'global (kbd "M-l") 'evil-shift-right)

    (evil-define-key 'visual 'global (kbd "M-r") 'paste-last-insert-from-region)
    (evil-define-key 'normal 'global (kbd "M-r") 'paste-last-insert-from-symbol)

    ;; (evil-define-key 'visual 'global (kbd "RET") 'wrap-array-at-n)
    ;; (evil-define-key 'visual 'global (kbd "RET") 'eval-c-define-to-enum)
    ;; (evil-define-key 'visual 'global (kbd "RET") 'eval-region-as-py-numtoflag)
    ;; (evil-define-key 'visual 'global (kbd "RET") 'eval-region-as-py-abbrev-float-32)
    ;; (evil-define-key 'visual 'global (kbd "RET") 'eval-region-as-lisp)
    (evil-define-key 'visual 'global (kbd "RET") 'my-generic-eval-selection)

    (evil-define-key-for-cc-modes 'visual (kbd "C-/") 'c-comment-from-cxx)

    ;; -----------------
    ;; Insert Mode  Keys
    ;; =================

    (evil-define-key 'insert 'mono-complete-mode (kbd "<end>") 'mono-complete-expand-or-fallback)

    (evil-define-key 'insert 'global (kbd "C-<backspace>") 'backward-kill-word)
    (evil-define-key 'insert 'global (kbd "C-<delete>") 'kill-word)

    ;; TODO: move to sidecar-locals.
    (evil-define-key 'insert 'global (kbd "M-t") 'utimeclock-toggle)
    (evil-define-key 'normal 'global (kbd "M-t") 'utimeclock-toggle)

    ;; default
    (evil-define-key 'insert 'global (kbd "RET") 'comment-indent-new-line)
    (evil-define-key 'insert 'global (kbd "C-c") 'evil-normal-state)
    ;; for other modes
    ;; (evil-define-key 'insert c-mode-map (kbd "RET") 'c-indent-new-comment-line)
    ;; Smart tabs
    ;; (evil-define-key 'insert c-mode-map (kbd "RET") 'newline-and-indent)


    (evil-define-key-for-cc-modes 'insert (kbd "RET") 'c-context-line-break)


    (evil-define-key 'insert 'global (kbd "<backtab>") 'insert-4-spaces)

    (evil-define-key 'insert 'global (kbd "C-v") 'clipboard-yank)

    ;; (evil-define-key 'insert 'global (kbd "M-h") 'backward-char)
    ;; (evil-define-key 'insert 'global (kbd "M-j") 'evil-next-line)
    ;; (evil-define-key 'insert 'global (kbd "M-k") 'evil-previous-line)
    ;; (evil-define-key 'insert 'global (kbd "M-l") 'forward-char)

    ;; avoid using home/end in insert mode
    (evil-define-key 'insert 'global (kbd "M-^") 'move-beginning-of-line)
    (evil-define-key 'insert 'global (kbd "M-$") 'move-end-of-line)

    ;; Handy insertions.
    (evil-define-key
     'insert 'global (kbd "C-.")
     (lambda ()
       (interactive)
       (insert "->")))

    ;; Simple auto-complete

    ;; company
    (when local-cfg/use-company
      ;; Dictionary based auto-complete.
      (evil-define-key 'insert 'global (kbd "C-n") 'company-dabbrev)
      ;; I almost never use this, expose since some modes extend this hook and it may come in handy.
      (evil-define-key 'insert 'global (kbd "C-M-n") 'completion-at-point)

      ;; Context sensitive auto-complete.
      (evil-define-key 'insert 'global (kbd "C-SPC") 'my-company-complete-common-use-context))

    ;; Own auto-complete.
    (evil-define-key 'insert 'global (kbd "S-SPC") 'my-complete-find-in-project)

    (evil-define-key 'insert 'global (kbd "C-M-h") 'evil-backward-word-begin)
    ;; (evil-define-key 'insert 'global (kbd "C-M-l") 'evil-forward-word-end)
    (evil-define-key 'insert 'global (kbd "C-M-l") 'evil-forward-word-end-plusone)

    (evil-define-key 'insert 'global (kbd "<C-backspace>") 'evil-delete-backward-word)

    (evil-define-key 'insert 'global (kbd "<backspace>") 'i42-backspace-whitespace-to-tab-stop)


    (evil-define-key 'insert 'global (kbd "<S-backspace>") 'kill-word-backward-sans-delimiter)
    (evil-define-key 'insert 'global (kbd "<S-delete>") 'kill-word-forward-sans-delimiter)

    ;; use dumb indentation (for now)
    (evil-define-key 'insert 'global (kbd "TAB") 'tab-to-tab-stop)


    ;; Re-bind Ctrl-Z, so we can use for undo and other things.
    ;; Keep as Ctrl-Alt-Z, so it's possible when needed.
    (evil-define-key 'motion 'global (kbd "C-z") nil)
    (evil-define-key 'insert 'global (kbd "C-z") nil)
    (evil-define-key 'emacs 'global (kbd "C-z") nil)

    (evil-define-key 'motion 'global (kbd "C-S-z") 'evil-emacs-state)
    (evil-define-key 'insert 'global (kbd "C-S-z") 'evil-emacs-state)
    (evil-define-key 'emacs 'global (kbd "C-S-z") 'evil-emacs-state)


    ;; Surround keys (don't use the mode.)
    (when local-cfg/evil-surround
      (evil-define-key 'operator 'global "s" 'evil-surround-edit)
      (evil-define-key 'operator 'global "S" 'evil-Surround-edit)

      (evil-define-key 'visual 'global "S" 'evil-surround-region)
      (evil-define-key 'visual 'global "bs" 'evil-Surround-region))

    (when local-cfg/use-undo-emacs
      (cond
       (local-cfg/evil-keyboard-remap
        (evil-define-key 'normal 'global (kbd "z") (scroll-on-jump-interactive 'undo-only))
        (evil-define-key 'normal 'global (kbd "Z") (scroll-on-jump-interactive 'undo-redo)))
       (t
        (evil-define-key 'normal 'global (kbd "u") (scroll-on-jump-interactive 'undo-only))
        (evil-define-key 'normal 'global (kbd "C-r") (scroll-on-jump-interactive 'undo-redo))
        (evil-define-key 'insert 'global (kbd "C-u") (scroll-on-jump-interactive 'undo-only))
        (evil-define-key 'insert 'global (kbd "M-S-u") (scroll-on-jump-interactive 'undo-redo))))

      (evil-define-key 'insert 'global (kbd "C-z") (scroll-on-jump-interactive 'undo-only))
      (evil-define-key 'insert 'global (kbd "C-S-z") (scroll-on-jump-interactive 'undo-redo)))

    (when local-cfg/use-undo-fu
      (cond
       (local-cfg/evil-keyboard-remap
        (evil-define-key 'normal 'global (kbd "z") (scroll-on-jump-interactive 'undo-fu-only-undo))
        (evil-define-key
         'normal 'global (kbd "Z") (scroll-on-jump-interactive 'undo-fu-only-redo)))
       (t
        (evil-define-key 'normal 'global (kbd "u") (scroll-on-jump-interactive 'undo-fu-only-undo))
        (evil-define-key
         'normal 'global (kbd "C-r") (scroll-on-jump-interactive 'undo-fu-only-redo))
        (evil-define-key
         'insert 'global (kbd "C-u") (scroll-on-jump-interactive 'undo-fu-only-undo))
        (evil-define-key
         'insert 'global (kbd "M-S-u") (scroll-on-jump-interactive 'undo-fu-only-redo))))

      (evil-define-key 'insert 'global (kbd "M-z") (scroll-on-jump-interactive 'undo-fu-only-undo))
      (evil-define-key
       'insert 'global (kbd "M-S-z") (scroll-on-jump-interactive 'undo-fu-only-redo)))


    (cond
     (local-cfg/evil-keyboard-remap
      (evil-define-key 'normal 'global (kbd "C-M-z") 'vundo))
     (t
      (evil-define-key 'normal 'global (kbd "C-M-u") 'vundo)))

    (evil-define-key 'normal 'global (kbd "s-e") 'i42-accum-clipboard-cut)
    (evil-define-key 'normal 'global (kbd "s-r") 'i42-accum-clipboard-paste)

    (evil-define-key 'normal neotree-mode-map (kbd "SPC") 'neotree-enter)
    (evil-define-key 'normal neotree-mode-map (kbd "RET") 'neotree-enter)
    (evil-define-key 'normal neotree-mode-map (kbd "M-l") 'neotree-enter)
    (evil-define-key 'normal neotree-mode-map (kbd "M-h") 'neotree-select-up-node)
    (evil-define-key 'normal neotree-mode-map (kbd "M-j") 'neotree-next-line)
    (evil-define-key 'normal neotree-mode-map (kbd "M-k") 'neotree-previous-line)
    (evil-define-key 'normal neotree-mode-map (kbd "q") 'neotree-hide)

    ;; Spatial Navigation

    ;; ----------------
    ;; Normal Mode Keys
    ;; ================

    (evil-define-key 'normal 'global (kbd "C-<backspace>") 'backward-kill-word)
    (evil-define-key 'normal 'global (kbd "C-<delete>") 'kill-word)

    (evil-define-key 'normal 'global (kbd "C-d") 'my-generic-fix-action)
    (evil-define-key 'normal 'global (kbd "C-e") 'flymake-goto-next-error)


    ;; (evil-define-key 'normal 'global (kbd "M-z") 'recomplete-ispell-word)
    (evil-define-key 'normal 'global (kbd "M-z") 'i42-recomplete-contextual)
    (evil-define-key 'normal 'global (kbd "M-a") 'cycle-at-point)
    (evil-define-key 'normal 'global (kbd "M-c") 'i42-recomplete-contextual-case-cycle)

    (evil-define-key 'normal 'global (kbd "M-;") 'my-eldoc-box-toggle-show-more)

    (evil-define-key
     'normal 'global (kbd "M-Z")
     (lambda ()
       (interactive)
       (let ((current-prefix-arg '(-1)))
         (call-interactively 'recomplete-ispell-word))))

    ;; (evil-define-key 'normal 'global (kbd "M-z") 'recomplete-case-style-forward)
    ;; (evil-define-key 'normal 'global (kbd "M-Z") 'recomplete-case-style-backward)

    ;; (evil-define-key 'normal 'global (kbd "M-z") 'recomplete-dabbrev-forward)
    ;; (evil-define-key 'normal 'global (kbd "M-Z") 'recomplete-dabbrev-backward)
    ;; (evil-define-key 'insert 'global (kbd "M-z") 'recomplete-dabbrev-forward)
    ;; (evil-define-key 'insert 'global (kbd "M-Z") 'recomplete-dabbrev-backward)


    (evil-define-key 'normal 'global (kbd "C-'") 'counsel-evil-marks)

    (evil-define-key 'normal 'global (kbd "C-M-r") 'my-generic-rename-thing-at-point)


    (evil-define-key 'normal 'global (kbd "C-M-t") 'titlecase-dwim)


    (evil-define-key
     'insert
     'global
     (kbd "M-k")
     (scroll-on-jump-interactive 'spatial-navigate-backward-vertical-bar))
    (evil-define-key
     'insert
     'global
     (kbd "M-j")
     (scroll-on-jump-interactive 'spatial-navigate-forward-vertical-bar))
    (evil-define-key
     'insert
     'global
     (kbd "M-h")
     (scroll-on-jump-interactive 'spatial-navigate-backward-horizontal-bar))
    (evil-define-key
     'insert
     'global
     (kbd "M-l")
     (scroll-on-jump-interactive 'spatial-navigate-forward-horizontal-bar))

    (evil-define-key
     'normal
     'global
     (kbd "M-k")
     (scroll-on-jump-interactive 'spatial-navigate-backward-vertical-box))
    (evil-define-key
     'normal
     'global
     (kbd "M-j")
     (scroll-on-jump-interactive 'spatial-navigate-forward-vertical-box))
    (evil-define-key
     'normal
     'global
     (kbd "M-h")
     (scroll-on-jump-interactive 'spatial-navigate-backward-horizontal-box))
    (evil-define-key
     'normal
     'global
     (kbd "M-l")
     (scroll-on-jump-interactive 'spatial-navigate-forward-horizontal-box))

    (evil-define-key
     'visual
     'global
     (kbd "M-k")
     (scroll-on-jump-interactive 'spatial-navigate-backward-vertical-box))
    (evil-define-key
     'visual
     'global
     (kbd "M-j")
     (scroll-on-jump-interactive 'spatial-navigate-forward-vertical-box))
    (evil-define-key
     'visual
     'global
     (kbd "M-h")
     (scroll-on-jump-interactive 'spatial-navigate-backward-horizontal-box))
    (evil-define-key
     'visual
     'global
     (kbd "M-l")
     (scroll-on-jump-interactive 'spatial-navigate-forward-horizontal-box)))


  (defun my-evil-leader-keys ()
    (evil-set-leader (list 'normal 'visual 'replace 'operator 'motion) (kbd "<SPC>"))

    ;; Close other windows.
    (evil-define-key 'normal 'global (kbd "<leader><escape>") 'i42-close-all-other-windows)

    ;; Open file or run at point.
    (evil-define-key 'normal 'global (kbd "<leader><RET>") 'run-stuff-command-on-region-or-line)

    ;; Revert all buffers.
    (cond
     (local-cfg/evil-keyboard-remap
      (evil-define-key 'normal 'global (kbd "<leader>TAB") 'revert-buffer-all))
     (t
      (evil-define-key 'normal 'global (kbd "<leader>r") 'revert-buffer-all)))

    ;; Navigate marks.
    (evil-define-key 'normal 'global (kbd "<leader>'") 'counsel-evil-marks)
    ;; Font syntax highlighting.
    ;; (evil-define-key 'normal 'global (kbd "<leader>f") 'font-lock-mode)

    ;; Number to flag.
    (evil-define-key 'normal 'global (kbd "<leader>f") 'eval-region-as-py-numtoflag)

    (evil-define-key 'normal 'global (kbd "<leader>j") 'diff-hl-next-hunk)
    (evil-define-key 'normal 'global (kbd "<leader>k") 'diff-hl-previous-hunk)

    ;; Undo diff hunk (handy)
    (evil-define-key
     'normal 'global
     (cond
      (local-cfg/evil-keyboard-remap
       (kbd "<leader>z"))
      (t
       (kbd "<leader>u")))
     'i42-diff-revert-hunks-at-point)

    ;; Undo ALL diff hunks (handy)
    (evil-define-key
     'normal 'global
     (cond
      (local-cfg/evil-keyboard-remap
       (kbd "<leader>Z"))
      (t
       (kbd "<leader>U")))
     'i42-diff-revert-hunks-all)

    ;; Shrink white-space.
    (evil-define-key 'normal 'global (kbd "<leader>x") 'shrink-whitespace)

    ;; Transpose arg after.
    (evil-define-key 'normal 'global (kbd "<leader>l") 'i42-transpose-structured-forward)
    ;; Transpose arg before.
    (evil-define-key 'normal 'global (kbd "<leader>h") 'i42-transpose-structured-backward)


    ;; Transpose arg before.
    (evil-define-key
     'normal 'global (kbd "<leader>o")
     '(lambda ()
        (interactive)
        (cond
         ((string-equal evil-state "visual")
          (call-interactively 'sort-lines))
         ((string-equal evil-state "normal")
          (call-interactively 'i42-sort-line-in-block))
         (t
          (error "Can't sort for mode %S" evil-state)))))

    (evil-define-key 'normal 'global (kbd "<leader>w") 'show-whitespace-toggle)

    ;; Diff against `main' branch, or the last commit if `main' is the current branch.
    (evil-define-key 'normal 'global (kbd "<leader>d") 'i42-diff-at-point-with-other-rev)

    ;; Spell checker, next error
    (evil-define-key 'normal 'global (kbd "<leader>e") 'spell-fu-goto-next-error)
    (evil-define-key
     'normal 'global (kbd "<leader>s")
     '(lambda ()
        (interactive)
        (cond
         (local-cfg/use-ispell
          (setq local-cfg/use-ispell nil)
          (my-generic-spell-check-disable)
          (message "Spelling: disabled"))
         (t
          (setq local-cfg/use-ispell t)
          (my-generic-spell-check-enable)
          (message "Spelling: enabled")))))

    ;; Highlight block mode.
    (evil-define-key
     'normal 'global (kbd "<leader>b")
     '(lambda ()
        (interactive)
        (cond
         (local-cfg/use-hl-block
          (setq local-cfg/use-hl-block nil)
          (when hl-block-mode
            (hl-block-mode-disable))
          (message "hl-block: disabled"))
         (t
          (setq local-cfg/use-hl-block t)
          (hl-block-mode)
          (message "hl-block: enabled")))))

    ;; -----------------
    ;; Paste with Indent
    ;; -----------------

    ;; https://emacs.stackexchange.com/questions/31454
    (defun paste-and-indent-after ()
      (interactive)
      (with-undo-amalgamate
        (evil-paste-after 1)
        ;; (evil-indent (evil-get-marker ?\[) (evil-get-marker ?\]))
        (i42-region-strip-indentation (evil-get-marker ?\[) (evil-get-marker ?\]))))

    (defun paste-and-indent-before ()
      (interactive)
      (with-undo-amalgamate
        (evil-paste-before 1)
        ;; (evil-indent (evil-get-marker ?\[) (evil-get-marker ?\]))
        (i42-region-strip-indentation (evil-get-marker ?\[) (evil-get-marker ?\]))))

    (cond
     (local-cfg/evil-keyboard-remap
      (evil-define-key '(normal insert) 'global (kbd "<leader>r") 'paste-and-indent-after)
      (evil-define-key '(normal insert) 'global (kbd "<leader>R") 'paste-and-indent-before))
     (t
      (evil-define-key '(normal insert) 'global (kbd "<leader>p") 'paste-and-indent-after)
      (evil-define-key '(normal insert) 'global (kbd "<leader>P") 'paste-and-indent-before)))


    ;; ------------------------------
    ;; Surround with double back-tick
    ;; ------------------------------

    (evil-define-key
     'normal 'global (kbd "<leader>`") 'i42-insert-surround-word-or-region-quote-backtick)
    (evil-define-key
     'normal 'global (kbd "<leader>'") 'i42-insert-surround-word-or-region-quote-single)
    (evil-define-key
     'normal 'global (kbd "<leader>\"") 'i42-insert-surround-word-or-region-quote-double)
    (evil-define-key
     'normal 'global (kbd "<leader>(") 'i42-insert-surround-word-or-region-bracket-parens)
    (evil-define-key
     'normal 'global (kbd "<leader>[") 'i42-insert-surround-word-or-region-bracket-square)
    (evil-define-key
     'normal 'global (kbd "<leader>{") 'i42-insert-surround-word-or-region-bracket-brace))


  (defun my-evil-setup ()
    (with-eval-after-load 'evil
      (my-evil-keys)
      (my-evil-leader-keys))))

(when local-cfg/use-bray

  (defun bray-keyfree (char)
    (interactive "c")
    (message "Key Free: %S" char))

  (defun bray-define-keys (map &rest keybinds)
    (declare (indent 1))
    (pcase-dolist (`(,key . ,def) keybinds)
      (define-key map (kbd key) def)))

  (with-eval-after-load 'repeat
    (push 'bray-ex-move-end-of-line repeat-too-dangerous)
    (push 'bray-ex-move-beginning-of-line repeat-too-dangerous)
    (push 'bray-ex-move-char-right repeat-too-dangerous)
    (push 'bray-ex-move-char-left repeat-too-dangerous)
    (push 'bray-ex-move-line-next repeat-too-dangerous)
    (push 'bray-ex-move-line-prev repeat-too-dangerous))

  ;; +------------+------------+------------+------------+------------+  +------------+------------+------------+------------+-------------+-------------+
  ;; | Grab       |            |            |            |            |  |            |            |            |            |             |             |
  ;; | GrabSwap   |            |            |            |            |  |            |            |            |            |             |             |
  ;; |            |            |            |            |            |  |            |            |            |            |             |             |
  ;; |            |            |            |            |            |  |            |            |            |            |             |             |
  ;; |            |            |            |            |            |  |            |            |            |            |             |             |
  ;; |           `|            |            |            |            |  |            |            |            |            |             |             |
  ;; +------------+------------+------------+------------+------------+  +------------+------------+------------+------------+-------------+-------------+
  ;;
  ;; +------------+------------+------------+------------+------------+  +------------+------------+------------+------------+-------------+-------------+
  ;; | Repeat     | Register   | Yank       | PasteClip  | ReplaceCh  |  | BegOfThing | JumpNext   | JumpBack   | EndOfThing |  FindRepeat | MacroExec   |
  ;; | ReptSubst:S|            | YankCut:S  | PasteLine:S|            |  |            | IMenuNext:S| IMenuBack:S|            |  FindRev:S  | MacroRec:S  |
  ;; | ReptPop:C  |            |            |            |            |  |            | BufferBeg/g| BufferEnd/g|            |             |             |
  ;; |            |            |            |            |            |  |            | AvyNext/f  | AvyPrev/f  |            |             | Fill&Move/g |
  ;; |            |            |            |            |            |  |            |            |            |            |             |             |
  ;; |           q|           w|           e|           r|           t|  |           y|           u|           i|           o|            p|            \|
  ;; +------------+------------+------------+------------+------------+  +------------+------------+------------+------------+-------------+-------------+
  ;; | SwapPtMark | Ins        | Select     | Find...    | Ex/Leader  |  | Left       | Down       | Up         | Right      | JumpSexp    |             |
  ;; |            | Append:S   | SelLn:S    |            |            |  | HomeNoWs:S | ParaDown:S | ParaUp:S   | EndNoWs:S  | JumpStrCmt:S|             |
  ;; |            |            | SelBlk:C   |            |            |  | InsPrev/g  |            |            | InsNext/g  |             |             |
  ;; |            |            |            |            |            |  | Find<Ch/f  | FileEnd/g  | FileBeg/g  | Find>Ch/f  | GotoLine/f  |             |
  ;; |            |            |            |            |            |  |Til<Ch:S/f  | FindNext/f | FindPrev/f |Til>Ch:S/f  |             |             |
  ;; |           a|           s|           d|           f|           g|  |           h|           j|           k|           l|            ;|            '|
  ;; +------------+------------+------------+------------+------------+  +------------+------------+------------+------------+-------------+-------------+
  ;; | Undo       | DelChar    | DelRegion  | PasteKill  | Change     |  | WordBack   | FindNext   | FindPrev   | WordNext   | WordEnd     |
  ;; | Redo:S     | BkSpace:S  | RepMode:S  | PasteLine:S| Join:S     |  | WORDBack:S | ScrlDown:S | ScrlUp:S   | WORDNext:S | WORDEnd:S   |
  ;; |            |            |            |            |            |  |            |            |            |            |             |
  ;; |            |            |            |            |            |  |            | Downcase/g | Upcase/g   |            |             |
  ;; |            |            |            |            |            |  |            | WordNext/f | WordPrev/f |            |             |
  ;; |           z|           x|           c|           v|           b|  |           n|           m|           ,|           .|            /|
  ;; +------------+------------+------------+------------+------------+  +------------+------------+------------+------------+-------------+

  (defun my-bray-basis-keys ()
    (bray-define-keys
     bray-state-keymap-normal

     '("1" . bray-ex-numeric-1)
     '("2" . bray-ex-numeric-2)
     '("3" . bray-ex-numeric-3)
     '("4" . bray-ex-numeric-4)
     '("5" . bray-ex-numeric-5)
     '("6" . bray-ex-numeric-6)
     '("7" . bray-ex-numeric-7)
     '("8" . bray-ex-numeric-8)
     '("9" . bray-ex-numeric-9)
     '("0" . bray-ex-numeric-0)

     '("`" . bray-ex-secondary-selection-from-region)
     '("~" . bray-ex-swap-grab)

     ;; ----
     ;; Left Hand: Row 1.
     ;; ----

     ;; '("q" . repeat)
     '("q" . repeat-fu-execute)
     '("Q" . bray-keyfree)

     '("w" . bray-ex-register-clipboard-actions)


     '("W" . bray-keyfree)

     '("e" . bray-ex-copy-clipboard-only) ;; Copy.
     '("E" . bray-ex-cut-clipboard-only) ;; Cut.

     '("r" . bray-ex-yank-replace-from-clipboard-only)
     '("R" . bray-ex-yank-replace-as-lines-from-clipboard-only)

     '("t" . bray-ex-replace-char)
     '("T" . bray-keyfree)

     ;; Left Hand: Row 2.
     '("a" . bray-ex-reverse)
     '("A" . bray-keyfree)

     '("s" . bray-ex-insert)
     '("S" . bray-ex-append)

     '("d" . bray-ex-region-toggle)
     '("D" . bray-ex-line-select-extend)

     '("C-d" . rectangle-mark-mode)

     '("f h" . bray-ex-find-prev)
     '("f H" . bray-ex-till-prev)
     '("f j" . bray-ex-isearch-forward-regexp)
     '("f k" . bray-ex-isearch-backward-regexp)
     '("f l" . bray-ex-find-next)
     '("f L" . bray-ex-till-next)

     '("f m" . bray-ex-isearch-next-at-point)
     '("f ," . bray-ex-isearch-back-at-point)

     '("f u" . avy-goto-symbol-1-below)
     '("f i" . avy-goto-symbol-1-above)

     ;; Alternative to "C-:" (frees up a key).
     '("f ;" . goto-line)

     '("F" . bray-keyfree)

     '("g h" . bray-ex-open-bol)
     '("g j" . bray-ex-open-below)
     '("g k" . bray-ex-open-above)
     '("g l" . bray-ex-open-eol)

     '("g m" . downcase-region)
     '("g ," . upcase-region)

     ;; From VIM, handy.
     '("g \\" . fill-region)

     '("g u" . end-of-buffer)
     '("g i" . beginning-of-buffer)

     '("G" . bray-keyfree)

     ;; Left Hand: Row 3.
     '("z" . undo-fu-only-undo)
     '("Z" . undo-fu-only-redo)

     '("x" . bray-ex-delete-char-forward)
     '("X" . bray-ex-delete-char-backward)

     '("c" . bray-ex-kill)
     '("C" . bray-ex-insert-overwrite)

     '("v" . bray-ex-yank-replace-from-kill-ring-only)
     '("V" . bray-ex-yank-replace-as-lines-from-kill-ring-only)

     '("b" . bray-ex-change)
     '("B" . bray-ex-join-eol)

     ;; Right Hand: Row 1.
     '("y" . bray-ex-line-goto-non-space-beginning)
     '("Y" . bray-keyfree)

     '("u" . bray-ex-move-paragraph-forward)
     '("U" . bray-keyfree)

     '("i" . bray-ex-move-paragraph-backward)
     '("I" . bray-keyfree)

     '("o" . bray-ex-line-goto-non-space-end)
     '("O" . bray-keyfree)

     '("p" . bray-keyfree)
     '("P" . bray-keyfree)

     ;; Right Hand: Row 2.
     '("h" . bray-ex-move-char-left)
     '("H" . bray-ex-line-goto-non-space-beginning)

     '("j" . bray-ex-move-line-next)
     '("J" . bray-ex-move-paragraph-forward)

     '("k" . bray-ex-move-line-prev)
     '("K" . bray-ex-move-paragraph-backward)

     '("l" . bray-ex-move-char-right)
     '("L" . bray-ex-line-goto-non-space-end)

     '(";" . bray-ex-jump-to-matching-bracket) ;; Use instead of `bray-reverse'.
     '(":" . bray-ex-jump-to-matching-syntax)

     '("'" . bray-keyfree)
     '("\"" . bray-keyfree)

     ;; Right Hand: Row 3.
     '("n" . bray-ex-back-symbol) ;; +.
     '("N" . bray-ex-back-word) ;; +.

     '("m" . bray-ex-isearch-repeat-forward)
     '("M" . bray-keyfree)

     '("," . bray-ex-isearch-repeat-backward)
     '("<" . bray-keyfree)

     '("." . bray-ex-next-symbol) ;; +.
     '(">" . bray-ex-next-word) ;; +.

     '("/" . bray-ex-next-symbol-end)
     '("?" . bray-ex-next-word-end)

     ;; Other keys.
     '("\\" . bray-ex-kmacro-end-or-call-macro-undo-amalgamate)
     '("|" . bray-ex-kmacro-start-macro)

     '("{" . bray-ex-move-paragraph-backward)
     '("}" . bray-ex-move-paragraph-forward)


     ;; '("RET" . bray-set-insert-state)
     )

    (bray-define-keys bray-state-keymap-insert '("<escape>" . bray-state-stack-pop)))

  (defun my-bray-leader-keys ()
    ;; ;; Use "g" as leader instead of space, allowing space as my own leader.
    ;; (define-key bray-state-keymap-normal "g" bray-leader-keymap)
    ;; (define-key bray-motion-state-keymap "g" bray-leader-keymap)

    (define-key bray-state-keymap-normal (kbd "SPC o") 'i42-sort-line-in-block)
    (define-key bray-state-keymap-normal (kbd "SPC TAB") 'revert-buffer-all)
    (define-key bray-state-keymap-normal (kbd "SPC <escape>") 'i42-close-all-other-windows)

    (define-key bray-state-keymap-normal (kbd "SPC x") 'shrink-whitespace)

    (define-key bray-state-keymap-normal (kbd "SPC h") 'i42-transpose-structured-backward)
    (define-key bray-state-keymap-normal (kbd "SPC l") 'i42-transpose-structured-forward)

    (define-key bray-state-keymap-normal (kbd "SPC d") 'i42-diff-at-point-with-other-rev)

    (define-key bray-state-keymap-normal (kbd "SPC e") 'spell-fu-goto-next-error)

    (define-key bray-state-keymap-normal (kbd "SPC z") 'i42-diff-revert-hunks-at-point)
    (define-key bray-state-keymap-normal (kbd "SPC Z") 'i42-diff-revert-hunks-all)

    (define-key bray-state-keymap-normal (kbd "SPC <RET>") 'run-stuff-command-on-region-or-line)

    (define-key
     bray-state-keymap-normal (kbd "SPC r") 'bray-ex-yank-replace-from-clipboard-only-with-indent)

    ;; Surrounding chars.
    (define-key
     bray-state-keymap-normal (kbd "SPC `") 'i42-insert-surround-word-or-region-quote-backtick)
    (define-key
     bray-state-keymap-normal (kbd "SPC '") 'i42-insert-surround-word-or-region-quote-single)
    (define-key
     bray-state-keymap-normal (kbd "SPC \"") 'i42-insert-surround-word-or-region-quote-double)
    (define-key
     bray-state-keymap-normal (kbd "SPC (") 'i42-insert-surround-word-or-region-bracket-parens)
    (define-key
     bray-state-keymap-normal (kbd "SPC [") 'i42-insert-surround-word-or-region-bracket-square)
    (define-key
     bray-state-keymap-normal (kbd "SPC {") 'i42-insert-surround-word-or-region-bracket-brace)

    ;;
    )

  (defun my-bray-register-keys ()

    (define-key bray-ex-state-register-clipboard-map (kbd "e") 'bray-ex-register-alt-copy)
    (define-key bray-ex-state-register-clipboard-map (kbd "E") 'bray-ex-register-alt-cut)

    (define-key bray-ex-state-register-clipboard-map (kbd "r") 'bray-ex-register-alt-paste)
    (define-key bray-ex-state-register-clipboard-map (kbd "R") 'bray-ex-register-alt-paste-lines))


  (defun my-bray-other-keys ()

    (define-key
     bray-state-keymap-normal (kbd "C-`")
     (lambda ()
       (interactive)
       (call-interactively 'narrow-to-region)
       (setq deactivate-mark t)))
    (define-key bray-state-keymap-normal (kbd "M-`") 'widen)

    (define-key bray-state-keymap-normal (kbd "TAB") 'bray-ex-indent-rigidly)
    (progn
      (define-key indent-rigidly-map (kbd "<left>") nil)
      (define-key indent-rigidly-map (kbd "<right>") nil)
      (define-key indent-rigidly-map (kbd "S-<left>") nil)
      (define-key indent-rigidly-map (kbd "S-<right>") nil)

      (define-key indent-rigidly-map (kbd "TAB") 'indent-for-tab-command)

      (define-key indent-rigidly-map (kbd "h") 'indent-rigidly-left-to-tab-stop)
      (define-key indent-rigidly-map (kbd "H") 'indent-rigidly-left)

      (define-key indent-rigidly-map (kbd "l") 'indent-rigidly-right-to-tab-stop)
      (define-key indent-rigidly-map (kbd "L") 'indent-rigidly-right))

    (define-key bray-state-keymap-normal (kbd "=") 'bray-ex-region-syntax-expand)
    (define-key bray-state-keymap-normal (kbd "-") 'bray-ex-region-syntax-contract)

    (define-key bray-state-keymap-normal (kbd "C-S-u") 'my-generic-usage-of-thing-at-point)

    (define-key bray-state-keymap-normal (kbd "C-l") 'my-generic-doc-jump-section)
    (define-key bray-state-keymap-normal (kbd "C-j") 'counsel-at-point-imenu)

    (when local-cfg/use-ivy
      ;; Fill in search based on the cursor.
      (define-key bray-state-keymap-normal (kbd "C-f") 'counsel-at-point-rg)

      (define-key bray-state-keymap-normal (kbd "C-M-f") 'swiper))

    (define-key bray-state-keymap-normal (kbd "C-w") 'my-generic-fix-action)

    (define-key bray-state-keymap-normal (kbd "C-S-j") 'i42-imenu-cycle-next)
    (define-key bray-state-keymap-normal (kbd "C-S-k") 'i42-imenu-cycle-prev)

    ;; Extended keys.
    (define-key bray-state-keymap-normal (kbd "C-SPC") 'my-comment-or-uncomment-region-or-line)

    (define-key bray-state-keymap-normal (kbd "C-M-z") 'vundo)

    (define-key bray-state-keymap-normal (kbd "C-n") 'i42-toggle-display-line-numbers)
    (define-key bray-state-keymap-normal (kbd "C-M-n") 'i42-toggle-display-line-numbers-relative)

    (define-key bray-state-keymap-normal (kbd "M-RET") 'my-generic-goto-thing-at-point)

    (define-key bray-state-keymap-normal (kbd "RET") 'my-generic-line-break-below)
    ;; (define-key bray-state-keymap-normal (kbd "RET") 'newline)
    (define-key bray-state-keymap-normal (kbd "S-<return>") 'my-generic-line-break-split)

    (when local-cfg/use-scroll-on-drag
      (define-key
       bray-state-keymap-normal [down-mouse-2]
       (lambda ()
         (interactive)
         (unless (scroll-on-drag)
           (or (mouse-yank-secondary t) (mouse-yank-primary t))))))

    (define-key
     bray-state-keymap-normal (kbd "M-s") 'i42-delete-surround-at-point-with-bracket-before)

    ;; (define-key bray-state-keymap-normal (kbd "C-a") 'holy-numbers/inc-at-pt)
    ;; (define-key bray-state-keymap-normal (kbd "C-x") 'holy-numbers/dec-at-pt)
    (define-key bray-state-keymap-normal (kbd "C-a") 'shift-number-up)
    (define-key bray-state-keymap-normal (kbd "C-x") 'shift-number-down)

    (define-key bray-state-keymap-normal (kbd "M-z") 'i42-recomplete-contextual)
    (define-key bray-state-keymap-normal (kbd "M-a") 'cycle-at-point)
    (define-key bray-state-keymap-normal (kbd "M-c") 'i42-recomplete-contextual-cycle-case)

    (define-key bray-state-keymap-normal (kbd "M-d") 'diff-at-point-open-and-goto-hunk)

    ;; TODO move to sidecar-locals.
    (define-key bray-state-keymap-normal (kbd "M-t") 'utimeclock-toggle)
    (define-key bray-state-keymap-insert (kbd "M-t") 'utimeclock-toggle)

    ;; NOTE: would be "visual" mode if MEOW supported that.
    (define-key bray-state-keymap-normal (kbd "C-?") 'i42-comment-multi-line-toggle)

    ;; Insert Mode

    ;; Editing keys.
    (define-key bray-state-keymap-insert (kbd "C-<backspace>") 'bray-ex-backward-kill-symbol)
    (define-key bray-state-keymap-insert (kbd "C-<delete>") 'bray-ex-kill-symbol)
    (define-key bray-state-keymap-insert (kbd "C-z") 'undo-fu-only-undo)
    (define-key bray-state-keymap-insert (kbd "C-S-z") 'undo-fu-only-redo)

    ;; Follow normal mode keys with Control prefix.
    (define-key bray-state-keymap-insert (kbd "C-e") 'kill-ring-save)

    (define-key bray-state-keymap-insert (kbd "C-r") 'bray-ex-yank-replace-from-clipboard-only)
    (define-key
     bray-state-keymap-insert (kbd "C-S-r") 'bray-ex-yank-replace-as-lines-from-clipboard-only)

    (define-key bray-state-keymap-insert (kbd "C-v") 'bray-ex-yank-replace-from-kill-ring-only)
    (define-key
     bray-state-keymap-insert (kbd "C-S-v") 'bray-ex-yank-replace-as-lines-from-kill-ring-only)

    (define-key bray-state-keymap-insert "\t" 'tab-to-tab-stop)

    ;; Mono complete mode only.
    (define-key bray-state-keymap-insert (kbd "<end>") 'mono-complete-expand-or-fallback)

    (define-key bray-state-keymap-insert (kbd "<backspace>") 'i42-backspace-whitespace-to-tab-stop)

    (define-key bray-state-keymap-insert (kbd "C-M-/") 'my-generic-comment-block)
    (define-key bray-state-keymap-insert (kbd "C-M-?") 'my-generic-comment-block-docstring)

    ;; (define-key bray-state-keymap-insert (kbd "RET") 'comment-indent-new-line)
    (define-key bray-state-keymap-insert (kbd "RET") 'my-generic-line-break-split)


    ;; (define-key bray-state-keymap-insert (kbd "RET") 'newline)
    (define-key bray-state-keymap-insert (kbd "C-SPC") 'my-company-complete-common-use-context)
    (define-key bray-state-keymap-insert (kbd "C-n") 'company-dabbrev)

    ;; use secondary selection in insert mode
    (define-key bray-state-keymap-insert (kbd "<down-mouse-1>") 'mouse-drag-secondary)
    (define-key bray-state-keymap-insert (kbd "<drag-mouse-1>") 'mouse-drag-secondary)
    (define-key bray-state-keymap-insert (kbd "<mouse-1>") 'mouse-start-secondary)
    (define-key bray-state-keymap-insert (kbd "<mouse-2>") 'i42-mouse-yank-secondary-and-deselect)
    (define-key bray-state-keymap-insert (kbd "<M-down-mouse-1>") 'mouse-drag-region)
    (define-key bray-state-keymap-insert (kbd "<M-drag-mouse-1>") 'mouse-drag-region)
    (define-key bray-state-keymap-insert (kbd "<M-mouse-1>") 'mouse-set-point)
    (define-key bray-state-keymap-insert (kbd "<M-mouse-2>") 'mouse-yank-primary)

    (define-key
     bray-state-keymap-insert (kbd "<mouse-3>") 'i42-mouse-yank-secondary-select-or-word-at-cursor)

    (define-key
     bray-state-keymap-normal
     (kbd "M-k")
     (scroll-on-jump-interactive 'spatial-navigate-backward-vertical-box))

    (define-key
     bray-state-keymap-normal
     (kbd "M-j")
     (scroll-on-jump-interactive 'spatial-navigate-forward-vertical-box))
    (define-key
     bray-state-keymap-normal
     (kbd "M-h")
     (scroll-on-jump-interactive 'spatial-navigate-backward-horizontal-box))
    (define-key
     bray-state-keymap-normal
     (kbd "M-l")
     (scroll-on-jump-interactive 'spatial-navigate-forward-horizontal-box))

    (define-key minibuffer-local-map (kbd "C-e") 'kill-ring-save)
    (define-key minibuffer-local-map (kbd "C-r") 'yank)

    ;;
    )

  (defun my-bray-setup ()
    ;; Extended functions.
    (require 'bray-ex)

    (setq bray-ex-state-insert 'insert)
    (setq bray-state-default 'normal)

    (defvar bray-state-hook-enter-insert nil)
    (defvar bray-state-hook-exit-insert nil)

    (add-hook
     'bray-state-hook-enter-insert
     (lambda ()
       ;; Allow deferring updates when typing.
       (setq jit-lock-defer-time 0.3456789)

       (when local-cfg/use-highlight-idle-mode
         (idle-highlight-mode 0))

       (set-mark (point))
       (deactivate-mark)))

    (add-hook
     'bray-state-hook-exit-insert
     (lambda ()
       (setq jit-lock-defer-time 0)

       (when local-cfg/use-highlight-idle-mode
         (idle-highlight-mode 1))

       ;; (set-mark (point))
       (deactivate-mark)))

    (defun my-bray-is-insert-enabled ()
      (eq bray-state 'insert))

    (setq mono-complete-generic-insert-mode-functions
          (list
           'my-bray-is-insert-enabled 'bray-state-hook-enter-insert 'bray-state-hook-exit-insert))

    (defvar bray-state-keymap-normal (make-keymap))
    (defvar bray-state-keymap-insert (make-keymap))

    ;; Optional, a quick way to mask insertion.
    (define-key bray-state-keymap-normal [remap self-insert-command] 'bray-keyfree)

    (setq bray-state-definitions
          (list
           (list
            :id 'normal
            ;; Define.
            :cursor-type 'hollow
            :lighter "<N>"
            :keymaps (list '(t . bray-state-keymap-normal)))
           (list
            :id 'insert
            ;; Define.
            :cursor-type 'bar
            :lighter "<I>"
            :keymaps (list '(t . bray-state-keymap-insert))

            :enter-hook 'bray-state-hook-enter-insert
            :exit-hook 'bray-state-hook-exit-insert

            ;; Optional.
            :is-input t)))

    (my-bray-basis-keys)

    (my-bray-leader-keys)
    (my-bray-register-keys)
    (my-bray-other-keys)

    (when local-cfg/use-scroll-on-jump
      (scroll-on-jump-advice-add bray-ex-isearch-back-at-point)
      (scroll-on-jump-advice-add bray-ex-isearch-next-at-point)

      (scroll-on-jump-advice-add bray-ex-isearch-repeat-forward)
      (scroll-on-jump-advice-add bray-ex-isearch-repeat-backward))))

(defun my-main-init ()

  (with-eval-after-load 'isearch
    (setq isearch-wrap-pause 'no-ding))

  ;; Enable per-buffer (avoid post-command-hooks).
  (global-font-lock-mode -1)

  ;; Always disable globally, enable per-mode.
  (global-eldoc-mode -1)

  ;; Store registers on exit (nice for macros), annoyingly this can fail with errors!
  (with-demoted-errors "savehist-mode: %S"
    (savehist-mode 1))
  (setq savehist-additional-variables '(register-alist))

  (sidecar-locals-mode)

  (my-setup-global-keys)

  (when local-cfg/use-evil
    (my-evil-setup))
  (when local-cfg/use-bray
    (my-bray-setup))

  ;; ----------------------------------------------------------------------------
  ;; Custom Variables
  ;; ################
  (setq custom-file (concat user-emacs-directory "custom.el"))
  (load custom-file t t)

  (let ((private-init (or (getenv "EMACS_PRIVATE_INIT") "")))
    (unless (string-equal private-init "")
      (load private-init nil t)))

  ;; ----------------------------------------------------------------------------
  ;; Final Application Tasks
  ;; #######################
  ;;
  ;; Anything that needs to be put last (keep to a minimum!).

  ;; Avoid warnings.
  ;; (eval-when-compile
  ;;   (require 'cc-cmds)
  ;;   (require 'cc-mode)
  ;;   (require 'hideshow)
  ;;   (require 'hl-line)
  ;;   (require 'imenu)
  ;;   (require 'use-package))

  ;; Own env var to skip loading server.
  (when (getenv "EMACS_USE_SERVER")
    (load "server" t t)
    (unless (server-running-p)
      (server-start))))

(my-main-init)

;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
