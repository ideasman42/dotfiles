;;; i42-major-mode-rst.el --- None -*- lexical-binding: t -*-

(eval-when-compile
  (require 'rst))

(use-package xref-rst
  :commands (xref-rst-mode)
  :ensure nil
  :load-path "/src/emacs/xref-rst")

;;;###autoload
(defun i42-major-mode-rst ()
  (xref-rst-mode 1)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  (setq-local fill-column 120)
  (setq-local indent-tabs-mode nil)
  (setq-local tab-width 3)
  (setq-local evil-shift-width 3)

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (cond
   (local-cfg/use-highlight-indent-vim
    (require 'visual-indentation-mode)
    (setq-local visual-indentation-width tab-width)
    (visual-indentation-mode))
   (local-cfg/use-highlight-indent-hig
    (highlight-indent-guides-mode))
   (local-cfg/use-highlight-indent-scope
    (hl-indent-scope-mode)))

  ;; Ignore RST keys.
  (define-key rst-mode-map (kbd "C-=") nil)
  (define-key rst-mode-map (kbd "C-M-h") nil)

  ;; Don't delimit on '_'.
  (modify-syntax-entry ?_ "w")

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.rst" "*.py"))

  (with-eval-after-load 'company
    (my-generic-ispell-company-complete-setup))

  ;; TODO, make this generic.
  (defun rst-view-file ()
    (interactive)
    (shell-command
     (concat
      "python -c "
      "\"__import__('webbrowser').open(__import__('sys').argv[-1].replace("
      "'/src/manual/manual/', '/src/manual/build/html/'"
      ").replace('.rst', '.html'))\" - "
      (regexp-quote (buffer-file-name)))))

  ;; Generic functions
  (setq my-generic-run 'rst-view-file)
  ;; (setq my-generic-goto-thing-at-point 'rst-find-in-project-at-point)
  ;; (setq my-generic-usage-of-thing-at-point 'rst-find-in-project-usage)

  (setq my-generic-goto-thing-at-point
        (lambda ()
          (interactive)
          (let ((xref-prompt-for-identifier nil))
            (call-interactively 'xref-find-definitions nil))))

  (setq my-generic-usage-of-thing-at-point
        (lambda ()
          (interactive)
          (let ((xref-prompt-for-identifier nil))
            (call-interactively 'xref-find-references nil))))


  (setq my-generic-build 'stackexchange-compile-context-sensitive))

(provide 'i42-major-mode-rst)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-rst.el ends here
