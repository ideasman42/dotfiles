;;; i42-major-mode-diff.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-diff ()
  ;; go to definition (rust only)
  ;; (define-key diff-mode-shared-map (kbd "M-l") 'diff-at-point-goto-source-and-close)
  (define-key diff-mode-shared-map (kbd "C-M-l") 'diff-at-point-goto-source-and-close)
  ;; (define-key diff-mode-shared-map (kbd "C-M-l") 'diff-goto-source)
  ;; go back
  (define-key diff-mode-shared-map (kbd "C-M-h") 'kill-buffer-and-window)

  ;; TEST.
  (define-key diff-mode-map (kbd "C-q") 'diff-ansi-buffer)

  ;; Generic functions
  ;; 'my-generic-run (no need)
  (setq my-generic-goto-thing-at-point 'diff-at-point-goto-source-and-close)
  (setq my-generic-jump-next 'diff-at-point-goto-source-and-close)
  (setq my-generic-jump-prev 'kill-buffer-and-window)
  (setq my-generic-doc-jump-section 'doc-jump-section-diff)

  (setq my-generic-doc-jump-next 'diff-hunk-next)
  (setq my-generic-doc-jump-prev 'diff-hunk-prev))

(provide 'i42-major-mode-diff)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-diff.el ends here
