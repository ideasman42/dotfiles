;;; i42-major-mode-cmake.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-cmake ()
  (setq-local fill-column 99)
  (setq-local indent-tabs-mode nil)
  (setq-local tab-width 2)

  ;; CMake's mode defines too many shortcuts, clear them all.
  (setq cmake-mode-map (make-keymap))

  (cond
   (local-cfg/use-highlight-indent-scope
    (hl-indent-scope-mode))
   (local-cfg/use-highlight-indent-hig
    (highlight-indent-guides-mode)))

  ;; cmake-mode options.
  (setq-local cmake-tab-width 2)

  (add-hook 'write-file-functions #'delete-trailing-whitespace nil t)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.cmake")
   :include-paths '("**/CMakeLists.txt"))

  ;; Generic functions.
  (setq my-generic-doc-jump-section 'i42-doc-jump-section)

  (setq my-generic-comment-block
        (lambda ()
          (interactive)
          (insert "#[[  ]]")
          (forward-char -3)))

  (setq-local evil-shift-width 2))

(provide 'i42-major-mode-cmake)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-cmake.el.el ends here
