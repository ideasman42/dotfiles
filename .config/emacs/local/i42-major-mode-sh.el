;;; i42-major-mode-sh.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-sh ()
  (setq-local fill-column 120)
  (setq-local tab-width 2)
  (setq-local evil-shift-width 2)
  (setq-local indent-tabs-mode nil)
  (highlight-indent-offset)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.sh"))

  ;; Generic functions.
  (setq my-generic-doc-jump-section 'i42-doc-jump-section))

(provide 'i42-major-mode-sh)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-sh.el ends here
