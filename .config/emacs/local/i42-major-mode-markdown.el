;;; i42-major-mode-markdown.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-markdown ()
  (setq-local fill-column 120)
  (setq-local indent-tabs-mode nil)

  ;; Don't delimit on '_'.
  (modify-syntax-entry ?_ "w")

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.md"))

  (with-eval-after-load 'company
    (my-generic-ispell-company-complete-setup)))

(provide 'i42-major-mode-markdown)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-markdown.el ends here
