;;; i42-major-mode-rust.el --- None -*- lexical-binding: t -*-

;; For rust auto-completion.
(use-package racer
  :commands (racer-mode))

;;;###autoload
(defun i42-major-mode-rust ()
  (setq-local fill-column 100)
  (setq-local indent-tabs-mode nil)

  (highlight-indent-offset)

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.toml"))

  (racer-mode)
  ;; racer's eldoc can be very slow
  ;; (eldoc-mode)
  (setq-local eldoc-documentation-function #'ignore)

  ;; Generic functions
  (setq my-generic-goto-thing-at-point 'racer-find-definition))

(provide 'i42-major-mode-rust)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-rust.el ends here
