;;; i42-major-mode-nix.el --- None -*- lexical-binding: t -*-

(use-package nixfmt
  :commands (nixfmt-on-save-mode))

;;;###autoload
(defun i42-major-mode-nix ()
  (setq-local fill-column 99)
  (setq-local indent-tabs-mode nil)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode)

    (setq hl-prog-extra-list
          ;; Extend the default list to include the bug tracker ID.
          (append
           (list
            ;; Match `some.text` as a constant.
            '("`[^`\n]+`" 0 comment font-lock-constant-face))
           hl-prog-extra-list))
    (hl-prog-extra-refresh))

  (nixfmt-on-save-mode)

  (i42-path-visibility-setup :include-names '("*.nix")))

(provide 'i42-major-mode-nix)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-nix.el ends here
