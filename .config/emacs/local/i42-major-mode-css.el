;;; i42-major-mode-css.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-css ()
  (setq-local fill-column 120)
  (setq-local indent-tabs-mode t)
  (setq-local tab-width 4)
  (setq-local evil-shift-width 4)

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (cond
   (local-cfg/use-highlight-indent-scope
    (hl-indent-scope-mode))
   (local-cfg/use-highlight-indent-hig
    (highlight-indent-guides-mode))
   (local-cfg/use-highlight-indent-vim
    (require 'visual-indentation-mode)
    (setq-local visual-indentation-width tab-width)
    (visual-indentation-mode)))

  (setq my-generic-comment-block 'my-insert-comment-block-c-style)

  ;; Don't delimit on '_'.
  (modify-syntax-entry ?_ "w")

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.css")))

(provide 'i42-major-mode-css)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-css.el ends here
