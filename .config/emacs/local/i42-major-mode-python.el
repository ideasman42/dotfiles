;;; i42-major-mode-python.el --- None -*- lexical-binding: t -*-

(use-package py-autopep8
  :commands (py-autopep8-mode)

  :load-path "/src/emacs/py-autopep8")

;;;###autoload
(defun i42-major-mode-python ()

  ;; (setq-local fill-column 80)
  (setq-local fill-column 120)
  (setq-local tab-width 4)
  (setq-local indent-tabs-mode nil)

  ;; py-autopep8-mode can check this on save, but it's not necessary to enable in the first
  ;; place if there is no config file.
  (require 'py-autopep8)
  (when (py-autopep8-check-pyproject-exists-with-autopep8)
    (py-autopep8-mode))

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (when local-cfg/use-treesitter
    (setq cycle-at-point-preset-override "python-mode"))

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  ;; don't show whitespace in Python mode (only trailing long lines).
  (setq-local local-cfg/use-show-whitespace nil)

  ;; Always use this package here.
  ;; (highlight-indent-offset)

  (cond
   (local-cfg/use-highlight-indent-scope
    (hl-indent-scope-mode))
   (local-cfg/use-highlight-indent-vim
    (require 'visual-indentation-mode)
    (setq-local visual-indentation-width tab-width)
    (visual-indentation-mode)))

  ;; Prefer no indentation for some chars.
  (setq-local electric-indent-chars (delq ?: electric-indent-chars))

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.py"))

  ;; Generic functions.
  (setq my-generic-doc-jump-section 'i42-doc-jump-section)

  (setq my-generic-comment-block
        (lambda ()
          (interactive)
          (insert "\"\"\"  \"\"\"")
          (forward-char -4)))

  (when local-cfg/use-lsp
    (setq my-generic-goto-thing-at-point 'xdg-open-or-lsp-find-definition)
    (setq my-generic-usage-of-thing-at-point 'lsp-find-references))

  (when (bound-and-true-p evil-state)
    (setq-local evil-numbers-separator-chars "_")))

(provide 'i42-major-mode-python)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-python.el ends here
