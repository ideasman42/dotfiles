;;; i42-major-mode-emacs-lisp.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-emacs-lisp ()

  ;; For some reason this runs on startup, avoid that.
  (when buffer-file-name

    ;; (modify-syntax-entry ?. "w")
    ;; (modify-syntax-entry ?+ "w")
    ;; (modify-syntax-entry ?- "w")
    ;; (modify-syntax-entry ?_ "w")
    ;; (modify-syntax-entry ?/ "w")
    ;; (modify-syntax-entry ?: "w")

    ;; Emacs own emacs-lisp uses this, it's code indents poorly without this.
    (setq-local tab-width 8)


    (setq-local indent-tabs-mode nil)

    (setq-local fill-column 80)

    (cond
     (local-cfg/use-highlight-indent-scope
      (hl-indent-scope-mode)))

    ;; When using own auto-formatter, use simple
    (cond
     ((and buffer-file-name
           (locate-dominating-file (file-name-directory buffer-file-name) ".elisp-autofmt"))

      ;; Use own non-Emacs overrides, otherwise use typical Emacs indentation.
      ;; (setq-local fill-column 99)
      ;; (setq-local lisp-indent-function nil)
      ;; (setq-local tab-width 2)
      ;; (setq-local lisp-indent-offset 2)
      )

     (t
      (setq-local fill-column 80)))

    (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

    (i42-path-visibility-setup
     :exclude-paths '("*/\\.*" "./*__pycache__/*")
     :include-names '("*.el" "*.org" "*.rst"))

    ;; In case hl-block-mode gets enabled.
    (setq-local hl-block-bracket "(")

    (when local-cfg/use-eldoc-mode
      (eldoc-mode 1))

    (lisp-extra-font-lock-mode)

    (nameless-mode)

    ;; Generic functions.
    (setq my-generic-run 'eval-buffer)

    ;; Generic functions.
    (setq my-generic-doc-jump-section 'i42-doc-jump-section)

    (setq my-generic-goto-thing-at-point
          (lambda ()
            (interactive)
            (let ((xref-prompt-for-identifier nil))
              (call-interactively 'xref-find-definitions nil))))

    (setq my-generic-usage-of-thing-at-point
          (lambda ()
            (interactive)
            (let ((xref-prompt-for-identifier nil))
              (call-interactively 'xref-find-references nil))))


    (elisp-autofmt-mode)

    (when (bound-and-true-p evil-state)
      ;; Evil mode.
      (setq-local evil-shift-width 2))))

(provide 'i42-major-mode-emacs-lisp)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-emacs-lisp.el ends here
