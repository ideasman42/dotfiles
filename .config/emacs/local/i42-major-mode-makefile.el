;;; i42-major-mode-makefile.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-makefile ()
  (setq-local local-cfg/use-show-whitespace t)

  (setq-local fill-column 120)
  (setq-local indent-tabs-mode t)
  (setq-local tab-width 4)
  (setq-local evil-shift-width 4)

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.make")
   :include-paths '("**/Makefile" "**/GNUmakefile"))

  ;; Generic functions.
  (setq my-generic-doc-jump-section 'i42-doc-jump-section))

(provide 'i42-major-mode-makefile)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-makefile.el ends here
