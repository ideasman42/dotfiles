;;; gui-clipboard-history-mode.el --- Store a history of clipboard values -*- lexical-binding: t -*-

;;; Usage:

;; Support pasting from the clipboard, in states besides
;; the most recent clipboard.
;; Call gui-clipboard-history-paste-penultimate for the second last clipboard,
;; otherwise you can call gui-clipboard-history-paste with an index argument,
;; the larger the number the older the clipboard value.

;;; Code:

;; Clipboard history.
(defvar gui-clipboard-history-limit 10)
(defvar gui-clipboard-history--list nil)

;;;###autoload
(defun gui-clipboard-history-paste (&optional index)
  (insert (nth (or index 0) gui-clipboard-history--list)))

;;;###autoload
(defun gui-clipboard-history-paste-penultimate ()
  (interactive)
  (gui-clipboard-history-paste))

(defun gui-clipboard-history--gui-set-selection (orig-fn type data)
  (when (eq type 'CLIPBOARD)
    (let ((old-data (gui-get-selection type)))
      (when old-data
        (let ((trim-number (- (length gui-clipboard-history--list) gui-clipboard-history-limit)))
          (when (> 0 trim-number)
            (nbutlast gui-clipboard-history--list trim-number)))
        (push old-data gui-clipboard-history--list))))
  (funcall orig-fn type data))

;;;###autoload
(define-minor-mode gui-clipboard-history-mode
  "Highlight block under the cursor."
  :group 'gui-clipboard-history-mode
  :global t
  :lighter
  ""
  (cond
   (gui-clipboard-history-mode
    (advice-add 'gui-set-selection :around #'gui-clipboard-history--gui-set-selection))
   (t
    (advice-remove 'gui-set-selection #'gui-clipboard-history--gui-set-selection))))

(provide 'gui-clipboard-history-mode)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; gui-clipboard-history-mode.el ends here
