;;; i42-major-mode-toml.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-toml ()

  (setq-local fill-column 120)
  (setq-local indent-tabs-mode nil)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./target/*")
   :include-names '("*.rs" "*.toml")))

(provide 'i42-major-mode-toml)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-toml-tomlel ends here
