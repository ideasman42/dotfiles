;;; i42-major-mode-glsl.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-glsl ()
  (setq-local fill-column 120)
  (setq-local indent-tabs-mode t)

  (clang-format-on-save-mode)

  (when local-cfg/use-comment-keywords
    (hl-prog-extra-mode))

  ;; clang-format now handles.
  ;; (add-to-list 'write-file-functions 'delete-trailing-whitespace)

  (cond
   (local-cfg/use-highlight-indent-scope
    (hl-indent-scope-mode))
   (local-cfg/use-highlight-indent-hig
    (highlight-indent-guides-mode)))

  ;; Don't delimit on '_'.
  (modify-syntax-entry ?_ "w")

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.glsl")))

(provide 'i42-major-mode-glsl)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-glsl.el ends here
