;;; i42-major-mode-c-generic.el --- None -*- lexical-binding: t -*-

(defun c-doc-next ()
  "Follow link/goto line."
  (interactive)
  (cond
   ((search-forward-regexp "\\(\\\\}\\|\\\\{\\)" nil t 1)
    (pcase (preceding-char)
      (?{ ;; Section Start
       (search-backward "/*" nil t 1) ;; find start of comment
       (move-beginning-of-line nil) ;; line start
       (recenter
        (max scroll-margin
             (/ (window-height)
                my-scroll-margin-recenter-div))) ;; place view
       (search-forward "*/" nil t 1) ;; comment end
       (forward-line 1) ;; step out of the comment
       )
      (?} ;; Section End
       (search-forward "*/" nil t 1) ;; comment end
       (move-beginning-of-line nil) ;; un-indent
       (recenter) ;; place view (likely its already been moved down)
       (forward-line 1) ;; step out of the comment
       )))
   (t ;; fallback
    (goto-char (point-max)))))
(defun c-doc-prev ()
  "Follow link/goto line."
  (interactive)
  (cond
   ((search-backward-regexp "\\(\\\\}\\|\\\\{\\)" nil t 1)
    (pcase (char-after (+ 1 (point)))
      (?{ ;; Section Start
       (search-backward "/*" nil t 1) ;; find start of comment
       (move-beginning-of-line nil) ;; line start
       (recenter
        (max scroll-margin
             (/ (window-height)
                my-scroll-margin-recenter-div))) ;; place view
       (forward-line -1) ;; step out of the comment
       )
      (?} ;; Section End
       (search-backward "/*" nil t 1) ;; comment end
       (move-beginning-of-line nil) ;; line start
       (forward-line -1) ;; step out of the comment
       (recenter) ;; place view (likely its already been moved down)
       )))
   (t ;; fallback
    (goto-char (point-min)))))

(defun c-expand-selection ()
  (interactive)
  (save-restriction
    (let ((table (make-syntax-table))
          (count 0))
      (with-syntax-table table
        (when (region-active-p)
          (narrow-to-region (region-beginning) (region-end)))

        ;; Remove overlines:
        ;; /*****/ /* ---- */ /* ==== */
        (goto-char (point-min))
        (while (re-search-forward "\\(\\/\\*[:blank:]*[\\-\\*=[:blank:]]+[:blank:]*\\*\\/\n\\)"
                                  nil
                                  t)
          (setq count (1+ count))
          (replace-match "" t nil nil))

        ;; Replace: '/* ---- Foo ---- */'
        (goto-char (point-min))
        (while (re-search-forward (concat
                                   "\\(\\/\\*\\)"
                                   "\\([[:blank:]]*\\)"
                                   "\\([-\\*=]+\\)"
                                   "\\([[:blank:]]+\\)"
                                   "\\([^\n]+\\)"
                                   "\\([[:blank:]]+\\)"
                                   "\\([-\\*=]+\\)"
                                   "\\([[:blank:]]*\\)"
                                   "\\(\\*\\/\\)")
                                  nil t)
          (setq count (1+ count))
          (replace-match
           (concat
            "/** \\\\} */\n"
            "\n"
            "/* -------------------------------------------------------------------- */\n"
            "/** \\\\name \\5\n * \\\\{ */")
           t nil nil))

        ;; ;; Replace: '/* Foo */'
        ;; (goto-char (point-min))
        ;; (while
        ;;   (re-search-forward
        ;;     (concat
        ;;       "\\(\\/\\*\\)"
        ;;       "\\([[:blank:]]*\\)"
        ;;       "\\([^\n]+\\)"
        ;;       "\\([[:blank:]]*\\)"
        ;;       "\\(\\*\\/\\)")
        ;;     nil t)
        ;;   (setq count (1+ count))
        ;;   (replace-match
        ;;     (concat
        ;;       "/** \\\\} */\n"
        ;;       "\n"
        ;;       "/* -------------------------------------------------------------------- */\n"
        ;;       "/** \\\\name \\3\n * \\\\{ */")
        ;;     t nil nil))

        ;; (goto-char (point-min))
        ;; (while (re-search-forward "\\bT\\([[:digit:]]+\\)\\b" nil t)
        ;;   (setq count (1+ count))
        ;;   (replace-match ":task:`\\1`" t nil nil))

        ;; (goto-char (point-min))
        ;; (while (re-search-forward "\\brB\\([[:digit:]a-zA-F]+\\)\\b" nil t)
        ;;   (setq count (1+ count))
        ;;   (replace-match ":rev:`B\\1`" t nil nil))

        ;; (goto-char (point-min))
        ;; (while (re-search-forward "\\brBM\\([[:digit:]a-zA-F]+\\)\\b" nil t)
        ;;   (setq count (1+ count))
        ;;   (replace-match ":rev:`BM\\1`" t nil nil))

        (message "Replaced %d items" count)))))


;; NOTE: I might want something similar to this for tree-sitter, add/modify as needed.
;; Don't combine them as they might use different keys.
(with-eval-after-load 'cc-mode
  (dolist (this-map (list c-mode-map c++-mode-map))

    ;; This is in fact useful, don't override.
    ;; (define-key this-map (kbd "{") nil)
    ;; (define-key this-map (kbd "}") nil)

    ;; Don't use these keys, I use them elsewhere.
    (define-key this-map (kbd "M-e") nil)
    (define-key this-map (kbd "M-q") nil)
    (define-key this-map (kbd "TAB") nil)

    (define-key this-map (kbd "/") nil)
    (define-key this-map (kbd "*") nil)
    (define-key this-map (kbd ",") nil)
    (define-key this-map (kbd "(") nil)
    (define-key this-map (kbd ")") nil)
    (define-key this-map (kbd ";") nil)
    (define-key this-map (kbd ":") nil)
    (define-key this-map (kbd "C-M-h") nil)))


(use-package prog-face-refine
  :commands (prog-face-refine-mode)
  :load-path "/src/emacs/prog-face-refine")

(use-package doc-show-inline
  :commands (doc-show-inline-mode doc-show-inline-printf doc-show-inline-goto)
  :config

  ;; Skip doxygen groups.
  ;; (setq doc-show-inline-exclude-regexp "\\(\\\\{\\|\\\\}\\)")
  (setq doc-show-inline-exclude-blank-lines 1)

  ;; Ensure the comment region is spell checked.
  (add-hook
   'doc-show-inline-buffer-hook
   (lambda ()
     ;; Custom settings for this project.

     ;; Enable additional highlighting.
     (hl-prog-extra-mode)

     (sidecar-locals-mode)
     ;; Spell check the comments.
     (spell-fu-mode 1)

     ;; Use special DOXYGEN highlighting.
     (prog-face-refine-mode)))

  (add-hook 'doc-show-inline-fontify-hook (lambda (beg end) (spell-fu-region beg end))))

;;;###autoload
(defun i42-major-mode-c-generic ()
  (eval-when-compile
    (require 'cc-mode)
    (require 'cc-vars))

  (setq-local indent-tabs-mode nil)

  (setq-local fill-column 99)

  (when local-cfg/use-treesitter
    (setq cycle-at-point-preset-override
          (cond
           ((eq major-mode 'c++-mode)
            "c++-mode")
           (t
            "c-mode"))))

  (cond
   (local-cfg/use-highlight-indent-scope
    (hl-indent-scope-mode)))

  (clang-format-on-save-mode)

  (when local-cfg/use-comment-keywords
    ;; Set this per-project.
    (hl-prog-extra-mode))


  (prog-face-refine-mode)

  ;; (setq-local c-default-style "blender-style")
  (setq-local c-basic-offset 2)
  (setq-local tab-width 2)
  (setq-local evil-shift-width 2)

  (setq-local c-default-style "k&r")

  (setq-local c-block-comment-prefix "* ")
  ;; don't right align slashes.
  (setq-local c-auto-align-backslashes nil)

  ;; for C
  (setq
   comment-start "//"
   comment-end "")

  ;; never re-indent while typing?, its infact handy for braces,
  ;; (when (fboundp 'electric-indent-mode) (electric-indent-mode -1))

  ;; Prefer no indentation for comment chars.


  ;; (c-set-offset 'arglist-intro '++)
  (c-set-offset 'arglist-intro (lambda (_pair) 4))
  ;; no indentation for hanging parens, eg:
  ;;
  ;;    /* before */
  ;;    function_call(
  ;;            a,
  ;;            b
  ;;                  );
  ;;
  ;;    /* after */
  ;;    function_call(
  ;;            a,
  ;;            b
  ;;    );
  ;;
  ;; (c-set-offset 'arglist-close (lambda (pair) 8))
  (c-set-offset 'arglist-close 0)
  ;; Don't indent text in 'extern {...}' blocks
  ;; (annoying for headers which use 'extern "C" {...}').
  (c-set-offset 'inextern-lang 0)
  ;; Don't indent '{' when it's on it's own line.
  (c-set-offset 'substatement-open 0)
  ;; indent contents of switch statement
  (c-set-offset 'case-label '+)
  ;;    /* Before */
  ;;    case FOO:
  ;;        {
  ;;            bar;
  ;;
  ;;    /* After */
  ;;    case FOO:
  ;;    {
  ;;        bar;
  (c-set-offset 'statement-case-open 0)
  ;; Indent multi-line arrays with one
  ;; indent instead of aligning to the first brace.
  (c-set-offset 'brace-list-intro '+)

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names
   (cond
    ((eq major-mode "glsl-mode")
     '("*.glsl"))
    (t
     '("*.c" "*.cc" "*.cpp" "*.cxx" "*.m" "*.mm" "*.h" "*.hh" "*.hpp" "*.hxx" "*.inl"))))

  ;; Don't delimit on '_'.
  ;; Try disable, navigate by symbols.
  ;; (modify-syntax-entry ?_ "w")

  ;; https://github.com/atilaneves/cmake-ide

  ;; (flycheck-mode) ;; fails with GCC warnings!

  ;; disable, I dont want to use this!
  (setq-local abbrev-mode nil)

  ;; There is a bug in `c-context-line-break' where adding newlines in comment blocks fails.
  ;; Workaround this bug with my own logic.
  ;; TODO: check if this should be reported to be fixed
  (with-eval-after-load 'cc-cmds
    (advice-add
     'c-context-line-break
     :around
     #'(lambda (old-fn &rest args)
         (let ((handled nil)
               (state (syntax-ppss))
               (accepted-dot-point-chars (list ?- ?*)))
           (when (nth 4 state)
             (let ((pos-beg (nth 8 state)))
               (when pos-beg
                 (when (and
                        (eq (char-after pos-beg) ?/) ;; Detect C-style "/*" comment start.
                        (eq (char-after (1+ pos-beg)) ?*))
                   (let ((block-column
                          (save-excursion
                            (goto-char (1+ pos-beg))
                            (current-column)))
                         (indent 1))
                     ;; Calculate in-comment indentation.
                     (save-excursion
                       (goto-char (pos-bol))
                       (forward-char block-column)
                       (when (and
                              (eq (char-after (point)) ?*) ;; Detect block continuation.
                              (eq (char-after (1+ (point))) ?\s))
                         (forward-char 2)
                         (let ((pos-indent-beg (point))
                               (pos-eol (pos-eol)))
                           ;; Skip any number of blank characters forward (can be none).
                           (skip-chars-forward "[:blank:]" pos-eol)
                           ;; Account for dot-points.
                           (when (cond
                                  ;; Detect dot-point: " - Text" for e.g.
                                  ((and (<= (+ (point) 2) pos-eol)
                                        (memq (char-after (point)) accepted-dot-point-chars)
                                        (eq (char-after (1+ (point))) ?\s))
                                   (forward-char 2)
                                   t)
                                  ;; Skip if this is not the EOL, otherwise the line is blank.
                                  ((< (point) pos-eol)
                                   t))
                             (setq indent (+ indent (- (point) pos-indent-beg)))))))

                     (insert "\n" (make-string block-column ?\s) "*" (make-string indent ?\s))
                     (setq handled t))))))
           (unless handled
             (apply old-fn args))))))

  (setq my-generic-build
        '(lambda ()
           (interactive)
           (let ((compilation-read-command nil) ; Ensure 'compile-command' is used.
                 (compile-command "make-universal"))
             (call-interactively 'compile))))

  (setq my-generic-comment-block 'my-insert-comment-block-c-style)
  (setq my-generic-comment-block-docstring 'my-insert-comment-block-c-style-doxygen)

  (setq my-generic-line-break-split 'c-context-line-break)
  (setq my-generic-line-break-below
        '(lambda ()
           (interactive)
           (unless (eolp)
             (end-of-line))
           (c-context-line-break)))

  (setq my-generic-line-break-above
        '(lambda ()
           (interactive)
           ;; note, this isn't perfect - if you perform it on the last line of a comment.
           (unless (eolp)
             (end-of-line))
           (c-context-line-break)
           ;; move line up and goto end
           (transpose-lines 1)
           (forward-line -2)
           (unless (eolp)
             (end-of-line))))

  (setq my-generic-goto-thing-at-point 'xdg-open-or-find-definition)

  (when local-cfg/use-lsp
    (setq my-generic-usage-of-thing-at-point 'lsp-find-references))

  (when local-cfg/use-eglot
    (setq my-generic-rename-thing-at-point 'eglot-rename)
    (setq my-generic-usage-of-thing-at-point 'xref-find-references))

  (setq my-generic-doc-jump-section 'i42-doc-jump-section)

  (setq my-generic-doc-jump-next 'c-doc-next)
  (setq my-generic-doc-jump-prev 'c-doc-prev)

  (setq my-generic-eval-selection 'c-expand-selection))

(provide 'i42-major-mode-c-generic)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-c-generic.el ends here
