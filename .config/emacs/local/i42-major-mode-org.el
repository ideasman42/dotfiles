;;; i42-major-mode-org.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-org ()
  (setq-local fill-column 120)
  (setq-local indent-tabs-mode nil)
  (setq-local tab-width 4)
  (setq-local evil-shift-width 4)

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t)

  (when local-cfg/use-highlight-indent-hig
    (highlight-indent-guides-mode))

  ;; Don't delimit on '_'.
  (modify-syntax-entry ?_ "w")

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.org"))

  (with-eval-after-load 'company
    (my-generic-ispell-company-complete-setup)))

(provide 'i42-major-mode-org)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-org.el ends here
