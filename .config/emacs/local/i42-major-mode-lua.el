;;; i42-major-mode-lua.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-lua ()
  (setq-local fill-column 120)
  (setq-local tab-width 4)
  (setq-local lua-indent-level 4)
  (setq-local indent-tabs-mode nil)
  (highlight-indent-offset)

  (when local-cfg/use-comment-keywords
    (setq-local hl-prog-extra-preset t)
    (hl-prog-extra-mode))

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.lua"))

  ;; Generic functions.
  (setq my-generic-comment-block
        (lambda ()
          (interactive)
          (insert "--[[  ]]--")
          (forward-char -5))))

(provide 'i42-major-mode-lua)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-lua.el ends here
