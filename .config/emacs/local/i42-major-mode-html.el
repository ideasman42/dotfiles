;;; i42-major-mode-html.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-html ()
  (setq-local fill-column 99)
  (setq-local indent-tabs-mode nil)
  (setq-local tab-width 2)
  (setq-local evil-shift-width 2)

  (add-hook 'write-file-functions 'delete-trailing-whitespace nil t))

(provide 'i42-major-mode-html)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-html.el ends here
