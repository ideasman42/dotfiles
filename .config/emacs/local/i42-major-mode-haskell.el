;;; i42-major-mode-haskell.el --- None -*- lexical-binding: t -*-

;;;###autoload
(defun i42-major-mode-haskell ()
  (setq-local fill-column 120)
  (setq-local indent-tabs-mode nil)

  (when local-cfg/use-highlight-indent-hig
    (highlight-indent-guides-mode))

  (i42-path-visibility-setup
   :exclude-paths '("*/\\.*" "./*__pycache__/*")
   :include-names '("*.hs"))

  ;; Don't delimit on '_'.
  (modify-syntax-entry ?_ "w"))

(provide 'i42-major-mode-haskell)
;; Local variables:
;; fill-column: 99
;; elisp-autofmt-load-packages-local: ("use-package")
;; end:
;;; i42-major-mode-haskell.el ends here
