-- awesome_mode: api-level=4:screen=on
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
-- Declarative object management
local ruled = require("ruled")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
naughty.connect_signal("request::display_error", function(message, startup)
    naughty.notification {
        urgency = "critical",
        title   = "Oops, an error happened"..(startup and " during startup!" or "!"),
        message = message
    }
end)
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")

-- customize the theme
beautiful.border_color_normal = "#101010"
beautiful.border_color_active = "#658ABA"
beautiful.border_color_urgent = "#FF0000"
beautiful.border_width = 2
beautiful.notification_font = "sans 20"
beautiful.notification_width = 600


-- This is used later as the default terminal and editor to run.
terminal = "st-default"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Local functions

if false then
    function my_window_hide(c)
        awful.util.spawn("wmctrl_scratchpad.sh minimize")
    end
    function my_window_restore()
        awful.util.spawn("wmctrl_scratchpad.sh restore")
    end
else
    local my_window_hide_order = {}
    function my_window_hide(c)
        c.sticky = true
        c.minimized = true
        table.insert(my_window_hide_order, c)
    end

    local function my_window_restore_client(c, tag)
        c.sticky = false
        c.minimized = false
        c.fullscreen = false
        c:move_to_tag(tag)
        client.focus = c
        c:raise()
        -- Unrelated but handy.
        mouse.coords({x=(c.x + (c.width / 2)), y=(c.y + (c.height / 2))}, true)
    end

    function my_window_restore()
        local tag = awful.tag.selected()
        local done = false
        while my_window_hide_order[1] do
            local c = table.remove(my_window_hide_order)
            if c.valid then
                my_window_restore_client(c, tag)
                done = true
                break
            end
        end

        -- Fallback to scanning all clients,
        -- on the off-chance one is minimized not using this shortcut.
        if not done then
            for c in awful.client.iterate(function (c) return c.minimized end) do
                my_window_restore_client(c, tag)
                break
            end
        end
    end
end

function my_screens_empty()
    local test = false
    for _, s in ipairs(screen) do
        for _, c in ipairs(s.clients) do
            return false
        end
    end
    return true
end

function my_viewstep(dir, is_hidden)
    local index = screen[1].selected_tag.index
    local s_focus = awful.screen.focused()
    while true do
        for s in screen do
            if s ~= s_focus then
                awful.tag.viewidx(dir, s)
            end
        end
        awful.tag.viewidx(dir, s_focus)

        if my_screens_empty() == is_hidden then
            break
        end
        -- Wrapped around
        if screen[1].selected_tag.index == index then
            break
        end
    end
end

function my_viewstep_with_client(c_focus, dir)
    if not c_focus then
        return
    end

    local s_focus = c_focus.screen
    for s in screen do
        if s ~= s_focus then
            awful.tag.viewidx(dir, s)
        end
    end
    awful.tag.viewprev(s_focus)
    local tag = awful.tag.selected()
    c_focus:move_to_tag(tag)
    client.focus = c_focus
end

function my_viewprev_visible()
    my_viewstep(-1, false)
end

function my_viewnext_visible()
    my_viewstep(1, false)
end

function my_viewnext_empty()
    my_viewstep(1, true)
end


-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu(
    {
        items = {
            { "awesome", myawesomemenu, beautiful.awesome_icon },
            { "open terminal", terminal },
        }
    }
)

-- mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon, menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Tag
-- Table of layouts to cover with awful.layout.inc, order matters.
tag.connect_signal("request::default_layouts", function()
    awful.layout.append_default_layouts({
        awful.layout.suit.floating,
        awful.layout.suit.tile,
        awful.layout.suit.tile.left,
        awful.layout.suit.tile.bottom,
        awful.layout.suit.tile.top,
        -- awful.layout.suit.fair,
        -- awful.layout.suit.fair.horizontal,
        awful.layout.suit.spiral,
        -- awful.layout.suit.spiral.dwindle,
        awful.layout.suit.max,
        -- awful.layout.suit.max.fullscreen,
        -- awful.layout.suit.magnifier,
        awful.layout.suit.corner.nw,
    })
end)
-- }}}

-- {{{ Wibar

-- Keyboard map indicator and switcher
-- mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
-- mytextclock = wibox.widget.textclock()

screen.connect_signal("request::desktop_decoration", function(s)
    -- Each screen has its own tag table.
    local layout_for_screen
    if s.index == 1 then
        layout_for_screen = awful.layout.layouts[2]
    else
        layout_for_screen = awful.layout.layouts[3]
    end
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, layout_for_screen)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox {
        screen  = s,
        buttons = {
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
            awful.button({ }, 4, function () awful.layout.inc( 1) end),
            awful.button({ }, 5, function () awful.layout.inc(-1) end),
        }
    }

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ modkey }, 1,
                function(t)
                    if client.focus then
                        client.focus:move_to_tag(t)
                    end
                end
            ),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ modkey }, 3,
                function(t)
                    if client.focus then
                        client.focus:toggle_tag(t)
                    end
                end
            ),
            awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end),
        }
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx( 1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx(-1) end),
        }
    }

    -- Create the wibox
    s.mywibox = awful.wibar({
            position = "bottom",
            screen = s,
            visible = false,
            -- start hidden
    })

    -- Add widgets to the wibox
    s.mywibox.widget = {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            -- mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            -- mykeyboardlayout,
            wibox.widget.systray(),
            -- mytextclock,
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
awful.mouse.append_global_mousebindings({
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev),
})
-- }}}

-- {{{ Key bindings

-- General Awesome keys
awful.keyboard.append_global_keybindings({
    -- Sync tags
    awful.key({ modkey,           }, "Prior", my_viewprev_visible),
    awful.key({ modkey,           }, "Next",  my_viewnext_visible),
    awful.key({ modkey, "Control" }, "k",     my_viewprev_visible),
    awful.key({ modkey, "Control" }, "j",     my_viewnext_visible),

    awful.key({ modkey, "Control"   }, "Return", my_viewnext_empty),

    -- move windows, needs a function
    awful.key({ modkey, "Shift" }, "Prior", function () my_viewstep_with_client(client.focus, -1) end),
    awful.key({ modkey, "Shift" }, "Next",  function () my_viewstep_with_client(client.focus,  1) end),
    -- end move window/tags function

    awful.key({ modkey, }, "Escape", naughty.destroy_all_notifications),

    awful.key({ modkey, }, "Tab",
        function ()
            local c_focus = client.focus
            if c_focus.fullscreen then
                c_focus.fullscreen = false
            end
            awful.client.focus.byidx( 1)
            c_focus = client.focus
            if c_focus then c_focus:raise() end
        end
    ),
    awful.key({ modkey, "Shift" }, "Tab",
        function ()
            local c_focus = client.focus
            if c_focus.fullscreen then
                c_focus.fullscreen = false
            end
            awful.client.focus.byidx(-1)
            c_focus = client.focus
            if c_focus then c_focus:raise() end
        end
    ),

    awful.key({ modkey,           }, "F10", function () awful.util.spawn( "xosd_time" ) end),

    awful.key({ }, "F13",
        function ()
            -- If you want to always position the menu on the same place set coordinates
            awful.menu.menu_keys.down = { "Down", "Alt_L" }
            local cmenu = awful.menu.clients({width=245}, { keygrabber=true })
        end
    ),

    -- Swap windows by direction
    awful.key({ modkey, }, "j", function () awful.client.focus.bydirection("down")
                                if client.focus then client.focus:raise() end end),
    awful.key({ modkey, }, "k", function () awful.client.focus.bydirection("up")
                                if client.focus then client.focus:raise() end end),

    -- Support navigating to empty screen
    awful.key({ modkey,           }, "h",
        function ()
            client_prev = client.focus
            awful.client.focus.global_bydirection("left")
            if (client_prev == client.focus) then
                awful.screen.focus_bydirection("left")
            end
            if client.focus then
                client.focus:raise()
            end
        end
    ),

    awful.key({ modkey,           }, "l",
        function ()
            client_prev = client.focus
            awful.client.focus.global_bydirection("right")
            if (client_prev == client.focus) then
                awful.screen.focus_bydirection("right")
            end
            if client.focus then
                client.focus:raise()
            end
        end
    ),

    --
    -- Move windows by direction
    awful.key({ modkey, "Shift" }, "j", function () awful.client.swap.global_bydirection("down")
                                        if client.focus then client.focus:raise() end end),
    awful.key({ modkey, "Shift" }, "k", function () awful.client.swap.global_bydirection("up")
                                        if client.focus then client.focus:raise() end end),
    awful.key({ modkey, "Shift" }, "h", function () awful.client.swap.global_bydirection("left")
                                        if client.focus then client.focus:raise() end end),
    awful.key({ modkey, "Shift" }, "l", function () awful.client.swap.global_bydirection("right")
                                        if client.focus then client.focus:raise() end end),


    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),

    -- Toggle Bar
    awful.key({ modkey,           }, "b",
        function (c)
            -- Simply toggle all, as if they are global
            local is_visible = not screen[1].mywibox.visible
            for s in screen do
                s.mywibox.visible = is_visible
            end
        end),

    -- Un-minimize (one at a time), reverse of mod-w
    awful.key({ modkey, "Shift"   }, "w", my_window_restore),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Shift"   }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "space", function () awful.layout.inc(1,  nil, layouts) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1, nil, layouts) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    awful.key({ modkey, "Shift"   }, "p", function () awful.screen.focused().mypromptbox:run() end),

    awful.key({ modkey            }, "p", function () awful.util.spawn(os.getenv("HOME") .. "/bin/dmenu_custom", false) end),
    awful.key({ modkey, "Control" }, "p", function () awful.util.spawn(os.getenv("HOME") .. "/bin/dmenu_custom_system", false) end),

    -- Fake paste, for silly web pages that disable pasting.
    awful.key({ modkey            }, "v", function () awful.util.spawn("x11_fake_paste.sh") end),
    awful.key({ modkey            }, "i", function () awful.util.spawn("xcalib -invert -alter -s $(wmctrl_current_screen.sh)") end),

    awful.key({ }, "XF86MonBrightnessUp",   function () awful.util.spawn( "xbacklight -inc 10" ) end),
    awful.key({ }, "XF86MonBrightnessDown", function () awful.util.spawn( "xbacklight -dec 10" ) end),

    awful.key({ }, "XF86AudioPlay", function () awful.util.spawn("audacious --play-pause", false) end),
    awful.key({ }, "XF86AudioNext", function () awful.util.spawn("audacious --fwd", false) end),
    awful.key({ }, "XF86AudioPrev", function () awful.util.spawn("audacious --req", false) end),
    awful.key({ }, "XF86AudioForward", function () awful.util.spawn("audacious-seek +", false) end),
    awful.key({ }, "XF86AudioRewind", function () awful.util.spawn("audacious-seek -", false) end),

    awful.key({ }, "XF86Launch5", function () awful.util.spawn("tts_clipboard", false) end),

    -- Audio keys.
    awful.key({ }, "XF86AudioRaiseVolume", function () awful.util.spawn( "pactl set-sink-volume 0 +5%" ) end),
    awful.key({ }, "XF86AudioLowerVolume", function () awful.util.spawn( "pactl set-sink-volume 0 -5%" ) end),

    awful.key({ }, "XF86Search", function () awful.util.spawn( "dmenu_custom_context_menu" ) end),

    awful.key({ }, "F15", function () awful.util.spawn("nerd-dictation-begin --full-sentence", false) end),
    awful.key({ }, "F16", function () awful.util.spawn("nerd-dictation-end", false) end),
    awful.key({ }, "F17", function () awful.util.spawn("nerd-dictation-begin", false) end),
    awful.key({ }, "F18", function () awful.util.spawn("nerd-dictation-end", false) end),

    -- Reset all windows
    awful.key({ modkey, "Shift" }, "x",
        function ()
            local tag = awful.tag.selected()
            for i=1, #tag:clients() do
                local c = tag:clients()[i]
                c.minimized = false
                c.maximized = false
                c.maximized_horizontal = false
                c.maximized_vertical = false
                c.fullscreen = false
                c.floating = false
            end
        end
    )

})

client.connect_signal("request::default_mousebindings", function()
    awful.mouse.append_client_mousebindings({
        awful.button({ }, 1, function (c)
            c:activate { context = "mouse_click" }
        end),
        awful.button({ modkey }, 1, function (c)
            c:activate { context = "mouse_click", action = "mouse_resize" }
        end),
        awful.button({ modkey }, 3, function (c)
            c:activate { context = "mouse_click", action = "mouse_move" }
        end),
    })
end)

client.connect_signal("request::default_keybindings", function()
    awful.keyboard.append_client_keybindings({
        awful.key({ modkey,           }, "q",      function (c) c:kill()                         end),
        awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
        awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
        awful.key({ modkey,           }, "z",      awful.client.movetoscreen                        ),
        -- same as above but don't move mouse
        awful.key({ modkey, "Shift"   }, "z",
            function (c)
                awful.client.movetoscreen()
                awful.screen.focus_relative(1)
            end
        ),
        awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
        -- minimize (mod-shift-w restores)

        -- The client currently has the input focus, so it cannot be
        -- minimized, since minimized clients can't have the focus.
        awful.key({ modkey,           }, "w",      function(c) my_window_hide(c) end),


        awful.key({ modkey,           }, "f",
            function (c)
                c.fullscreen = not c.fullscreen
                c:raise()
            end
        ),
        awful.key({ modkey,           }, "x",
            function (c)
                local new_maximized = not (c.maximized_horizontal or c.maximized_vertical)
                c.maximized_horizontal = new_maximized
                c.maximized_vertical   = new_maximized
                c:raise()
            end
        )
    })
end)

-- }}}

-- {{{ Rules
-- Rules to apply to new clients.
ruled.client.connect_signal("request::rules", function()
    -- All clients will match this rule.
    ruled.client.append_rule {
        id         = "global",
        rule       = { },
        properties = {
            focus     = awful.client.focus.filter,
            raise     = true,
            screen    = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    }

    -- Floating clients.
    ruled.client.append_rule {
        id       = "floating",
        rule_any = {
            instance = { "copyq", "pinentry" },
            class    = {
                "Arandr", "Blueman-manager", "Gpick", "Kruler", "Sxiv",
                "Tor Browser", "Wpa_gui", "veromix", "xtightvncviewer"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name    = {
                "Event Tester",  -- xev.
            },
            role    = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    }

    -- Add titlebars to normal clients and dialogs
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = true      }
    }
end)

-- }}}

-- {{{ Notifications

ruled.notification.connect_signal('request::rules', function()
    -- All notifications will match this rule.
    ruled.notification.append_rule {
        rule       = { },
        properties = {
            screen           = awful.screen.preferred,
            implicit_timeout = 0,
        }
    }
end)

naughty.connect_signal("request::display", function(n)
    naughty.layout.box { notification = n }
end)

-- }}}

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:activate { context = "mouse_enter", raise = false }
end)


-- Cursor follows focus

local function warp_to_focus_client(c)
    if c == nil then
        return
    end
    client_focus = c.geometry(c)
    mcoords = mouse.coords()
    if mcoords.x < client_focus.x or
        mcoords.x > (client_focus.x + client_focus.width) or
        mcoords.y < client_focus.y or
        mcoords.y > (client_focus.y + client_focus.height) then

        mouse.coords({
            x=(client_focus.x + (client_focus.width / 2)),
            y=(client_focus.y + (client_focus.height / 2))},
            true
        )
    end
end

client.connect_signal("request::activate", warp_to_focus_client)
-- client.connect_signal("request::rules", function() warp_to_focus_client(client.focus) end)

client.connect_signal("request::manage", function (c)
    if not awesome.startup then
        local my_unfocus = awful.client.focus.history.get(c.screen, 1, nil)
        if my_unfocus then
            awful.client.setslave(c)
            local c_step_init = nil
            while true do
                local c_step = awful.client.next(-1, c)
                if not c_step then
                    break
                end
                if c_step_init == nil then
                    c_step_init = c_step
                else
                    if c_step_init == c_step then -- Prevent eternal loop.
                        break
                    end
                end
                c:swap(c_step)
                if c_step == my_unfocus then
                    break
                end
            end
        end
    end
end)
