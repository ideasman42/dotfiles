-------------------------------
--  "Zenburn" awesome theme  --
--    By Adrian C. (anrxc)   --
-------------------------------

-- Alternative icon sets and widget icons:
--  * http://awesome.naquadah.org/wiki/Nice_Icons

-- {{{ Main
theme = {}
-- theme.wallpaper = "/opt/awesome/share/awesome/themes/zenburn/zenburn-background.png"
-- }}}

-- {{{ Styles
theme.font      = "GohuFont 9"

-- {{{ Colors
theme.fg_normal  = "#DCDCCC"
theme.fg_focus   = "#F0DFAF"
theme.fg_urgent  = "#CC9393"
theme.bg_normal  = "#000000"
theme.bg_focus   = "#1E2320"
theme.bg_urgent  = "#3F3F3F"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.border_width  = 2
theme.border_normal = "#222222"
theme.border_focus  = "#658ABA"
theme.border_marked = "#CC9393"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#3F3F3F"
theme.titlebar_bg_normal = "#3F3F3F"
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
theme.taglist_bg_focus = "#888888"
theme.tasklist_bg_focus = "#404040"
theme.tasklist_bg_normal = "#000000"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = 15
theme.menu_width  = 400
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = "/opt/awesome/share/awesome/themes/zenburn/taglist/squarefz.png"
theme.taglist_squares_unsel = "/opt/awesome/share/awesome/themes/zenburn/taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = "/opt/awesome/share/awesome/themes/zenburn/awesome-icon.png"
theme.menu_submenu_icon      = "/opt/awesome/share/awesome/themes/default/submenu.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = "/opt/awesome/share/awesome/themes/zenburn/layouts/tile.png"
theme.layout_tileleft   = "/opt/awesome/share/awesome/themes/zenburn/layouts/tileleft.png"
theme.layout_tilebottom = "/opt/awesome/share/awesome/themes/zenburn/layouts/tilebottom.png"
theme.layout_tiletop    = "/opt/awesome/share/awesome/themes/zenburn/layouts/tiletop.png"
theme.layout_fairv      = "/opt/awesome/share/awesome/themes/zenburn/layouts/fairv.png"
theme.layout_fairh      = "/opt/awesome/share/awesome/themes/zenburn/layouts/fairh.png"
theme.layout_spiral     = "/opt/awesome/share/awesome/themes/zenburn/layouts/spiral.png"
theme.layout_dwindle    = "/opt/awesome/share/awesome/themes/zenburn/layouts/dwindle.png"
theme.layout_max        = "/opt/awesome/share/awesome/themes/zenburn/layouts/max.png"
theme.layout_fullscreen = "/opt/awesome/share/awesome/themes/zenburn/layouts/fullscreen.png"
theme.layout_magnifier  = "/opt/awesome/share/awesome/themes/zenburn/layouts/magnifier.png"
theme.layout_floating   = "/opt/awesome/share/awesome/themes/zenburn/layouts/floating.png"
theme.layout_cornernw   = "/opt/awesome/share/awesome/themes/zenburn/layouts/cornernw.png"
theme.layout_cornerne   = "/opt/awesome/share/awesome/themes/zenburn/layouts/cornerne.png"
theme.layout_cornersw   = "/opt/awesome/share/awesome/themes/zenburn/layouts/cornersw.png"
theme.layout_cornerse   = "/opt/awesome/share/awesome/themes/zenburn/layouts/cornerse.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/close_focus.png"
theme.titlebar_close_button_normal = "/opt/awesome/share/awesome/themes/zenburn/titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = "/opt/awesome/share/awesome/themes/zenburn/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = "/opt/awesome/share/awesome/themes/zenburn/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = "/opt/awesome/share/awesome/themes/zenburn/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = "/opt/awesome/share/awesome/themes/zenburn/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = "/opt/awesome/share/awesome/themes/zenburn/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = "/opt/awesome/share/awesome/themes/zenburn/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = "/opt/awesome/share/awesome/themes/zenburn/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = "/opt/awesome/share/awesome/themes/zenburn/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = "/opt/awesome/share/awesome/themes/zenburn/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
