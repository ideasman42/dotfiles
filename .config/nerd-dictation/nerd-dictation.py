# User configuration file typically located at `~/.config/nerd-dictation/nerd-dictation.py`
import re


# -----------------------------------------------------------------------------
# Replace Multiple Words

TEXT_REPLACE_REGEX = (
    # (" comma" "\\b", ","),
    # (" full stop" "\\b", "."),
    ("\\b" "data type" "\\b", "data-type"),
    ("\\b" "copy on write" "\\b", "copy-on-write"),
    ("\\b" "key word" "\\b", "keyword"),
    ("\\b" "in my humble opinion" "\\b", "IMHO"),
    # ("\\b" "begin bracket" "\\b", "("),
)
#
TEXT_REPLACE_REGEX = tuple(
    (re.compile(match), replacement)
    for (match, replacement) in TEXT_REPLACE_REGEX
)


# -----------------------------------------------------------------------------
# True Case

def truecase(text, only_proper_nouns=False, only_lower_cased=False):
    import nltk
    truecased_sents = []  # List of true-cased sentences.
    # Apply POS-tagging.
    tagged_sent = nltk.pos_tag([
        word.lower() for word in nltk.word_tokenize(text)
    ])
    # Infer capitalization from POS-tags.
    capitalize_tags = (
        {"NNP", "NNPS"} if only_proper_nouns else
        {"NN", "NNS"}
    )
    normalized_sent = [
        word.capitalize()
        if ((tag in capitalize_tags) and
            # If the word has upper case characters, leave it as-is.
            ((not only_lower_cased) or word.islower()))
        else word
        for (word, tag) in tagged_sent
    ]
    # Use regular expression to get punctuation right.
    pretty_string = re.sub(
        " (?=[\\.,'!?:;])",
        "",
        " ".join(normalized_sent)
    )
    return pretty_string


# -----------------------------------------------------------------------------
# Replace Single Words

WORD_CAPITALIZE = {
    "I",
    "API",
    "Linux",
    "Campbell",
}

# VOSK-API doesn't use capitals anywhere so they have to be explicit added in.
WORD_REPLACE = {
    # Common misunderstandings.
    "daughter": "data",
    **{w.lower(): w for w in WORD_CAPITALIZE}
}

# Regular expressions allow partial words to be replaced.
WORD_REPLACE_REGEX = (
    ("^i'(.*)", "I'\\1"),
    ("^optimisation([a-z]+)", "optimization\\1"),
)
WORD_REPLACE_REGEX = tuple(
    (re.compile(match), replacement)
    for (match, replacement) in WORD_REPLACE_REGEX
)


# -----------------------------------------------------------------------------
# Main Processing Function

def nerd_dictation_process(text):
    for match, replacement in TEXT_REPLACE_REGEX:
        text = match.sub(replacement, text)

    words = text.split(" ")

    for i, w in enumerate(words):
        w_init = w
        w_test = WORD_REPLACE.get(w)
        if w_test is not None:
            w = w_test

        if w_init == w:
            for match, replacement in WORD_REPLACE_REGEX:
                w_test = match.sub(replacement, w)
                if w_test != w:
                    w = w_test
                    break

        words[i] = w

    # Strip any words that were replaced with empty strings.
    words[:] = [w for w in words if w]

    text = " ".join(words)
    # text = truecase(text)
    return text
