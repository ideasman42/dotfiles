#!/usr/bin/env python

import subprocess
import json

RIVER_BEDLOAD = "/opt/river/bin/river-bedload"
RIVER_CTL = "/opt/river/bin/riverctl"
SCRATCHPAD_TAG = 1 << 20

# Extra actions to reduce flickering.
USE_FLICKER_REDUCE = False

def main():

    outputs = json.loads(subprocess.check_output([
        RIVER_BEDLOAD,
        "-minified",
        "-print",
        "outputs",
    ]))
    i_focused = 0
    for i, o in enumerate(outputs):
        if o["focused"]:
            i_focused = i
            break

    has_scratchpad = False
    for o in outputs:
        if o["focused_tags"] & SCRATCHPAD_TAG:
            has_scratchpad = True
            break

    use_scratchpad = not has_scratchpad

    if USE_FLICKER_REDUCE:
        subprocess.check_output([RIVER_CTL, "set-cursor-warp", "disabled"])
        subprocess.check_output([RIVER_CTL, "border-color-focused", "0x101010"])

    for n in range(len(outputs)):
        o = outputs[(n + i_focused) % len(outputs)]

        is_scratchpad = (o["focused_tags"] & SCRATCHPAD_TAG) != 0
        if is_scratchpad != use_scratchpad:
            subprocess.check_output([RIVER_CTL, "toggle-focused-tags", "{:d}".format(SCRATCHPAD_TAG)])

        if len(outputs) > 1:
            subprocess.check_output([RIVER_CTL, "focus-output", "next"])

    subprocess.check_output([
        RIVER_CTL,
        "border-color-focused",
        "0xFF0000" if use_scratchpad else "0x658aba",
    ])

    subprocess.check_output([
        RIVER_CTL,
        "background-color",
        "0x440000" if use_scratchpad else "0x000000",
    ])

    if USE_FLICKER_REDUCE:
        subprocess.check_output([RIVER_CTL, "set-cursor-warp", "on-focus-change"])


if __name__ == "__main__":
    main()
