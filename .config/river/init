#!/bin/sh

# Disable title bar (only run once!)
# gsettings set org.gnome.desktop.wm.preferences button-layout ""


# -----------------------------------------------------------------------------
# Non-River Commands

#killall dbus-daemon
#dbus-daemon --fork --session

USE_MAKO=1

if [ "$USE_MAKO" -eq "1" ] ; then
  mako &
else
  dunst &
fi

# My own extended version of the spiral generating tile layout.
USE_CUSTOM_SPIRAL=1
USE_CUSTOM_SCRATCHPAD=1

USE_X11=0

# -----------------------------------------------------------------------------
# Hardware

# wlr-randr --output DP-1 --transform 90 --pos 0,0 --output DP-2 --transform 270  --pos 2160,0
wlr-randr-default 1.0


# -----------------------------------------------------------------------------
# Environment

if [ -d "/opt/river/bin" ]; then
  export PATH=$PATH:/opt/river/bin
  RIVERCTL='/opt/river/bin/riverctl'
else
  RIVERCTL='riverctl'
fi

# TODO: not on NIX yet.
RIVERWM_UTILS_CYCLE_TAGS="/opt/river/bin/cycle-focused-tags"

# -----------------------------------------------------------------------------
# X11

if [ "$USE_X11" -eq "1" ] ; then
  xrdb -load ~/.Xresources/*
fi

# -----------------------------------------------------------------------------
# Theme / Colors

riverctl background-color 0x000000
riverctl border-width 3
riverctl border-color-focused 0x658aba # Blue.
# riverctl border-color-focused 0xFF0000 # Red.
riverctl border-color-unfocused 0x101010
riverctl border-color-urgent 0xFFFF00

riverctl xcursor-theme Adwaita 48

# Always use SSD, otherwise firefox isn't obviously active/inactive.
riverctl rule-add ssd

# -----------------------------------------------------------------------------
# Window Manager Configuration

# Use the "logo" key as the primary modifier
mod="Mod4"

# Set repeat rate
riverctl set-repeat 40 200
riverctl focus-follows-cursor always
# riverctl focus-follows-cursor disabled
riverctl set-cursor-warp on-focus-change

# Set the layout on startup
riverctl layout rivertile left
riverctl default-layout rivertile

# Set app-ids of views which should float
riverctl rule-add -app-id 'float*' -title 'popup' float
riverctl rule-add -app-id 'float*' -title 'float' float

# Set app-ids of views which should use client side decorations
# riverctl csd-filter-add "gedit"

# Float some windows.
# riverctl float-filter-add "Rofi"

# -----------------------------------------------------------------------------
# Layout Keys

# Mod+X to toggle float
riverctl map normal $mod X toggle-float
# Mod+F to toggle fullscreen
riverctl map normal $mod F toggle-fullscreen

if [ "$USE_CUSTOM_SPIRAL" -eq "1" ] ; then
  riverctl map normal $mod+Control H send-layout-cmd spiral "main-location left"
  riverctl map normal $mod+Control J send-layout-cmd spiral "main-location bottom"
  riverctl map normal $mod+Control K send-layout-cmd spiral "main-location top"
  riverctl map normal $mod+Control L send-layout-cmd spiral "main-location right"
else
  riverctl map normal $mod+Control H send-layout-cmd rivertile "main-location left"
  riverctl map normal $mod+Control J send-layout-cmd rivertile "main-location bottom"
  riverctl map normal $mod+Control K send-layout-cmd rivertile "main-location top"
  riverctl map normal $mod+Control L send-layout-cmd rivertile "main-location right"
fi

# riverctl map normal $mod minus send-layout-cmd rivertile "main-ratio -0.1"
# riverctl map normal $mod equal send-layout-cmd rivertile "main-ratio +0.1"

riverctl map normal $mod minus spawn 'wlr-randr-default -0.25'
riverctl map normal $mod equal spawn 'wlr-randr-default +0.25'
riverctl map normal $mod backspace spawn 'wlr-randr-default round'

# -----------------------------------------------------------------------------
# Window Management Keys

# Mod+Q to close the focused view
riverctl map normal $mod Q close

# Mod+E to exit river
riverctl map normal $mod+Shift Q exit

# Mod+J and Mod+K to focus the next/previous view in the layout stack
riverctl map normal $mod J focus-view next
riverctl map normal $mod K focus-view previous

# Alt-Tab.
riverctl map normal Mod1 TAB focus-view next
riverctl map normal Mod1+Shift TAB focus-view previous

# Mod+Shift+J and Mod+Shift+K to swap the focused view with the next/previous
# view in the layout stack
riverctl map normal $mod+Shift J swap next
riverctl map normal $mod+Shift K swap previous

# Mod+Period and Mod+Comma to focus the next/previous output
riverctl map normal $mod H focus-output next
riverctl map normal $mod L focus-output previous

# Mod+Shift+{Period,Comma} to send the focused view to the next/previous output
# riverctl map normal $mod Z send-to-output next
riverctl map normal $mod Z spawn "bash -c \"\
$RIVERCTL send-to-output -current-tags next && \
$RIVERCTL focus-output next\""

# riverctl map normal $mod+Shift Comma send-to-output previous

# Mod+Return to bump the focused view to the top of the layout stack
riverctl map normal $mod+Shift Return zoom

# Mod+H and Mod+L to decrease/increase the main factor by 5%
# If using rivertile(1) this determines the width of the main stack.
# riverctl map normal $mod H mod-main-factor -0.05
# riverctl map normal $mod L mod-main-factor +0.05

# Mod+Shift+H and Mod+Shift+L to increment/decrement the number of
# main views in the layout
riverctl map normal $mod+Shift H mod-main-count +1
riverctl map normal $mod+Shift L mod-main-count -1

# Mod+Alt+{H,J,K,L} to move views
riverctl map normal $mod+Mod1 H move left 100
riverctl map normal $mod+Mod1 J move down 100
riverctl map normal $mod+Mod1 K move up 100
riverctl map normal $mod+Mod1 L move right 100

# Mod+Alt+Control+{H,J,K,L} to snap views to screen edges
riverctl map normal $mod+Mod1+Control H snap left
riverctl map normal $mod+Mod1+Control J snap down
riverctl map normal $mod+Mod1+Control K snap up
riverctl map normal $mod+Mod1+Control L snap right

# Mod+Alt+Shif+{H,J,K,L} to resize views
riverctl map normal $mod+Mod1+Shift H resize horizontal -100
riverctl map normal $mod+Mod1+Shift J resize vertical 100
riverctl map normal $mod+Mod1+Shift K resize vertical -100
riverctl map normal $mod+Mod1+Shift L resize horizontal 100

# Mod + Left Mouse Button to move views
riverctl map-pointer normal $mod BTN_LEFT move-view

# Mod + Right Mouse Button to resize views
riverctl map-pointer normal $mod BTN_RIGHT resize-view

WORKSPACE_COUNT=4

for i in $(seq 1 $WORKSPACE_COUNT)
do
    tags=$((1 << ($i - 1)))

    # Mod+[1-9] to focus tag [0-8]
    riverctl map normal $mod $i set-focused-tags $tags

    # Mod+Alt+[1-9] to tag focused view with tag [0-8]
    riverctl map normal $mod+Control $i set-view-tags $tags

    # Mod+Ctrl+[1-9] to toggle focus of tag [0-8]
    riverctl map normal $mod+Mod1 $i toggle-focused-tags $tags

    # Mod+Alt+Ctrl+[1-9] to toggle tag [0-8] of focused view
    riverctl map normal $mod+Mod1+Control $i toggle-view-tags $tags
done

# Mod+0 to focus all tags
# Mod+Shift+0 to tag focused view with all tags
all_tags=$(((1 << 32) - 1))
riverctl map normal $mod 0 set-focused-tags $all_tags
riverctl map normal $mod+Mod1 0 set-view-tags $all_tags

# Mod+{Up,Right,Down,Left} to change layout orientation
riverctl map normal $mod Up layout rivertile top
riverctl map normal $mod Right layout rivertile right
riverctl map normal $mod Down layout rivertile bottom
riverctl map normal $mod Left layout rivertile left

# Mod+S to change to Full layout
riverctl map normal $mod S layout full

# Declare a passthrough mode. This mode has only a single mapping to return to
# normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

# Mod+F11 to enter passthrough mode
riverctl map normal $mod F11 enter-mode passthrough

# Mod+F11 to return to normal mode
riverctl map passthrough $mod F11 enter-mode normal

if [ "$USE_MAKO" -eq "1" ] ; then
  riverctl map normal $mod Escape spawn "makoctl dismiss"
fi

# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked
do
    # Eject the optical drive
    riverctl map $mode None XF86Eject spawn eject -T
done

# Set opacity and fade effect
# riverctl opacity 1.0 0.75 0.0 0.1 20


# -----------------------------------------------------------------------------
# Scratch Pad
# See: https://github.com/riverwm/river/wiki/Scratchpad-and-Sticky

# The scratchpad will live on an unused tag. Which tags are used depends on your
# config, but rivers default uses the first 9 tags.
scratch_tag=$((1 << 20))

if [ "$USE_CUSTOM_SCRATCHPAD" -eq "1" ] ; then
  # Toggle the scratchpad with Super+Shift+W
  riverctl map normal $mod+Shift W spawn ~/.config/river/bin/riverwm_scratchpad_all
  riverctl map normal $mod SPACE   spawn ~/.config/river/bin/riverwm_scratchpad_all_exclusive

  # Send windows to the scratchpad with Super+W
  riverctl map normal $mod W spawn ~/.config/river/bin/riverwm_scratchpad_send
else
  # Toggle the scratchpad with Super+Shift+W
  riverctl map normal $mod+Shift W toggle-focused-tags ${scratch_tag}

  # Send windows to the scratchpad with Super+W
  riverctl map normal $mod W set-view-tags ${scratch_tag}
fi



# Set spawn tagmask to ensure new windows don't have the scratchpad tag unless
# explicitly set.
all_but_scratch_tag=$((((1 << 32) - 1) ^ $scratch_tag))
riverctl spawn-tagmask ${all_but_scratch_tag}

# -----------------------------------------------------------------------------
# Cycle

riverctl map normal $mod+Control Return spawn "bash -c \"$RIVERWM_UTILS_CYCLE_TAGS --all-outputs --skip-occupied +1 $WORKSPACE_COUNT\""

riverctl map normal $mod M spawn     "bash -c \"$RIVERWM_UTILS_CYCLE_TAGS --all-outputs --skip-empty +1 $WORKSPACE_COUNT\""
riverctl map normal $mod COMMA spawn "bash -c \"$RIVERWM_UTILS_CYCLE_TAGS --all-outputs --skip-empty -1 $WORKSPACE_COUNT\""

riverctl map normal $mod+Shift M spawn     "bash -c \"$RIVERWM_UTILS_CYCLE_TAGS --all-outputs --follow +1 $WORKSPACE_COUNT\""
riverctl map normal $mod+Shift COMMA spawn "bash -c \"$RIVERWM_UTILS_CYCLE_TAGS --all-outputs --follow -1 $WORKSPACE_COUNT\""


# -----------------------------------------------------------------------------
# App/Menu Launcher Keys

riverctl map normal $mod P spawn dmenu_custom
riverctl map normal $mod+Control P spawn dmenu_custom_system

riverctl map normal $mod F10 spawn xosd_time

# Mod+Shift+Return to start an instance of foot (https://codeberg.org/dnkl/foot)
# Use foot with my own defaults (tmux).
riverctl map normal $mod Return spawn foot-default


# -----------------------------------------------------------------------------
# Extra Launcher Keys

# Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
riverctl map normal None XF86AudioRaiseVolume  spawn 'i42-audio-volume up'
riverctl map normal None XF86AudioLowerVolume  spawn 'i42-audio-volume down'

riverctl map normal None XF86AudioPlay    spawn "i42-audio-media pause"
riverctl map normal None XF86AudioPrev    spawn "i42-audio-media prev"
riverctl map normal None XF86AudioNext    spawn "i42-audio-media next"
riverctl map normal None XF86AudioRewind  spawn "i42-audio-media seek_prev"
riverctl map normal None XF86AudioForward spawn "i42-audio-media seek_next"

riverctl map normal None XF86Launch8  spawn "nerd-dictation-resume"
riverctl map normal None XF86Launch9  spawn "nerd-dictation-suspend"


# -----------------------------------------------------------------------------
# Setup Tiling

if [ "$USE_CUSTOM_SPIRAL" -eq "1" ] ; then
  riverctl default-layout spiral
  env PATH="/scratch/src/river_python_venv/bin:$PATH" riverwm_spiral_extended
else
  riverctl default-layout rivertile left
  rivertile -outer-padding 1 -view-padding 0 -main-location left -main-ratio 0.5
fi
