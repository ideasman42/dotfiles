#!/usr/bin/env python3
import collections
import os

menu = collections.OrderedDict()

_term_font = os.environ.get(
    "MY_MONO_FONT",
    "Source Code Pro:pixelsize=14",
)

def is_wayland():
    import os
    return os.environ.get("XDG_SESSION_TYPE", "") == "wayland"


def term(title="Term", cmd="", pause=False):
    """
    Wrapper to tun terminal commands.
    """
    if pause:
        cmd = cmd + " && echo -e '\nDone, press key to exit' && read"

    if is_wayland():
        cmd_final = (
            f"LANG=en_US.UTF-8 "
            f"foot 2>/dev/null "
            f"-f \"{_term_font}\" "
            f"-T {title!r} "
            f"-e bash -c {cmd!r}"
        )
    else:
        cmd_final = (
            f"LANG=en_US.UTF-8 "
            f"st 2>/dev/null "
            f"-f \"{_term_font}\" "
            f"-t {title!r} "
            f"-e bash -c {cmd!r}"
        )
    return cmd_final


def term_session(title="Term", cmd="", session="default"):
    """
    Wrapper to tun terminal commands, using a named tmux session.
    """
    if False:
        return (
            f"LANG=en_US.UTF-8 "
            f"urxvt -sl 0 +sb -title {title!r} -e "
            f"tmux new-session -A -s {session} "
            f"{cmd!r} \; set status off"
        )

    if is_wayland():
        cmd_final = (
            f"LANG=en_US.UTF-8 "
            f"foot 2>/dev/null "
            f"-f \"{_term_font}\" "
            f"-T {title!r} -e "
            f"tmux new-session -A -s {session} "
            f"{cmd!r} \; set status off"
        )
    else:
        cmd_final = (
            f"LANG=en_US.UTF-8 "
            f"st 2>/dev/null "
            f"-f \"{_term_font}\" "
            f"-t {title!r} -e "
            f"tmux new-session -A -s {session} "
            f"{cmd!r} \; set status off"
        )
    return cmd_final


def menu_item_term():
    if is_wayland():
        return "foot-default"
    return "st-default"


def menu_item_term():
    if is_wayland():
        return "foot-default"
    return "st-default"


menu["term"] = lambda: (
    "foot-default" if is_wayland() else
    "st-default"
)
menu["term (no TMUX)"] = lambda: (
    "env LANG=C.UTF-8 foot -f \"$MY_MONO_FONT\"" if is_wayland() else
    "env LANG=C.UTF-8 st -f \"$MY_MONO_FONT\""
)

# menu["file"] = "worker"
menu["file"] = "nautilus"

menu["edit"] = "EMACS_USE_SERVER=1 emacs"
menu["note"] = "gnote"
menu["ide-qtcreator"] = (
    "PATH=$PATH:$HOME/.config/QtProject/qtcreator/externaltools "
    "qtcreator default",
)
# menu["ide-netbeans"] = "netbeans.sh"
# menu["ide-eclipse"] = "eclipse.sh"

menu["meld"] = "meld"

menu["web"] = "GDK_DPI_SCALE=1.75 firefox" + " 2> /dev/null"
menu["news"] = "liferea"
menu["chat (blender)"] = (
    'firefox '
    '--kiosk '
    '--no-remote '
    '--new-instance '
    '-P blender.chat '
    'https://chat.blender.org'
)

# menu["chat (irc)"] = term_session(title="Chat", cmd="weechat", session="irc")

# menu["email"] = "evolution"
menu["email"] = "GDK_DPI_SCALE=1.5 thunderbird"
# menu["email-cmd"] = term_session(title="EMail", cmd="neomutt", session="email")

menu["torrent"] = "transmission-gtk"
menu["ftp"] = "filezilla"
# editors
menu["edit-image"] = "gimp"
menu["edit-audio"] = "audacity"

# menu["music"] = "audacious"
menu["music"] = "foot-unique-session music dbus-launch mpv --no-video --idle=yes"
# menu["jukebox"] = "rhythmbox"
menu["jukebox"] = "audacious"
menu["podcast"] = "LANG=en_US.UTF-8 GDK_DPI_SCALE=1.5 gpodder"
menu["youtube from clipboard"] = "youtube-dl-as-podcast"
menu["tts from clipboard (selection)"] = "tts_clipboard --primary"
menu["tts from clipboard (clipboard)"] = "tts_clipboard"
menu["mixer"] = "pavucontrol"

menu["podcast_sync"] = term(title='Podcast/Sync', cmd='podcast_sync', pause=True)

menu["usb-mount"] = "udisksctl_usb_all --mount"
menu["usb-eject"] = "udisksctl_usb_all --unmount"
menu["usb-power-off"] = "udisksctl_usb_all --unmount-power-off"

if is_wayland():
    menu["screen-shot region"] = "wl-screenshot.sh"


menu["scratch-buffer"] = "EMACS_NO_SERVER=1 emacs ~/.personal/scratch.org"
