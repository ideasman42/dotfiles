#!/usr/bin/env python3
import collections
import os
menu = collections.OrderedDict()

# _term_font = "Source Code Pro:pixelsize=14"  # vertical HD monitors
# _term_font = "Source Code Pro:pixelsize=20"  # 2x vertical 4k monitors
# _term_font = "IBM Plex Mono:pixelsize=18"
_term_font = os.environ.get("MY_MONO_FONT", "monospaced")

# menu["!wm_cfg"] = "gvim ~/.spectrwm.conf"
menu["wm_cfg"] = "ed-wm.py"
menu["menu"] = "emacs ~/.config/dmenu/custom.py ~/.config/dmenu/custom_system.py"

menu["wm_exit"] = "openbox --exit"
menu["suspend"] = "foot -f \"" + _term_font + "\" backup_daily.sh; systemctl suspend"
menu["shutdown"] = "foot -f \"" + _term_font + "\" backup_daily.sh; shutdown now"
menu["reboot"] = "reboot"
menu["up"] = "up.sh"
menu["system-monitor"] = "gnome-system-monitor"
menu["install_all"] = "foot -f \"" + _term_font + "\" inst_all"
menu["first"] = "LANG=en_US.UTF-8 gpodder & firefox & liferea & evolution"
