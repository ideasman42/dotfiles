# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{
  config,
  lib,
  pkgs,
  ...
}:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  # Don't fill up the `/boot` partition with kernels.
  boot.loader.systemd-boot.configurationLimit = 5;

  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelParams = [
    "splash"
    "fbcon=rotate:3"
  ];

  boot.blacklistedKernelModules = [
    # Avoid annoying additional output devices.
    "snd_hda_codec_hdmi"
  ];

  # Clear `/tmp` on startup.
  boot.tmp.cleanOnBoot = true;

  networking.hostName = "nixos"; # Define your `hostname`.
  # Pick only one of the below networking options.
  networking.wireless.enable = false; # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = false; # Easiest to use and most distros use this by default.

  networking.extraHosts = ''
    192.168.0.15 arch.local
  '';

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Set your time zone.
  time.timeZone = "Australia/Melbourne";

  # Select internationalization properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
    useXkbConfig = false; # use xkb.options in tty.
  };

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Auto-login for convenience.
  services.getty.autologinUser = "ideasman42";

  services.udev.extraHwdb = ''
    # Swap middle & extra buttons for: Evoluent Evoluent VerticalMouse D
    evdev:input:b0003v1A7Cp0197e0111*
    # The ? button.
     KEYBOARD_KEY_90001=btn_extra
    # The ? button.
     KEYBOARD_KEY_90002=btn_side
    # The middle (non wheel) button.
     KEYBOARD_KEY_90003=btn_right
    # The lower thumb button.
     KEYBOARD_KEY_90004=btn_middle
    # The ? button.
     KEYBOARD_KEY_90005=btn_extra
    # The upper thumb button.
     KEYBOARD_KEY_90006=btn_left
  '';

  # Configure keymap in X11
  # services.xserver.xkb.layout = "us";
  # services.xserver.xkb.options = "eurosign:e,caps:escape";

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # hardware.pulseaudio.enable = true;
  # OR
  services.pipewire = {
    enable = true;
    pulse.enable = true;
  };

  # For nautilus & others to list USB devices.
  services.gvfs.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.ideasman42 = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.fish;
    packages = [

      # NIX specific packages.
      pkgs.nixfmt-rfc-style

      # Desktop (internet).
      pkgs.firefox
      pkgs.thunderbird
      pkgs.transmission_4-gtk
      pkgs.links2
      # Command line.
      pkgs.rsync
      pkgs.wget

      # Desktop utilities.
      pkgs.evince # PDF viewer.
      pkgs.gnome-disk-utility
      pkgs.gnome-system-monitor
      pkgs.libnotify # For `notify-send`.
      pkgs.nautilus # File manager.
      pkgs.xdg-utils # For `xdg-open`.

      # Desktop image.
      pkgs.geeqie

      # Desktop video.
      pkgs.mpv # Media player.
      # Command line.
      pkgs.yt-dlp
      pkgs.ffmpeg

      # Desktop audio.
      pkgs.audacious # GUI player.
      pkgs.audacity # Audio editor.
      pkgs.pavucontrol
      pkgs.pulseaudio # Command line `pactl` to change volume.
      # Command line audio.
      pkgs.cmus

      # Desktop themes.
      # Without these icons are *very* basic (& ugly) for nautilus.
      pkgs.adwaita-icon-theme

      # Banking.
      pkgs.hledger
      # `hledger-flow` (TODO: not packaged).

      # System facilities.
      pkgs.sshfs # Allow mounting SSH file-systems.

      # WAYLAND.
      pkgs.foot
      pkgs.river
      pkgs.seatd
      # sway
      # `tofi` # Opens on wrong monitor w/ RIVERWM, sigh.
      pkgs.wofi # Arrow key behavior isn't so nice, otherwise works well.

      pkgs.wayland
      pkgs.wayland-protocols
      pkgs.wayland-scanner
      pkgs.waypipe # Handy to run WAYLAND over SSH.
      pkgs.wlr-randr
      pkgs.xwayland

      pkgs.grim # screenshot functionality.
      pkgs.slurp # screenshot functionality

      pkgs.mako # Notifications.
      pkgs.wl-clipboard # Copy/paste.

      # Command line environment.
      pkgs.dash # A fast, low memory alternative shell.
      pkgs.fish # Main shell.

      # Command line utilities.
      pkgs.aha # ANSI to HTML.
      pkgs.atool
      pkgs.file # Detect file types.
      pkgs.fzf
      pkgs.inotify-tools # For `inotifywait` (used by own scripts to detect changes).
      pkgs.jq # For extracting info out of JSON expression (use for interacting w/ `nitropy`).
      pkgs.openssl # For encrypting/decrypting secrets.
      pkgs.pcre # Useful more feature rich GREP (`pcregrep`).
      pkgs.psmisc # For `killall` and other utilities.
      pkgs.pynitrokey # For `nitropy` command line NITROKEY access.
      pkgs.ripgrep
      pkgs.tmux

      # Compression.
      pkgs.p7zip
      pkgs.unzip

      # Hardware:

      # `cpupower frequency-set --governor powersave` and similar commands.
      pkgs.linuxKernel.packages.linux_hardened.cpupower

      # Support for mounting USB drives.
      pkgs.udisks
      pkgs.zenity # `udisks` uses to send messages.

      # Development (generic).
      pkgs.delta
      pkgs.cgdb
      pkgs.gdb
      pkgs.git
      pkgs.git-lfs
      pkgs.meld
      pkgs.mercurial
      pkgs.tig

      # Development (build systems)
      pkgs.cmake
      pkgs.gnumake
      pkgs.meson
      pkgs.ninja
      pkgs.pkg-config

      # Development Tool Chains (C/C++)
      pkgs.ccache
      pkgs.gcc14
      pkgs.mold
      # Development Utilities (C/C++)
      pkgs.doxygen
      # Development User-land (C/C++)
      pkgs.qtcreator
      # pkgs.clang-tools # `clangd` (for EMACS EGLOT.)
      # Checkers.
      pkgs.cppcheck

      # Development (Python).
      pkgs.geckodriver
      pkgs.ruff

      (pkgs.python312.withPackages (python-pkgs: [
        # select Python packages here
        python-pkgs.autopep8
        python-pkgs.pyenchant # For checking spelling.
        python-pkgs.selenium
        python-pkgs.pip
        python-pkgs.python-lsp-server # `pylsp` for EMACS, EGLOT.
        python-pkgs.sphinx
        python-pkgs.virtualenv
        python-pkgs.wheel # For `bdist_wheel` for Blender extensions tests.

        # Checkers.
        python-pkgs.pylint
        python-pkgs.mypy
      ]))

      # Development (ZIG).
      pkgs.zig

      # Development User-land (generic).
      pkgs.emacs29-pgtk

      # NOTE: `aspell` needs special handling.
      # aspell # Needed by emacs for spelling.
      # aspellDicts.en
      (pkgs.aspellWithDicts (dicts: with dicts; [ en ]))
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable `nix-ld`.
  programs.nix-ld.enable = true;
  # Sets up all the libraries to load
  programs.nix-ld.libraries = with pkgs; [
    stdenv.cc.cc
    zlib
    # ...
  ];
  # End `nix.hl`

  # List services that you want to enable:

  programs.fish.enable = true;

  # Setup facilities for RIVERWM.
  programs.river.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?

}
