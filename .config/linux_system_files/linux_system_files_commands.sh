#!/bin/bash

set -x

# Setup auto-login:
mkdir -p /etc/systemd/system/getty@tty1.service.d

cat > /etc/systemd/system/getty@tty1.service.d/override.conf<< EOF
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --autologin ideasman42 %I $TERM
# Once all other actions finish.
Type=idle
EOF

# Don't show GTK buttons in WAYLAND.
gsettings set org.gnome.desktop.wm.preferences button-layout ""

# Keyboard repeat rate for WAYLAND.
gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 40
gsettings set org.gnome.desktop.peripherals.keyboard delay 200

# For some reason KDE messes with GTK defaults.
gsettings set org.gnome.desktop.interface cursor-theme Adwaita
gsettings set org.gnome.desktop.interface cursor-size 48

# Default fonts.
# Nice fonts include "Dosis", "Ubuntu Condensed", "Exo 2", "Dosis 14"
gsettings set org.gnome.desktop.interface monospace-font-name 'Berkley Mono 12'
gsettings set org.gnome.desktop.interface document-font-name 'Souvenir 12'
gsettings set org.gnome.desktop.interface font-name 'Souvenir 12'


# So the monitor is properly rotated on startup.
sudo systemctl enable i42-on-wake-for-wayland

# Smart-card (NITROKEY setup).
# requires packages `ccid` and `opensc`.
# TODO: may want to enable later?
# sudo systemctl enable pcscd

sudo systemctl daemon-reload

# Setup ssh-agent.
systemctl --user enable ssh-agent.service
systemctl --user start ssh-agent.service

# Ensure `dbus-daemon` is running.
systemctl --user enable dbus.service
systemctl --user start dbus.service

# Allow access to /dev/ttyACM0 for keyboardio
# stat /dev/ttyACM0 points to Gid: uucp
sudo usermod -a -G uucp ideasman42

# Allow generation of `input` device commands, needed for `dotool`.
sudo usermod -a -G input ideasman42
