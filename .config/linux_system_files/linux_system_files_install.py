# SPDX-License-Identifier: GPL-2.0-or-later

import os
import shutil
import filecmp

from typing import Generator

BASEDIR = os.path.abspath(os.path.dirname(__file__))

FILES_EXCLUDE = {
    "/Makefile",
    "/etc/udev/hwdb.d/readme.rst",
    "/linux_system_files_install.py",
    "/linux_system_files_commands.sh",
}

# Exclude filenames with this prefix.
FILES_EXCLUDE_PREFIX = (
    ".#",
)


def files_recursive(path: str) -> Generator[str, None, None]:
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if filename.startswith(FILES_EXCLUDE_PREFIX):
                continue
            yield os.path.join(dirpath, filename)


def main() -> None:
    print("Installing files from", BASEDIR)
    count_files_updated = 0
    for f_abs_src in sorted(files_recursive(BASEDIR)):
        f_abs_dst = "/" + os.path.relpath(f_abs_src, BASEDIR)
        if f_abs_dst in FILES_EXCLUDE:
            continue
        print(f_abs_dst, end="... ")
        f_abs_dst_dir = os.path.dirname(f_abs_dst)
        os.makedirs(f_abs_dst_dir, exist_ok=True)
        if os.path.exists(f_abs_dst) and filecmp.cmp(f_abs_src, f_abs_dst, shallow=True):
            print("same!")
        else:
            print("updating!")
            shutil.copy2(f_abs_src, f_abs_dst)
            count_files_updated += 1
    print("Updated", count_files_updated, "file(s)")


if __name__ == "__main__":
    main()
