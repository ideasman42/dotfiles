#!/bin/bash
if [ "${1}" == "post" ]; then
    wlr-randr --output DP-1 --transform 270 --pos 2160,0 \
              --output DP-2 --transform 90  --pos 0,0
fi
