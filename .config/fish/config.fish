
# -------------
# Fish Specific

# no greeting
set -U fish_greeting ""

# no PWD shortening
set -U fish_prompt_pwd_dir_length 0

export SVN_EDITOR="gvim --nofork"
export BZR_EDITOR="gvim --nofork"
export EDITOR="gvim --nofork"

if status is-interactive
    set fish_color_autosuggestion 444444 red

    if [ "$TERM" = "eterm-color" ]
        # pass
    else
        function fish_title
            if [ $_ = 'fish' ]
                echo "["(prompt_pwd)"]"
            else
                echo "("$_")"
            end
        end
    end

    abbr -a -g g git
    # abbr -a -g b /src/blender/blender.bin
    # abbr -a -g b gdb /src/blender/blender.bin -ex=r --args /src/blender/blender.bin
    abbr -a -g b cgdb -- /src/blender/blender.bin -ex=r --args /src/blender/blender.bin
end

function lk
  set loc (walk $argv); and cd $loc;
end

function fish_user_key_bindings

    # Unbind keys.
    bind --erase --key \cV # Ctrl-V (let TMUX handle this).

    # vim style hjkl navigation
    bind $argv \ck up-or-search
    bind $argv \cj down-or-search

    # Ctrl Backspace/Delete
    bind $argv \e\[3\;5~ kill-bigword
    bind $argv \cH backward-kill-bigword

    # Ctrl Left/Right steps 'bigword'
    bind $argv \e\[1\;5C forward-bigword
    bind $argv \e\[1\;5D backward-bigword

    # Ctrl-c, refresh
    bind \cc 'echo; commandline | cat; commandline ""; commandline -f repaint'

    # History search.
    bind $argv \cR \
        "commandline -i (
            begin;
                history;
                cat ~/.config/any_shell/history_permanent;
            end | fzf --no-sort --tiebreak=end --exact +i
        ) 2>/dev/null"

    # File search.
    # note: printf avoids './' prefix
    bind $argv \cF \
        "commandline -i (
            command find . -not -path '*/\.*' -not -path \"./*__pycache__/*\" -type f -printf '%P\n' |
            fzf --tiebreak=length --exact +i
        ) 2>/dev/null"

    # Ctrl-N: Complete based on the tmux buffer content.
    bind \cn "commandline -i (fzf-complete-from-tmux.sh) 2>/dev/null"

    # Ctrl-S: Switch git branch.
    bind \cs "git_checkout_branch_interactive.sh; echo ''; commandline -f repaint"
end

function fish_prompt
    set -l prompt_status "$status"
    echo -n (tput setab 234)

    # Use rev-parse to detect if we're in a git repository,
    # if so get the branch name with no error handling.
    set -l git_branch (git -C "." rev-parse 2>/dev/null && git branch --show-current)

    # This may be set by any of my own tools (currently only `nix-shell`).
    if set -q I42_PS_PREFIX
        echo -n $I42_PS_PREFIX
    end

    # echo -n (set_color cyan)(prompt_pwd)(set_color normal)
    # Nicer, bold slashes.
    echo -n \
        (set_color cyan) \
        (prompt_pwd | string replace -a '/' (set_color white)'/'(set_color cyan))' '

    if [ -n "$git_branch" ]
        echo -n (set_color green)"($git_branch) "(set_color normal)
    end
    if [ "$prompt_status" != "0" ]
        echo -n "<$prompt_status>"
    end
    echo -n -e '\n'(set_color normal)' > '
end

source ~/.config/any_shell/env.sh
source ~/.config/any_shell/aliases.sh

# Not in version control
source ~/.config/any_shell/aliases.local.sh

# Port these to general scripts
alias C='cd /src/cmake_debug'
alias CR='cd /src/cmake_release'
alias A='cd /src/blender/scripts/addons_core'
# alias b='/src/blender/blender.bin'
# alias b='gdb ./blender.bin -ex=r --args ./blender.bin'
# alias b='cgdb -- /src/blender/blender.bin -ex=r --args /src/blender/blender.bin'
alias dc='ccmake -S /src/blender -B /src/cmake_debug'
alias M='cd /src/manual'

# Start X at login
if status is-login
    if test -e ~/.xinitrc_session
        # X11.
        if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
            exec startx -- -quiet
        end
    else
        # Wayland/River.
        if test -z "$WAYLAND_DISPLAY" -a "$XDG_VTNR" = 1

            # River.
            # No hardware cursors (for NVIDIA only)!
            # env WLR_NO_HARDWARE_CURSORS=1 dbus-launch /opt/river/bin/river

            if test -f /opt/river/bin/river
                env LD_LIBRARY_PATH=/opt/wlroots_17/lib dbus-launch /opt/river/bin/river
            else
                river
            end

            # Hyprland.
            # dbus-launch /opt/hyprland/bin/Hyprland

            # GNOME.
            # MOZ_ENABLE_WAYLAND=1 QT_QPA_PLATFORM=wayland XDG_SESSION_TYPE=wayland exec dbus-run-session gnome-session
        end
    end
end
