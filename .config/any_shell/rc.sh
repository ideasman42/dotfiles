#!/bin/sh
# use for bash and zsh

tabs -4

# Disable ctrl-s and ctrl-q.
stty -ixon

# Exports
#

function find_blender_build() {
	if [ -e CMakeCache.txt ] && [ ! -d '.git' ] ; then
		echo $PWD
	elif [ -L ./blender.bin ] ; then
		echo $(dirname $(dirname $(readlink blender.bin)))
	else
		TEMP_RET=$(locate_dominating_directory.sh '.git')
		if [ -z "$TEMP_RET" ]; then
			echo $BDIR_BUILD
		else
			echo $(dirname $(dirname $TEMP_RET))
		fi
		unset TEMP_RET
	fi
}

function find_blender_source() {
	if [ -e CMakeCache.txt ] && [ ! -d '.git' ] ; then
		echo $(grep CMAKE_HOME_DIRECTORY CMakeCache.txt | cut -d"=" -f2)
	elif [ -L ./blender.bin ] ; then
		echo $PWD
	else
		TEMP_RET=$(locate_dominating_directory.sh '.git')
		if [ -z "$TEMP_RET" ]; then
			echo $BDIR
    else
        echo $TEMP_RET
		fi
    unset TEMP_RET
	fi
}

function d() {

	if [ -e Cargo.toml ] ; then
		cargo-oos build
		return
	fi

	DST="$(find_blender_build)"
	echo "Building in: $DST"
	if [ -e $DST/build.ninja ] ; then

		# support colors!
		mtime_before=$(stat -c "%y" $DST/build.ninja)

		GCC_COLORS=1 bash -c "cd $DST ; pump ninja -j9"

		# support color (again)
		mtime_after=$(stat -c "%y" $DST/build.ninja)
		if [ "$mtime_before" == "$mtime_after" ]; then
			bash -c "cd $DST ; cmake_ninja_color.py"
		fi

	else
		GCC_COLORS=1 bash -c "cd $DST ; make -j`nproc`"
	fi
	unset DST
}
function b() {
	if [ -e Cargo.toml ] ; then
		cargo-oos run
		return
	fi

	SRC="$(find_blender_source)"
	ln -s $SRC/intern/cycles/blender/addon $SRC/release/scripts/addons/cycles 2> /dev/null
	BLENDER_SYSTEM_SCRIPTS="$SRC/release/scripts" LD_LIBRARY_PATH=/opt/py/lib:/opt/sdl13/lib:/opt/oiio/lib:/opt/gcc/lib64 $SRC/blender.bin $@
	exit_code=$?
	unset SRC
	return $exit_code
}
function br() {
	if [ -e Cargo.toml ] ; then
		cargo-oos run
		return
	fi

	SRC="$(find_blender_source)"
	ln -s $SRC/intern/cycles/blender/addon $SRC/release/scripts/addons/cycles 2> /dev/null
	BLENDER_SYSTEM_SCRIPTS="$SRC/release/scripts" LD_LIBRARY_PATH=/opt/py/lib:/opt/sdl13/lib:/opt/oiio/lib:/opt/gcc/lib64 $SRC/../cmake_release/bin/blender $@
	exit_code=$?
	unset SRC
	return $exit_code
}


function dp() {
	SRC="$(find_blender_source)"
	DST="$(find_blender_build)"

	python $SRC/tools/utils_ide/cmake_qtcreator_project.py --build-dir $DST/
	# cmake -H$SRC -B$DST -G"Ninja" # ------------remove me
	unset SRC DST
}
function dpn() {
	SRC="$(find_blender_source)"
	DST="$(find_blender_build)"

	python $SRC/tools/utils_ide/cmake_netbeans_project.py --build-dir $DST/
	unset SRC DST
}
function dpe() {
	SRC="$(find_blender_source)"
	DST="$(find_blender_build)"

	python $SRC/tools/utils_ide/cmake_qtcreator_project.py --build-dir $DST/

	cd $DST
	eclipse_project_clean.py
	cd -

	unset SRC DST
}

function dc() {
	DST="$(find_blender_build)"
	ccmake $DST
	unset DST
}

function B() {
	cd /src/retrace
	# SRC="$(find_blender_source)"
	# cd $SRC
	# unset SRC
}

function M() {
	cd /src/manual
}

function A() {
	SRC="$(find_blender_source)"
	cd $SRC/release/scripts/addons
	unset SRC
}

function AC() {
	SRC="$(find_blender_source)"
	cd $SRC/release/scripts/addons_contrib
	unset SRC
}

function S() {
	SRC="$(find_blender_source)"
	cd $SRC/release/scripts
	unset SRC
}

function C() {
	DST="$(find_blender_build)"
	cd $DST
	unset DST
}

function CR() {
	cd /src/cmake_release
}

function bd() {
	SRC="$(find_blender_source)"
	cd $SRC
	BLENDER_SYSTEM_SCRIPTS="$SRC/release/scripts" LD_LIBRARY_PATH=/opt/py/lib:/opt/sdl13/lib:/opt/oiio/lib:/opt/gcc/lib64 ddd --eval-command="r" blender.bin
	exit_code=$?
	unset SRC
	return $exit_code
}

# Handy functionality,
# local, directory relative todo lists access with "t" command.
# wrap taskwarrior, use local '.task' dir if available
function t() {
	if [ -d ".task" ]; then
		TASKRC=$HOME/.taskrc HOME=$PWD task "$@"
	else
		task "$@"
	fi
}

function manx() { yelp man:$@; }

. ~/.config/any_shell/env.sh
. ~/.config/any_shell/aliases.sh

if [ -f ~/.config/any_shell/aliases.local.sh ]; then
	. ~/.config/any_shell/aliases.local.sh
fi
