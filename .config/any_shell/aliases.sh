#!/bin/sh

# ignore from history
alias shutdown=' shutdown'
alias yt-dlp=" yt-dlp"

# single character aliases
alias f='find -name'
alias d='make-universal'
alias o='xdg-open'
alias w='gitlog_latest_edit'

alias mplayer=' mpv -quiet'
alias mpv=' mpv -quiet'
alias youtube=" youtube-viewer --video-player=vlc"
alias links=" elinks"

alias valgrind='valgrind --track-origins=yes --error-limit=no --num-callers=50 --read-var-info=yes --track-fds=yes --leak-check=full'
alias callgrind='/usr/bin/valgrind --tool=callgrind --collect-jumps=yes --dump-instr=yes'

# end being clever
alias bb='d ; b'
alias touchdiff='touch `git st | pcregrep "^\tmodified:\s+" | cut -f4-8 -d" "`'
alias cmake='cmake -Wdev --warn-uninitialized'

# TODO
# alias git_revert_ws='svn diff --extensions --ignore-all-space > _tmp.diff ; svn revert -R . ; svn patch _tmp.diff ; rm _tmp.diff'
# alias git_clean_ws='git diff --ignore-space-at-eol > eol.diff && git checkout . && git apply eol.diff && rm eol.diff'

# open most recent blend in downloads dir
alias bbug=blender-open-lastest-bugreport.sh

# pacman
## alias pkg_del_orphans='pacman -Rs $(pacman -Qqtd)'
## alias pkg_up='pacman -Suy --noconfirm && packer -Suy --noconfirm --noedit && pip freeze --local | grep -v '^\-e' | cut -d = -f 1 | xargs pip install -U --allow-external'
## alias pkg_inst='pacman -S'
## alias pkg_searc='pacman -Ss'
## alias pacman-sizes="pacman -Qi | awk '/^Installed Size/{print int(\$4), name} /^Name/{name=\$3}'|sort -nr"

alias 'rst2html'='rst2html --stylesheet=/dsk/src/rst2html-style/style.css'

# ssh
alias ssh_bsd='ssh -p 3022 root@127.0.0.1'
alias ssh_redhat7='ssh -p 3021 cbarton@127.0.0.1'
alias ssh_redhat8='ssh -p 3022 cbarton@127.0.0.1'
alias chrome='chromium -user-data-dir=/tmp/test'

alias '..'='cd ..'

alias ls='ls --color=auto --group-directories-first'
alias du='du -kh'       # Makes a more readable output.
alias df='df -kTh -x tmpfs -x devtmpfs'
alias cal='cal -y'      # Show whole year

# alias ed='roxterm --profile=Large -e nvim'
# alias ed='gvim'
# alias ed='roxterm --profile=Large -e nvim'
# alias ed='emacsclient --alternate-editor="" -c'
alias ed='gvim --remote-silent'

alias ce='env NITOATPOINT_CFG="emacs=" nito-at-point'

# regex search wrappers
alias cssrep='regex-search-cwd-markup-css'
alias htmlrep='regex-search-cwd-markup-html'
alias rstrep='regex-search-cwd-markup-rst'
alias mdrep='regex-search-cwd-markup-md'
alias cr='regex-search-cwd-source-c'
alias cmrep='regex-search-cwd-source-cmake'
alias elrep='regex-search-cwd-source-elisp'
alias hsrep='regex-search-cwd-source-haskell'
alias jsrep='regex-search-cwd-source-javascript'
alias luarep='regex-search-cwd-source-lua'
alias mrep='regex-search-cwd-source-makefile'
alias prep='regex-search-cwd-source-python'
alias tomlrep='regex-search-cwd-source-toml'
alias rr='regex-search-cwd-source-rust'
alias porep='regex-search-cwd-text-po'
alias glslrep='regex-search-cwd-source-glsl'

alias V='source $(python-venv-oos --container=.sidecar-locals --venv-dir=venv)'
alias X='eval $(python-nixshell-oos --container=.sidecar-locals --filename=shell.nix)'

# git
alias g='git'
alias ga='git ci -a'
# alias gd='git di --no-prefix'
alias dd='bash -c "git diff --no-prefix -U6 | delta --side-by-side --no-gitconfig --paging=never --true-color=always --width=\$(tput cols) --syntax-theme=zenburn"'
alias gb='git branch --list'
# Checkout branch, sorted by date, showing author.
alias gcob='git_checkout_branch_interactive.sh'
alias ge='git_edit_last_commit_files.sh'
alias grm='git rebase master'

alias gu='git_pull_fancy'
alias gu_all='git_pull_fancy --all'


# alias gk='~/bin/personal/_gitk_disown'
alias gk='tig'
alias gg='emacs-git-log'

alias st='st -f Triskweline'
