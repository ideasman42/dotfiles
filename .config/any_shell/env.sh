#!/bin/sh

export PATH="\
$PATH:\
$HOME/.local/personal/bin:\
$HOME/.local/bin"

# Use sane YYYY-MM-DD
export TIME_STYLE="long-iso"

# for GO
export GOPATH="/opt/go"

export EDITOR="emacs --debug-init"

export ASAN_OPTIONS="abort_on_error=true:check_initialization_order=true:strict_init_order=true:disable_core=true"

# TRUNK
export BDIR="/src/blender"
export BDIR_BUILD="$BDIR/../cmake_debug"

# Avoid gtk3 warning about accessibility bus
export NO_AT_BRIDGE=1

export GIT_PAGER=cat


export PYTHONIOENCODING="utf_8"
export NINJA_STATUS="[%p: -%u] "

# Rust
# export RUST_SRC_PATH="/dsk/src/rust/src"

# For my own utilities
export CARGO_BIN=cargo-oos

# nirw-search wrapper
export NIRW_DEFAULTS="--color-reverse --index-zero --progress=never"

export GTK_OVERLAY_SCROLLING=0
export GTK_THEME=Adwaita:dark

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

export KALEIDOSCOPE_DIR="/scratch/src/keyboardio_kaleidoscope"

# `nitropy` complains if this isn't set, maybe an update solves this.
export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
