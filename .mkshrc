#!/bin/mksh

#
# :tocdepth: 3
#
# %%%%%%%%%%%%%%%%%%%%%%%%
# MirBSD Korn Shell (MKSH)
# %%%%%%%%%%%%%%%%%%%%%%%%

# -----------------------------------------------------------------------------
# Git Prompt
# ##########

# https://gist.github.com/ciphernix/2debfa3b6a3a71f97152
# __gitdir accepts 0 or 1 arguments (i.e., location)
# returns location of .git repo
function __gitdir
{
  # Note: this function is duplicated in git-completion.bash
	# When updating it, make sure you update the other one to match.
	if [ -z "${1-}" ]; then
		if [ -n "${__git_dir-}" ]; then
			echo "$__git_dir"
		elif [ -n "${GIT_DIR-}" ]; then
			test -d "${GIT_DIR-}" || return 1
			echo "$GIT_DIR"
		elif [ -d .git ]; then
			echo .git
		else
			git rev-parse --git-dir 2>/dev/null
		fi
	elif [ -d "$1/.git" ]; then
		echo "$1/.git"
	else
		echo "$1"
	fi
}

function __gitstate
{
	local g="$(__gitdir)"
	if [ -z "$g" ]; then
		if [ $pcmode = yes ]; then
			#In PC mode PS1 always needs to be set
			PS1="$ps1pc_start$ps1pc_end"
		fi
	else
		local r=""
		local b=""
		if [ -f "$g/rebase-merge/interactive" ]; then
			r="|REBASE-i"
			b="$(cat "$g/rebase-merge/head-name")"
		elif [ -d "$g/rebase-merge" ]; then
			r="|REBASE-m"
			b="$(cat "$g/rebase-merge/head-name")"
		else
			if [ -d "$g/rebase-apply" ]; then
				if [ -f "$g/rebase-apply/rebasing" ]; then
					r="|REBASE"
				elif [ -f "$g/rebase-apply/applying" ]; then
					r="|AM"
				else
					r="|AM/REBASE"
				fi
			elif [ -f "$g/MERGE_HEAD" ]; then
				r="|MERGING"
			elif [ -f "$g/CHERRY_PICK_HEAD" ]; then
				r="|CHERRY-PICKING"
			elif [ -f "$g/BISECT_LOG" ]; then
				r="|BISECTING"
			fi

			b="$(git symbolic-ref HEAD 2>/dev/null)" || {
				detached=yes
				b="$(
				case "${GIT_PS1_DESCRIBE_STYLE-}" in
				(contains)
					git describe --contains HEAD ;;
				(branch)
					git describe --contains --all HEAD ;;
				(describe)
					git describe HEAD ;;
				(* | default)
					git describe --tags --exact-match HEAD ;;
				esac 2>/dev/null)" ||

				b="$(cut -c1-7 "$g/HEAD" 2>/dev/null)..." ||
				b="unknown"
				b="($b)"
			}
		fi
	fi
	echo -n $r
}

# -----------------------------------------------------------------------------
# Prompt
# ######

function _PS1_PROMPT {
	local COLOR_BLUE_BG='\E[44m'
	local COLOR_YELLOW_BG='\E[43m'
	local COLOR_NORMAL_BG='\E[0m'

	local COLOR_BLACK_FG='\E[30m'
	local COLOR_NORMAL_FG='\E[39m'


	echo -n $COLOR_BLACK_FG$COLOR_YELLOW_BG${PWD/$HOME/~}$COLOR_NORMAL_BG$COLOR_NORMAL_FG

	local vcs=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
	if [ $? -eq 0 ]; then
		echo -n ' ('$COLOR_BLUE_BG$vcs$COLOR_NORMAL_BG$(__gitstate)')'
	# else
		 # svn?
	fi

	if (( USER_ID )); then
		print ' $ '
	else
		print ' # '
	fi
}

PS1='$(_PS1_PROMPT)'

# -----------------------------------------------------------------------------
# Misc Options
# ############

export HISTFILE=~/.mksh_history

# keys
bind '^L=clear-screen'

source ~/.config/any_shell/aliases.sh
source ~/.config/any_shell/aliases.local.sh
