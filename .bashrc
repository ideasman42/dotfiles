#!/bin/bash

#
# :tocdepth: 3
#
# %%%%%%%%%%%%%%%%%%%%%%%%%
# Bourne Again Shell (Bash)
# %%%%%%%%%%%%%%%%%%%%%%%%%

HAS_GIT_COMPLETE=0
if [ -d '/usr/share/git-core/contrib/completion' ]; then # centOS7
    source /usr/share/git-core/contrib/completion/git-prompt.sh
elif [ -d '/usr/share/git/completion' ]; then
    source /usr/share/git/completion/git-prompt.sh
    source /usr/share/git/completion/git-completion.bash
    HAS_GIT_COMPLETE=1
elif [ -d '/usr/pkg/share/examples/git' ]; then
    source /usr/pkg/share/examples/git/git-prompt.sh
    source /usr/pkg/share/examples/git/git-completion.bash
    HAS_GIT_COMPLETE=1
fi

# export PS1="\w\n> "
export PS1="\[\033[0;36m\]\w\[\033[0;32m\]\$(__git_ps1)\[\033[00m\] > "
export PS2=$PS1

# append to the history file, don't overwrite it
shopt -s histappend

export HISTSIZE=200000
export HISTFILESIZE=1000000000
export HISTCONTROL=ignorespace:ignoredups:erasedups
export HISTFILE=~/.bash_history

# Load in history from file.
history -r ~/.config/any_shell/history_permanent

#~ history() {
#~   _bash_history_sync
#~   builtin history "$@"
#~ }

#~ _bash_history_sync() {
#~   builtin history -a         #[1]
#~   HISTFILESIZE=$HISTFILESIZE #[2]
#~   builtin history -c         #[3]
#~   builtin history -r         #[4]
#~ }

#~ export PROMPT_COMMAND+='_bash_history_sync;'

# <<< end shared history

# ArchLinux, so we get package suggestions.
if [ -f /usr/share/doc/pkgfile/command-not-found.bash ]; then
  source /usr/share/doc/pkgfile/command-not-found.bash
fi

# shared aliases and exports
source ~/.config/any_shell/rc.sh

if [[ $HAS_GIT_COMPLETE -ne 0 ]]; then
    __git_complete g __git_main
fi
unset HAS_GIT_COMPLETE
