#!/bin/sh

SRC="/dsk/src"
DIR="mesa"
BUILD_DIR="mesa_build"
REPO="git://anongit.freedesktop.org/git/mesa/mesa"
INST="/opt/mesa"

if [ -d $SRC/$DIR ]; then
	echo "Updating Git"
	cd $SRC/$DIR
	git pull
else
	echo "Checking out Git"
	cd $SRC
	git clone $REPO $DIR
	cd $SRC/$DIR
fi

mkdir ../$BUILD_DIR
cd ../$BUILD_DIR

# out of source build isnt supported

# ../mesa/autogen.sh


../mesa/configure \
  --prefix=$INST \
  --enable-gles1=no \
  --enable-gles2=no \
  --disable-egl \
  --enable-gallium-osmesa \
  --enable-xlib-glx




## SWRAST
# ../mesa/configure \
#   --prefix=$INST \
#   --enable-gles1=no \
#   --enable-gles2=no \
#   --enable-gbm=no \
#   --disable-egl \
#   --enable-xlib-glx \
#   --enable-gallium-egl=no \
#   --enable-gallium-gbm=no \
#   --enable-motif=no \
#   --enable-gallium-llvm=no \
#   --enable-dri=no \
#   --enable-llvm=no \
#   --disable-shared-glapi \
#   --enable-osmesa \
#   --disable-dri \
#   --with-gallium-drivers=







#~ EGL2 --- NOT WORKING
# ../mesa/configure \
#   --prefix=$INST \
#   --enable-gles1=yes \
#   --enable-gles2=yes \
#   --disable-dri \
#   --enable-gallium-egl \
#   --enable-llvm-shared-libs \
#   --enable-osmesa \
#   --enable-gallium-egl=no \
#   --with-gallium-drivers=swrast
# 

# --with-gallium-drivers=swrast

make -j20
make install
