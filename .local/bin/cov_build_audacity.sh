# --------------------------
# Hard coded paths (for now)
AUDACITY_SRCDIR="/dsk/src/audacity-read-only"
AUDACITY_BLDDIR="/dsk/src/audacity-read-only"
COV_BUILD="/dsk/src/cov-analysis-linux64-6.6.1/bin/cov-build"


# -----------------
# Command line args
MAKE_ARGS="-j6"

# ------
# Update
echo "Updating..."
svn up --ignore-externals $AUDACITY_SRCDIR


# -------------------
# Find Version String

AUDACITY_REV=$(svnversion $AUDACITY_SRCDIR)

echo "Audacity Version: "$AUDACITY_REV


# -----
# Build
cd $AUDACITY_BLDDIR
rm -rf cov-int
rm audacity_archive.tgz

make clean

echo "Building..."

./configure \
    --without-ffmpeg \
    --enable-unicode \
    WX_CONFIG=/usr/bin/wx-config-2.8


# avoid uploading errors we ignore anyway
make -C lib-src

time $COV_BUILD --dir cov-int \
    make $MAKE_ARGS
echo "Done Building!"


# --------
# Compress
echo "Compressing..."
export GZIP=-9
tar czvf audacity_archive.tgz cov-int


# ------
# Upload
echo "Uploading "$(du -h audacity_archive.tgz)"..."

time curl \
	--form project=Audacity \
	--form token=OqByOS0JfUt7LNyWa0jKDw \
	--form email=ideasman42@gmail.com \
	--form file=@$AUDACITY_BLDDIR/audacity_archive.tgz \
	--form version=$AUDACITY_REV \
	--form description=Description http://scan5.coverity.com/cgi-bin/upload.py

echo "Done Uploading!"
