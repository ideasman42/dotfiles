#!/bin/sh

# reference
# http://wiki.netbeans.org/WorkingWithNetBeansSources

SRC="/dsk/src"
DIR="netbeans"
REPO="http://hg.netbeans.org/main-silver"

cd $SRC
if [ -d $SRC/$DIR ]; then
	echo "Updating Mercurial"
	cd $DIR
	hg pull
	hg up
else
	echo "Checking out Mercurial"
	hg clone $REPO $DIR
fi

export ANT_OPTS="-Xmx2560m -XX:MaxPermSize=960m"

ant
