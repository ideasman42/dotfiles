#!/usr/bin/env python3

"""
Continually check for file/line formatted string in the clipboard.

Open in an editor, handy for checking over valgrind output and similar.

To lookup memory addresses, pass a binary argument, so addresses such as '0x1234'
will use addr2line to find the source line.
"""


VERBOSE = False


def run(cmd):
    if VERBOSE:
        print(">>> ", cmd)

    import subprocess
    proc = subprocess.Popen(
        cmd,
        shell=False,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )

    result = proc.stdout.read()
    return result


def main():
    import re

    # Check for:
    #   file.c:123
    # ..or
    #   file.c:123:4
    file_line_col_re = re.compile(r"\s*([^\:]+)\:(\d+):(\d+)")
    file_line_re     = re.compile(r"\s*([^\:]+)\:(\d+)")

    def line_parse(text):
        m = re.match(file_line_col_re, text)
        if m is not None:
            return m.group(1), int(m.group(2)), int(m.group(3))

        m = re.match(file_line_re, text)
        if m is not None:
            return m.group(1), int(m.group(2)), 0

        return None, None, None

    import time

    clip_prev = None
    while 1:
        clip_curr = run(["xclip", "-out"])

        # first time only, don't do anything
        if clip_prev is None:
            clip_prev = clip_curr

        if clip_curr != clip_prev:
            clip_text = clip_curr.decode('utf-8')
            print("  text is:", clip_text)
            if clip_text.startswith("0x"):
                import sys
                clip_text = run(["addr2line", "-e", sys.argv[-1], clip_text])
                clip_text = clip_text.decode('utf-8')
                print("  text is:", clip_text)
            file, line, col = line_parse(clip_text)
            if file is not None:
                run(["ce", "%s:%d:%d" % (file, line, col)])
        clip_prev = clip_curr
        time.sleep(0.25)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("finishing up!")

