#!/usr/bin/python

import sys
fn = sys.argv[-1]

if 1:
    import xml.dom.minidom
    xml = xml.dom.minidom.parse(fn) # or xml.dom.minidom.parseString(xml_string)
    pretty_xml_as_string = xml.toprettyxml()
else:
    import lxml.etree as etree
    x = etree.parse("filename")
    pretty_xml_as_string = etree.tostring(x, pretty_print=True)

pretty_xml_as_string = "\n".join([l for l in pretty_xml_as_string.split("\n") if l.strip()])
print(pretty_xml_as_string)
