#!/bin/bash

# echo all commands
set -x

SRCDIR="/dsk/src"
INSTDIR="/opt/builder"

if [ -d $SRCDIR/gnome-builder ] ; then
	cd $SRCDIR/gnome-builder
	SHA1_PREV=$(git rev-parse HEAD)
	git pull
	SHA1_CURR=$(git rev-parse HEAD)
	if [ "$SHA1_PREV" == "$SHA1_CURR" ] ; then
		echo "Already at rev $SHA1_CURR, nothing to do!"
		exit 0
	fi
else
	echo "Checking out "$SRCDIR"/gnome-builder ..."
	cd $SRCDIR
	git clone git://git.gnome.org/gnome-builder
	cd $SRCDIR/gnome-builder
fi

rm -rf $INSTDIR

rm -rf ../gnome-builder_build
mkdir  ../gnome-builder_build
# cd     ../gnome-builder_build  # OUT OF SOURCE BUILDS BROKEN
cd     ../gnome-builder

../gnome-builder/autogen.sh \
  --prefix=$INSTDIR \
  --disable-appstream-util && \
  make -j6 && \
  make install

# ensure we built
if [ $? -ne 0 ]; then echo "error -> $?" ; exit 1 ; fi

ln -s $INSTDIR/bin/gnome-builder ~/bin/

# Needed to run in $INSTDIR
rm        /usr/share/gir-1.0/Ide-1.0.gir
ln -s $INSTDIR/share/gir-1.0/Ide-1.0.gir \
          /usr/share/gir-1.0/Ide-1.0.gir

ln -s $INSTDIR/share/glib-2.0/schemas/* /usr/share/glib-2.0/schemas/
glib-compile-schemas /usr/share/glib-2.0/schemas/

