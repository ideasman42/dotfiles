#!/bin/bash
for fn in "$@"
do
	# TEST
	uncrustify -c /uncrustify.cfg  --no-backup --replace "$fn"

    cp "$fn" "$fn.NEW"
	svn revert "$fn" 1> /dev/null
	
	diff "$fn" "$fn.NEW" -u --ignore-trailing-space --ignore-blank-lines > "$fn.DIFF"
	
	patch -p0 < "$fn.DIFF" 1> /dev/null
	
	rm "$fn.NEW"
	rm "$fn.DIFF"
done
