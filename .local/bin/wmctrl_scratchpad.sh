#!/bin/bash

function minimize {
	id=$1
	xprop -id $id -f _SCRATCH 32ii -set _SCRATCH $(date +%s,%N);
	xdotool windowunmap $id
}

function restore {
	# load from scratchpad
	id=$(for w in $(xwininfo -root -children | grep -e "^\s*0x[0-9a-f]\+" -o); do
		t=$(xprop -id $w _SCRATCH | grep ' = \(.*\)')
		if [ -n "$t" ]; then
			echo $t $w
		fi
		# Sort so most recently minimized is first to restore.
		done|sort -n -r|head -n1|cut -d" " -f 5)

	if [ -n "$id" ]; then
		xprop -id $id -remove _SCRATCH
		xdotool windowmap $id
	fi
}

if [ "$1" = "minimize" ]; then
	# send to scratchpad
	id=$(xdotool getwindowfocus)
	# Avoid hiding windows which are frames created by the window manager (notion does this)
	# hiding such windows just causes a confusing situation.
	if [ -n "$(xprop -id $id WM_STATE 2>&1 | grep 'not found')" ]; then
		echo "Widow is not regular, abort!"
		exit 1
	fi
	if [ -n "$id" ]; then
		minimize $id
	fi
elif [ "$1" = "restore" ]; then
	restore
else
	echo "Unknown command '$1'"
	exit 1
fi
