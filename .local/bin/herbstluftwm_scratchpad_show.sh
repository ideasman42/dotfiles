#!/bin/bash
i=$(for w in $(xwininfo -root -children | grep -e "^\s*0x[0-9a-f]\+" -o);do
	t=$(xprop -id $w _SCRATCH | grep ' = \(.*\)')
	if [ -n "$t" ];then
		echo $t $w
	fi
done|sort -n|head -n1|cut -d" " -f 5);
if [ -n "$i" ]; then
	xprop -id $i -remove _SCRATCH
	xdotool windowmap $i
	xdotool windowactivate $i
fi

