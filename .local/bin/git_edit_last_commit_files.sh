#!/usr/bin/env bash

REV="$1"
if [ -z "$REV" ]; then
	REV="HEAD"
fi

declare -a FILES_AND_LINES=()

echo 'Editing files:'

# Find first line of each edit.
while IFS= read -r -d '' FILE; do
  LINE=$(git show -U0 "$FILE" | pcregrep -M --only-matching=1 '^@@\s+\-[\d]+(?:,[\d]+)?\s+\+([\d]+)(?:,[\d]+)?\s+@@' | head -n1)
  FILES_AND_LINES+=("$FILE:$LINE")
  # echo "$FILE:$LINE"
done < <(git diff-tree --no-commit-id --name-only -z -r "$REV")

nito-at-point ${FILES_AND_LINES[@]}
