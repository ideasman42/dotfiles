#!/bin/sh

cd /dsk/src/i3
git pull


# ----
rm -rf /opt/i3
autoreconf -fi
./configure --prefix=/opt/i3

cd /dsk/src/i3/x86_64-pc-linux-gnu
make clean
make -j10 install
