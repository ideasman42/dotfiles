#!/bin/sh

SRC="/dsk/src"
DIR="smatch"
REPO="git://repo.or.cz/smatch.git"

cd $SRC
if [ -d $DIR ]; then
	echo "Updating Git"
	cd $DIR
	git pull
else
	echo "Checking out Git"
	git clone $REPO $DIR
fi

make clean
make -j9

# now you have smatch binary. no need to install
# just symlink to binary dir or add to $PATH
