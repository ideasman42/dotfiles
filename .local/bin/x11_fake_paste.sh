#!/bin/bash
xdotool \
	keyup `grep '^#define' /usr/include/X11/keysymdef.h | sed -r 's/^#define XK_(\S*?).*$/\1/;' | grep -E '_(L|R|Level.*)$'` \
	type --delay 1 "$(xclip -o)"

