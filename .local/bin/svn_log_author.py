#!/usr/bin/env python3

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

# --------------------------------
# Global Options (could make args)

USE_COLOR = True
USE_SKIP_BUGFIX = False
USE_COUNT = False

blacklist_revs = []

# end global options
# ------------------

import sys
import os
import io
import re

# -----------
# Handle args
import argparse

parser = argparse.ArgumentParser(description="SVN XML Pretty Printer")
parser.add_argument("-a", "--author", dest="author", type=str, required=False,
                    help="The name of the author or a blank string for all")
parser.add_argument("-w", "--wiki", dest="wiki", action='store_true', default=False,
                    help="Use wiki formatting")
args = parser.parse_args(sys.argv[1:])
# main assignment!
check_author = args.author
if check_author is None:
    check_author = ""
use_wiki = bool(args.wiki)

del argparse, parser, args
# end argparse
# ------------

# ------------
# Runtime Vars

re_bugreport = re.compile(r"(\[#)(\d+)(\])")

# end runtime vars
# ----------------


if USE_COLOR:
    color_codes = {
        'black':        '\033[0;30m',
        'bright_gray':  '\033[0;37m',
        'blue':         '\033[0;34m',
        'white':        '\033[1;37m',
        'green':        '\033[0;32m',
        'bright_blue':  '\033[1;34m',
        'cyan':         '\033[0;36m',
        'bright_green': '\033[1;32m',
        'red':          '\033[0;31m',
        'bright_cyan':  '\033[1;36m',
        'purple':       '\033[0;35m',
        'bright_red':   '\033[1;31m',
        'yellow':       '\033[0;33m',
        'bright_purple':'\033[1;35m',
        'dark_gray':    '\033[1;30m',
        'bright_yellow':'\033[1;33m',
        'normal':       '\033[0m',
    }

    def colorize(msg, color=None):
        return (color_codes[color] + msg + color_codes['normal'])
else:
    def colorize(msg, color=None):
        return msg


# avoid encoding issues
sys.stdin = os.fdopen(sys.stdin.fileno(), "rb") 
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8', errors='surrogateescape', line_buffering=True)
sys.stderr = io.TextIOWrapper(sys.stderr.buffer, encoding='utf-8', errors='surrogateescape', line_buffering=True)

from xml.dom.minidom import parseString
tree = parseString(sys.stdin.read())

log = tree.getElementsByTagName("log")[0]
commits = log.getElementsByTagName("logentry")
tot = 0


for ci in commits:
    rev = ci.attributes['revision'].value
    author = ci.getElementsByTagName("author")[0].childNodes[0].nodeValue
    msg_tag = ci.getElementsByTagName("msg")
    msg = "<empty message>"
    if msg_tag:
        msg_tag_child = msg_tag[0].childNodes
        if msg_tag_child:
            msg = msg_tag_child[0].nodeValue

    paths = ci.getElementsByTagName("paths")[0]
    files = []
    for p in paths.getElementsByTagName("path"):
        fn = p.childNodes[0].nodeValue
        action = p.attributes['action'].nodeValue
        files.append((action, fn))

    # sanity check
    if int(rev) in blacklist_revs:
        continue
    msg_test = " ".join(msg.lower().split())
    if msg_test.startswith(("code cleanup:", "style cleanup:", "internal:")):
        continue

    if USE_SKIP_BUGFIX:
        if ("fix [#" in msg_test) or ("fix #" in msg_test) or \
            ("bugfix [#" in msg_test) or ("bugfix #" in msg_test):

            continue
        if msg_test.startswith(("fix ", "bugfix ", "bug fix ")):
            continue
        if "fix for " in msg_test:
            continue

    del msg_test

    if (author == check_author) or (check_author == ""):
        if use_wiki:
            commit_str = "({{Commit|%s}})" % rev
        else:
            commit_str = "Commit %s" % rev

        print("\n%s %s %s%s\n" %
                (colorize(commit_str, color="cyan"),
                 colorize(author, color="blue"),
                 colorize(("-" * (78 - (len(rev) + len(author)))), color="white"),
                 colorize((" (%d)" % int((tot / 614) * 100)), color="red") if USE_COUNT else ""))

        msg = msg.strip()

        if use_wiki:
            msg = re.sub(re_bugreport, colorize(r"{{BugReport|\2}}", color="red"), msg)
        else:
            msg = re.sub(re_bugreport, colorize(r"\1\2\3", color="red"), msg)


        print(msg)
        print("")
        for action, fn in sorted(files):
            print(colorize("%s: %s" % (action, fn), color="dark_gray"))

        tot += 1

