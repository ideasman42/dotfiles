#!/usr/bin/env bash
BRANCH=$(
    git for-each-ref \
        --sort=committerdate refs/heads/ \
        --format="%(refname:short)|%(committerdate:short) %(authorname)" | \
        column -t -s"|" | \
        fzf --tac --no-sort | cut -d" " -f1)
# User exits
if [ -z "$BRANCH" ]; then
    exit 0
fi

git checkout "$BRANCH"
