#!/bin/bash

PARENT=""
GRANDPARENT="$(realpath ../)"
FIND=$1

if [ -d "./$FIND" ]; then
    GRANDPARENT="$(realpath ./)"
else
    while [ ! -d "$GRANDPARENT/$FIND" ] && [ "$GRANDPARENT" != '/' ] ; do
		    PARENT=$GRANDPARENT
		    GRANDPARENT="$(realpath $PARENT/..)"
    done
fi

if [ ! -z "$GRANDPARENT" ] && [ "$GRANDPARENT" != '/' ] ; then
		echo $GRANDPARENT
fi
