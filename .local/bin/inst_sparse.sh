#!/bin/sh

SRC="/scratch/src"
DIR="sparse"
REPO="git://git.kernel.org/pub/scm/devel/sparse/sparse.git"

cd $SRC
if [ -d $DIR ]; then
	echo "Updating Git"
	cd $DIR
	git pull
else
	echo "Checking out Git"
	git clone $REPO $DIR
fi

make clean
make -j9

# now you have sparse binary. no need to install
# just symlink to binary dir or add to $PATH
