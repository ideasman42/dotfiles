#!/bin/sh

#  walkman_video.sh ~/somemovie.avi
#

if mount | grep "/media/WALKMAN" > /dev/null; then
    echo "Found mount..."
else
    echo "Mount not found, aborting"
    exit 0
fi

for fullfile in "$@"
do


    # fullfile=$1
    filename=$(basename "$fullfile")
    extension="${filename##*.}"
    filename="${filename%.*}"

    # TODO: move to ffmpeg
    # 
    # WIP. works but not on walkman
    # ffmpeg -i in.avi -strict experimental \
    #        -vcodec mpeg4 -c:a aac -b:v 250k -b:a 128k -vf scale=320:240 -af \
    #        aresample=osr=48000 out.avi
    #
	# -vf-add harddup
	mencoder -ofps 25 -of lavf -lavfopts format=mp4 \
		-lavfdopts o=genpts \
        -af lavcresample=48000 -srate 48000  \
        -vf-add scale=320:240 -oac lavc -ovc lavc -lavcopts \
        aglobal=1:vglobal=1:acodec=libfaac:abitrate=128:vcodec=mpeg4:vbitrate=250:autoaspect:mbd=2:mv0:trell:v4mv:last_pred=3:predia=2:dia=2:precmp=2:cmp=2:subcmp=2:preme=2:turbo \
        -quiet -info name="Name_to_display" \
        -o "/media/WALKMAN/VIDEO/$filename.mp4" \
        "$fullfile"

done

echo "Done!"
