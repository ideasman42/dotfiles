#!/usr/bin/env python3
#
# Extracts C function definitions from headers and checks they exist in a binary.
#
# Usage:
#    clang_unused_funcs.py BINARY SOURCE_DIR defines....
#
# Eg:
#    clang_unused_funcs.py /src/cmake_debug/bin/blender:/src/cmake_debug/bin/makesdna:/src/cmake_debug/bin/makesrna /src/blender/source/blender

import sys

BINARY = sys.argv[1]
SOURCE = sys.argv[2]
args = sys.argv[3:]

# for quick tests
# SOURCE = "/src/blender/source/blender"
# BINARY = "/src/cmake_debug/bin/blender:/src/cmake_debug/bin/makesdna:/src/cmake_debug/bin/makesrna"

# -----------------------------------------------------------------------------
import os
def load_binary(binary):
    """ Extracts lines that look like, eg:
        '0000000000d350b5 t dot_v3v3'
    """
    funcs = []
    for l in os.popen('nm -an %s' % binary).readlines():
        l_split = l.split()
        if len(l_split) == 3 and l_split[1] in ("t", "T"):
            funcs.append(l_split[2])
    return set(funcs)


BINARY_FUNCTIONS = set()
for b in BINARY.split(":"):
    BINARY_FUNCTIONS |= load_binary(b)


# -----------------------------------------------------------------------------

import sys

if 0:
    # Examples with LLVM as the root dir: '/dsk/src/llvm'

    # path containing 'clang/__init__.py'
    CLANG_BIND_DIR = "/dsk/src/llvm/tools/clang/bindings/python"

    # path containing libclang.so
    CLANG_LIB_DIR = "/opt/llvm/lib"
else:
    import os
    CLANG_BIND_DIR = os.environ.get("CLANG_BIND_DIR")
    CLANG_LIB_DIR = os.environ.get("CLANG_LIB_DIR")

    if CLANG_BIND_DIR is None:
        print("$CLANG_BIND_DIR python binding dir not set")
    if CLANG_LIB_DIR is None:
        print("$CLANG_LIB_DIR clang lib dir not set")

sys.path.append(CLANG_BIND_DIR)

import clang
import clang.cindex
from clang.cindex import (CursorKind,
                          TypeKind,
                          TokenKind)

clang.cindex.Config.set_library_path(CLANG_LIB_DIR)

index = clang.cindex.Index.create()

# print('Translation unit: %s' % tu.spelling)

store_defs = {}

def extract_function_defs(filepath, tu, ls_pair):

    # main checking function
    def extract_func(node):
        is_func = bool(list(node.get_children()))
        name_func = node.spelling
        print("  ", name_func, node.kind)

        ls_pair[is_func].append(name_func)

        if not is_func:
            location = node.location
            store_defs[name_func] = (str(location.file), int(location.line))

    # we dont really care what we are looking at, just scan entire file for
    # function calls.

    def recursive_func_call_check(node):
        for c in node.get_children():
            if str(c.location.file) == filepath:
                if c.kind == CursorKind.FUNCTION_DECL:
                    extract_func(c)
            recursive_func_call_check(c)

    recursive_func_call_check(tu.cursor)

ls = [], []
# extract_function_defs(tu, True, ls)


def extract_function_defs_recursive(dirpath):
    from os.path import join, splitext

    def source_list(path, filename_check=None):
        for dirpath, dirnames, filenames in os.walk(path):
            # skip ".git"
            dirnames[:] = [d for d in dirnames if not d.startswith(".")]
            for filename in filenames:
                filepath = join(dirpath, filename)
                if filename_check is None or filename_check(filepath):
                    yield filepath

    def is_source(filename):
        ext = splitext(filename)[1]
        # return (ext in {".c", ".inl", ".cpp", ".cxx", ".hpp", ".hxx", ".h"})
        return (ext in {".hpp", ".hxx", ".h", ".hh"})

    for filepath in source_list(dirpath, is_source):
        print(filepath)
        tu = index.parse(filepath, args)
        extract_function_defs(filepath, tu, ls)


extract_function_defs_recursive(SOURCE)


# -----------------------------------------------------------------------------

print("\n\nFunctions not found:...")
for func in sorted(set(ls[1])):
    if func not in BINARY_FUNCTIONS:
        print("  %s" % func)
