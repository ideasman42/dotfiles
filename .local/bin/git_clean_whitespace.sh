#!/bin/bash

PATCH=$(mktemp -t git_whitespace_XXXX --suffix=".diff")

git diff --ignore-space-at-eol > $PATCH && \
	git stash && \
	git apply $PATCH && \
	echo "Cleaned whitespace, original diff stashed, run 'git stash drop' if all is well."

rm $PATCH
