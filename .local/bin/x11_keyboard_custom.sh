#!/bin/bash

# reset, needed if we're loading a second time.
setxkbmap -layout us

xset r rate 200 40

XINPUT_OUTPUT="$(xinput)"
# if grep -P -q "(ErgoDox|Dactyl)" <<<$XINPUT_OUTPUT; then
# 	# numlock messes w/ ergodox layout.
# 	numlockx
# 	# for some reason F14 maps to XF86Launch5, XF86Tools
# 	xmodmap \
#       -e "keycode 196 = F18" \
#       -e "keycode 195 = F17" \
#       -e "keycode 194 = F16" \
#       -e "keycode 193 = F15" \
#       -e "keycode 192 = F14" \
#       -e "keycode 191 = F13"

#   # Scroll-lock
#   # xmodmap -e "keycode 78 = Hyper_R"
#   # xmodmap -e "add mod3 = Hyper_R"

#   # Remap R-Super to R-Hyper
#   xmodmap -e "remove mod4 = Super_R"
#   xmodmap -e "keycode 134 = Hyper_R"
#   xmodmap -e "add mod3 = Hyper_R"

# 	# Capslock as Find
# 	xmodmap -e 'remove Lock = Caps_Lock' \
# 			-e 'keysym Caps_Lock = Find'

# else
# 	# Capslock as Esc
# 	xmodmap -e 'remove Lock = Caps_Lock' \
# 			-e 'remove Control = Escape' \
# 			-e 'keysym Escape = Caps_Lock' \
# 			-e 'keysym Caps_Lock = Escape'

# 	# escape is F13
# 	xmodmap -e "keycode 9 = F13"
# 	# NumLock is F14
# 	xmodmap -e "remove mod2 = Num_Lock" \
# 			-e "keycode 77 = F14"

# 	# Use numbers when numlock is off
# 	xmodmap -e "keysym KP_End = KP_1" \
# 			-e "keysym KP_Down = KP_2" \
# 			-e "keysym KP_Next = KP_3" \
# 			-e "keysym KP_Left = KP_4" \
# 			-e "keysym KP_Begin = KP_5" \
# 			-e "keysym KP_Right = KP_6" \
# 			-e "keysym KP_Home = KP_7" \
# 			-e "keysym KP_Up = KP_8" \
# 			-e "keysym KP_Prior = KP_9" \
# 			-e "keysym KP_Insert = KP_0" \
# 			-e "keysym KP_Delete = KP_Decimal"

# 	# swap numbers, symbols!
# 	xmodmap -e "keycode  10 = exclam 1 exclam 1" \
# 			-e "keycode  11 = at 2 at 2" \
# 			-e "keycode  12 = numbersign 3 numbersign 3" \
# 			-e "keycode  13 = dollar 4 dollar 4" \
# 			-e "keycode  14 = percent 5 percent 5" \
# 			-e "keycode  15 = asciicircum 6 asciicircum 6" \
# 			-e "keycode  16 = ampersand 7 ampersand 7" \
# 			-e "keycode  17 = asterisk 8 asterisk 8" \
# 			-e "keycode  18 = parenleft 9 parenleft 9" \
# 			-e "keycode  19 = parenright 0 parenright 0"

# fi

# TODO: use udev for all mouse settings.

if grep -q "VerticalMouse 2" <<<$XINPUT_OUTPUT; then
	# workstation:
	if grep -q "ErgoDox" <<<$XINPUT_OUTPUT; then
		# use keyboard for mouse keys (except mouse wheel)
		# xinput set-button-map "Kingsis Peripherals Evoluent VerticalMouse 2" -1 -1 -1 4 5 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1

		# Scrap the exception, use mouse keys!
		xinput set-button-map "Kingsis Peripherals Evoluent VerticalMouse 2" 1 9 3 4 5 6 7 8 2
	else
		xinput set-button-map "Kingsis Peripherals Evoluent VerticalMouse 2" 1 9 3 4 5 6 7 8 2
	fi
	# set accel
	# xinput --set-prop 'Kingsis Peripherals Evoluent VerticalMouse 2' 'Device Accel Constant Deceleration' 1.3
fi

if grep -q "Cirque" <<<$XINPUT_OUTPUT; then
	DEV="Cirque Corporation 9925 AG Touchpad"
	xinput --set-prop "$DEV" 'libinput Accel Speed' 1
	xinput --set-ptr-feedback "$DEV" 100 50 2
	# xinput --set-prop "$DEV" 'Device Accel Constant Deceleration' 2.5
	# xinput --set-prop "$DEV" 'Device Accel Velocity Scaling' 2
	# disable mouse buttons... kindof!
	xinput set-button-map "$DEV" -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
fi

if grep -q "Wacom" <<<$XINPUT_OUTPUT; then
	WACOM_TOUCH=$(xsetwacom --list | grep TOUCH | sed -r "s/.*id: *([0-9]*).*/\1/")
	xsetwacom --set "$(xsetwacom --list | grep TOUCH | sed -r 's/.*id: *([0-9]*).*/\1/')" gesture off
	xinput --set-prop 'Wacom Intuos5 touch M Finger touch' 'Device Accel Adaptive Deceleration' 1
	xinput --set-prop 'Wacom Intuos5 touch M Finger touch' 'Device Accel Constant Deceleration' 2.5
	xinput --set-prop 'Wacom Intuos5 touch M Finger touch' 'Device Accel Velocity Scaling' 2
	# disable mouse buttons... kindof!
	xinput set-button-map "Wacom Intuos5 touch M Finger touch" -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
fi

# Trackball
if grep -q "Kensington Expert Mouse" <<<$XINPUT_OUTPUT; then
	DEV="Kensington      Kensington Expert Mouse"
	xinput set-button-map "$DEV" 1 8 3 4 5 6 7 2
	xinput --set-prop "$DEV" 'libinput Accel Speed' 1
	xinput --set-ptr-feedback "$DEV" 100 50 2
fi
