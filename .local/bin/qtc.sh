#!/bin/sh

PATH=$PATH:/root/.config/QtProject/qtcreator/externaltools
export LD_LIBRARY_PATH=/opt/py33/lib:/opt/sdl13/lib:/opt/oiio/lib:/opt/gcc/lib64

# python /b/build_files/cmake/cmake_qtcreator_project.py

QT_STYLE_OVERRIDE="gtk"

# GIT
if [ -e /opt/qtc/bin/qtcreator ]; then
	exec /opt/qtc/bin/qtcreator default
else
	exec /usr/bin/qtcreator default
fi

# exec /dsk/src/qt-creator/bin/qtcreator default
# /usr/bin/qtcreator default

# beta
# exec /opt/qtcreator-2.2.84/bin/qtcreator default
