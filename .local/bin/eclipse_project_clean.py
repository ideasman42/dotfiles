#!/usr/bin/env python3

import xml.etree.ElementTree as etree

ECLIPSE_PROJ = ".project"
ECLIPSE_CPROJ = ".cproject"

ECLIPSE_PROJ_HEAD = b'<?xml version="1.0" encoding="UTF-8"?>\n'
ECLIPSE_CPROJ_HEAD = b'<?xml version="1.0" encoding="UTF-8" standalone="no"?><?fileVersion 4.0.0?>\n'


# First:  Only display C/C++ source
# Second: Hide '.' dirs, '.svn', '.git'... etc
project_filter = """
\t<filteredResources>
\t\t<filter>
\t\t\t<id>1368941520074</id>
\t\t\t<name></name>
\t\t\t<type>21</type>
\t\t\t<matcher>
\t\t\t\t<id>org.eclipse.ui.ide.multiFilter</id>
\t\t\t\t<arguments>1.0-name-matches-true-true-^.*\.(c|cc|cpp|cxx|h|hh|hpp|hxx|inl|m|mm)$</arguments>
\t\t\t</matcher>
\t\t</filter>
\t\t<filter>
\t\t\t<id>1368941520084</id>
\t\t\t<name></name>
\t\t\t<type>26</type>
\t\t\t<matcher>
\t\t\t\t<id>org.eclipse.ui.ide.multiFilter</id>
\t\t\t\t<arguments>1.0-name-matches-true-false-.*</arguments>
\t\t\t</matcher>
\t\t</filter>
\t</filteredResources>
"""


# ideally should get these from gcc
# gcc -c -xc - -v
import subprocess
import os
cproject_includes = tuple(
    [l.strip().decode("utf-8") for l in subprocess.check_output(
        "cpp -x c++ -Wp,-v".split(),
        input="",
        stderr=subprocess.STDOUT).split(b"\n")
     if l.startswith(b" /") and os.path.exists(l.strip())]
    )

def proj_strip_excess_links():
    proj = etree.parse(ECLIPSE_PROJ)
    root = proj.getroot()
    lr = root.find("linkedResources")
    # remove all [Subprojects], [Targets]
    # ... rely on [Source Directory]
    for l in lr.findall("link"):
        n = l.find("name")
        if n.text.startswith(("[Subprojects]", "[Targets]")):
            # remove!
            lr.remove(l)
    with open(ECLIPSE_PROJ, 'wb') as f:
        f.write(ECLIPSE_PROJ_HEAD)
        etree.ElementTree(root).write(f, 'utf-8')


def proj_add_filter():
    proj = etree.parse(ECLIPSE_PROJ)
    root = proj.getroot()

    filter = etree.fromstring(project_filter)
    root.append(filter)

    with open(ECLIPSE_PROJ, 'wb') as f:
        f.write(ECLIPSE_PROJ_HEAD)
        etree.ElementTree(root).write(f, 'utf-8')


def cproj_add_includes():
    proj = etree.parse(ECLIPSE_CPROJ)
    root = proj.getroot()

    def get_pathentry():
        cfg = root.find("storageModule").find("cconfiguration")
        for sm in cfg.findall("storageModule"):
            if sm.get("moduleId") == "org.eclipse.cdt.core.pathentry":
                return sm
        raise Exception("can't find pathentry")

    sm = get_pathentry()
    for inc in cproject_includes:
        sm.append(etree.fromstring('<pathentry include="%s" kind="inc" path="" system="true"/>\n' % inc))

    with open(ECLIPSE_CPROJ, 'wb') as f:
        f.write(ECLIPSE_CPROJ_HEAD)
        etree.ElementTree(root).write(f, 'utf-8')


if __name__ == "__main__":
    proj_strip_excess_links()
    proj_add_filter()
    cproj_add_includes()
