#!/usr/bin/env bash

readarray -t -d '' FILES < <(
	  git ls-files -z --other --directory |
		    grep --null-data --null -v '.bin$\|Cargo.lock$' | \
		    grep --null-data --null -v 'scripts/addons\|scripts/addons_contrib$'
)

if [ "$FILES" = "" ]; then
	  echo  "Nothing to clean!"
	  exit 0
fi

echo "Dirty files:"
echo ""
printf '  %s\n' "${FILES[@]}"

DO_REMOVE=0

while true; do
	  echo ""
	  read -p "Remove ${#FILES[@]} files? [y/n]: " choice
	  case "$choice" in
		    y|Y )
			      DO_REMOVE=1
			      break ;;
		    n|N )
			      echo "Exiting!"
			      break ;;
		    * ) echo "Invalid input, expected [Y/y/N/n]"
			      continue ;;
	  esac
done

if [ "$DO_REMOVE" -eq 1 ];then
	  echo "Removing!"
	  for f in "${FILES[@]}"; do
        rm -rfv -- "$f"
	  done
fi
