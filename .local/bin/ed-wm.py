#!/usr/bin/python3

import os

def run(cmd):
    print(">>> ", " ".join(cmd))
    import subprocess
    proc = subprocess.Popen(" ".join(cmd),
                            shell=True,
                            stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE)

    result = proc.stdout.read()
    return result

def run_bg(cmd):
    os.system(cmd + " & disown")

data = run(["wmctrl", "-m"])
wm = [d[6:].strip() for d in data.split(b'\n') if d.startswith(b'Name: ')][0]
print(wm)

files = []
if wm == b'awesome':
    files += [
        "~/.config/awesome/rc.lua",
        ]

elif wm == b'notion':
    files += [
        "~/.notion/cfg_notioncore.lua",  # main file first
        "~/.notion/cfg_defaults.lua",
        "~/.notion/cfg_notion.lua",
        "~/.notion/cfg_tiling.lua",
        ]

elif wm == b'xmonad':
    files += [
        "~/.xmonad/xmonad.hs",
        ]

elif wm == b'Openbox':
    files += [
        "~/.config/openbox/rc.xml",
        ]

elif wm == b'i3':
    files += [
        "~/.i3/config",
        "~/.i3status.conf",
        ]

elif wm == b'herbstluftwm':
    files += [
        "~/.config/herbstluftwm/autostart",
        ]

elif wm == b'bspwm':
    files += [
        "~/.config/bspwm/bspwm.sxhkdrc",
        "~/.config/bspwm/bspwmrc",
        ]

elif wm == b'spectrwm':
    files += [
        "~/.spectrwm.conf",
        ]
else:
    print("Not known window manager %r" % wm)
    import sys
    sys.exit(0)

files[:] = [os.path.expanduser(f) for f in files]
run_bg("emacs " + " ".join(files))
