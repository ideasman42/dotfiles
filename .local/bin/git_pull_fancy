#!/usr/bin/env python3

"""
Git update utility.
"""

import sys
import os
import argparse

from typing import (
    Any,
)


def argparse_create() -> argparse.ArgumentParser:

    # When --help or no args are given, print this help
    description = __doc__
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        "--all",
        dest="update_all",
        action='store_true',
        help="Update all submodules.",
    )

    parser.add_argument(
        "--pause",
        dest="pause",
        action='store_true',
        help="Pause instead of performing any operations (to run with symlinks applied).",
    )


    return parser


class TemporarySymlinkSwap:
    __slots__ = (
        "_symlink_pairs"
    )

    @staticmethod
    def _swap_symlink_with_dir(dir_path: str, dir_link: str, state: str) -> None:
        if os.path.islink(dir_path):
            print(state, "path was link", dir_path)
            return
        if not os.path.islink(dir_link):
            print(state, "path was not link", dir_link)
            return
        os.remove(dir_link)
        os.rename(dir_path, dir_link)
        os.symlink(dir_link, dir_path)

    def __init__(self, filename_base: str) -> None:
        symlink_pairs: list[tuple[str, str]] = []

        filename_parent = os.path.normpath(os.path.join(filename_base, ".."))
        filename_dir_only = filename_base[len(filename_parent) + 1:]
        filename_symlink_prefix = filename_dir_only + ".symlink."

        for entry in os.scandir(filename_parent):
            name = entry.name
            if not name.startswith(filename_symlink_prefix):
                continue

            name_suffix = name[len(filename_symlink_prefix):]

            full_path_ext = os.path.join(filename_parent, name)
            full_path_int = os.path.join(filename_base, name_suffix.replace(".", os.sep))
            if not os.path.exists(full_path_int):
                print("warning: internal path not found", full_path_int)
                continue

            symlink_pairs.append((full_path_ext, full_path_int))

        self._symlink_pairs = symlink_pairs

        for full_path_ext, full_path_int in self._symlink_pairs:
            print(full_path_ext, full_path_int)

    def __enter__(self) -> None:
        for full_path_ext, full_path_int in self._symlink_pairs:
            self._swap_symlink_with_dir(full_path_ext, full_path_int, "ENTER")

    def __exit__(self, _exc_type: Any, _exc_value: Any, _exc_traceback: Any) -> None:
        for full_path_ext, full_path_int in self._symlink_pairs:
            self._swap_symlink_with_dir(full_path_int, full_path_ext, "EXIT")


def main() -> None:
    import subprocess

    argparse_create()

    args = argparse_create().parse_args()

    dir_cwd = os.path.abspath(".")

    with TemporarySymlinkSwap(dir_cwd):
        if args.pause:
            print("Paused... Ctrl-C to exit:")
            try:
                import time
                time.sleep(1000000000)
            except KeyboardInterrupt:
                print("Done...")
                return

        make_update_script = os.path.join(dir_cwd, "build_files", "utils", "make_update.py")
        if os.path.exists(make_update_script):
            if args.update_all:
                subprocess.run([sys.executable, make_update_script, "--use-linux-libraries", "--use-tests"])
            else:
                subprocess.run(["git", "pull"])
        else:
            if args.update_all:
                subprocess.run(["git", "pull"])
                subprocess.run(["git", "submodule", "update", "--init", "--recursive"])
                subprocess.run(["git", "submodule", "foreach", "git", "pull", "--rebase", "origin", "main"])
            else:
                subprocess.run(["git", "pull"])


if __name__ == "__main__":
    main()
