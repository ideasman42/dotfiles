#!/bin/bash
id=$(xdotool getwindowfocus)
if [ -n "$id" ];then
	xprop -id $id -f _SCRATCH 32ii -set _SCRATCH $(date +%s,%N);
	xdotool windowunmap $id;
	# may want to make window floating
fi
