#!/bin/sh
DESK_NUM=$(wmctrl -d | grep -e '^[0-9]\s\+\*' | cut -f1 -d' ')
wmctrl -l | \
	grep -e '^\S\+\s\+'$DESK_NUM | \
	cut -f1 -d' ' | \
	xargs -n1 \
	wmctrl -i -b remove,hidden -r
