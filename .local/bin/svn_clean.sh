#!/bin/bash
svn status --no-ignore \
	| grep '^[?I]' \
	| awk '{print $1, $2}' \
	| grep -v "blender.bin" \
	| grep -v "blender.kdev4" \
	| grep -v "blender_fast.bin" \
	| grep -v "CMakeLists.txt.user" \
	| grep -v "user-config.py" \
	| grep -v ".arcconfig" \
	| xargs rm -rfv

# rm /b/release/scripts/modules/pydna.py
# ln -s /b/intern/tools/pydna.py /b/release/scripts/modules/pydna.py
