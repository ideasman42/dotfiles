#!/usr/bin/env bash

rm -f ~/screenshot.png ~/screenshot.webm
grim -g "$(slurp)" ~/screenshot.png

# Handy, pick high/low quality.
convert ~/screenshot.png ~/screenshot.webm
convert ~/screenshot.png ~/screenshot.jpg

notify-send '~/screenshot.png created!'
