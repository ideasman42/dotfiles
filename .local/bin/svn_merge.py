#!/usr/bin/python3

import sys
import subprocess

def get_merge_info():
    cmd = "svn propget svn:mergeinfo .".split()
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].decode("utf-8")

    root, sep, rev = output.partition(":")
    rev = int(rev.rpartition("-")[2].rpartition(",")[2])

    # example out
    #
    #  /trunk/blender:39992-41932
    root = '/trunk/blender'
    return root, rev

def get_status():
    cmd = "svn status --ignore-externals .".split()
    output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0].decode("utf-8")
    return output.strip().split("\n")

def main():

    print("\nRunning Svn Merge Helper...\n")

    # cheap way to check on status, yeah yeah, should use XML...
    status = get_status()
    status = [l for l in status if l[:2] in {"M\t", "M "}]

    if status:
        print("Error, local changes found\n\n%s" % "\n".join(status))
        print("\nAborting!\n")
        # return

    root, rev = get_merge_info()
    cmd = "svn merge ^%s -r%d:HEAD --accept postpone" % (root, rev)
    print("  %s" % cmd)

    print("\nExecuting...\n")
    subprocess.call(cmd.split())
    print("\n...Done\n")

    root, rev_new = get_merge_info()
    cmd = "svn commit --message \"svn merge ^%s -r%d:%d\"" % (root, rev, rev_new)
    print("  %s" % cmd)


if __name__ == "__main__":
    main()

