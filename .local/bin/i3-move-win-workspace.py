#!/usr/bin/python

# i3, switches next/prev desktops, taking the window, even if the workspace isnt created

import i3
import sys

# --next/prev
delta = 1 if "--next" in sys.argv else -1

ws_ls = i3.get_workspaces()
ws_act = [ws for ws in ws_ls if ws and ws['focused']][0]


ws_store = list(range(1, 11))
ws_other = []
for i, ws in enumerate(ws_ls):
    if ws_act['output'] == ws['output']:
        ws_store[ws['num'] - 1] = ws
    else:
        ws_other.append(ws['num'] - 1)
# remove other screens
for i in reversed(ws_other):
    del ws_store[i]

i = ws_store.index(ws_act)

# get only ws on this monitor
i_next = (len(ws_store) + i + delta) % len(ws_store)
ws_delta = ws_store[i_next]
if type(ws_delta) == int:
    ws_name = str(ws_delta)
else:
    ws_name = ws_delta['name']

i3.move('container to workspace', ws_name)
i3.workspace(ws_name)

