#!/usr/bin/env python3

import ast
import sys


def dump(node, annotate_fields=True, include_attributes=False, indent=0):
    """
    ast.dump from Python3.4 modified for pretty printing.
    """
    from ast import AST, iter_fields

    def _format(node, level):
        if indent:
            level_next = level + 1
            indent_next = (level_next * indent) * " "
            indent_next_line = "\n" + indent_next
            comma_delim = ",\n"
        else:
            level_next = 0
            indent_next = ""
            indent_next_line = ""
            comma_delim = ", "

        if isinstance(node, AST):
            fields = [(a, _format(b, level_next)) for a, b in iter_fields(node)]
            rv = "%s(%s%s" % (
                node.__class__.__name__,
                indent_next_line if len(fields) > 0 else "",
                (comma_delim + indent_next).join(
                (["%s=%s" % field for field in fields] + ([""] if fields else []))
                if annotate_fields else (b for a, b in fields)
            ))
            if include_attributes and node._attributes:
                rv += fields and ", " or " "
                rv += (comma_delim + indent_next).join(
                    "%s=%s" % (a, _format(getattr(node, a), level_next))
                    for a in node._attributes
                )
            return rv + ")"
        elif isinstance(node, list):
            return (
                "[%s%s]" % (
                    indent_next_line if len(node) > 0 else "",
                    (comma_delim + indent_next).join(
                    [_format(x, level_next) for x in node] + ([""] if node else []))
                )
            )
        return repr(node)
    if not isinstance(node, AST):
        raise TypeError("Expected AST, got %r" % node.__class__.__name__)

    if indent < 0:
        raise ValueError("indent must be >= 0")

    return _format(node, 0)


import sys

def str_hash(string):
    import hashlib

    h = hashlib.new('sha512')
    h.update(string.encode("utf-8"))
    return h.hexdigest()


def dump_to_stdout(fh, identifier: str | None, use_hash: bool):
    data = fh.read()
    try:
        tree = ast.parse(data)
    except:
        sys.stderr.write("Unable to parse: %r\n" % (identifier or "<stdin>"))
        return

    if use_hash:
        if identifier is not None:
            print(identifier, str_hash(dump(tree, indent=0)))
        else:
            print(str_hash(dump(tree, indent=0)))
    else:
        print(dump(tree, indent=2))

# find . -name '*.py' -print0 | xargs -0 py_to_ast.py

def main():
    use_hash = False
    if "--hash=sha512" in sys.argv:
        sys.argv.remove("--hash=sha512")
        use_hash = True

    if len(sys.argv) > 1:
        for filepath in sys.argv[1:]:
            with open(filepath, encoding="utf-8") as fh:
                dump_to_stdout(fh, filepath, use_hash)
    else:
        dump_to_stdout(sys.stdin, None, use_hash)

if __name__ == "__main__":
    main()
