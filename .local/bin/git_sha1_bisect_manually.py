#!/usr/bin/env python3

"""

Script to help with manual bisect
  git_sha1_bisect_manually.py a2489e29f6233b407c577a7bbf6400a9b160117d...9ae0e585b0aab466c978ec1a55c824d902faa3b4

Optionally you can pass in a factor

  git_sha1_bisect_manually.py a2489e29f6233b407c577a7bbf6400a9b160117d...9ae0e585b0aab466c978ec1a55c824d902faa3b4 0.2

Where 0.5 is default (the middle sha1)
"""

VERBOSE = False
def run(cmd):
    if VERBOSE:
        print(">>> ", cmd)

    import subprocess
    proc = subprocess.Popen(" ".join(cmd),
                            shell=True,
                            stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE)

    result = proc.stdout.read()
    return result


def main():
    import sys
    argv = sys.argv[:]
    try:
        fac = float(argv[-1])
        del argv[-1]
    except:
        fac = 0.5

    if "..." not in argv[-1]:
        print("Git range not given: sha1start...sha1end")
        return

    data = run((
            "git",
            "log",
            argv[-1],
            "--format=%H",
            ))

    data = data.split(b"\n")
    print("Range is %d" % len(data))
    print("   ", data[int(len(data) * fac)].decode("ascii"))


if __name__ == "__main__":
    main()

