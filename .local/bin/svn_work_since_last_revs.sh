echo "Starting:"

trim() { echo $1; }


SVN_TRUNK="https://svn.blender.org/svnroot/bf-blender"
SVN_EXTEN="https://svn.blender.org/svnroot/bf-extensions"

TEMP_XML="/tmp/svn_tmp.xml"

REV_TRUNK_FROM="$1"
REV_EXTEN_FROM="$2"
REV_TRUNK="$(svn info $SVN_TRUNK | grep 'Rev:' | cut -d':' -f2)"
REV_EXTEN="$(svn info $SVN_EXTEN | grep 'Rev:' | cut -d':' -f2)"
echo "Trunk: $REV_TRUNK_FROM --> $REV_TRUNK"
echo "Ext:   $REV_EXTEN_FROM --> $REV_EXTEN"

REV_TRUNK="$(trim $REV_TRUNK)"
REV_EXTEN="$(trim $REV_EXTEN)"

echo "Getting Logs:"

# SVN_TRUNK_BMESH="$SVN_TRUNK/branches/bmesh/blender"
SVN_TRUNK_PROPE="$SVN_TRUNK/trunk/blender"


svn log $SVN_TRUNK_PROPE --verbose --xml -r$REV_TRUNK_FROM:$REV_TRUNK > $TEMP_XML
cat $TEMP_XML | svn_log_author.py --wiki --author=campbellbarton > log_trunk.txt
echo "Written: log_trunk.txt"

# svn log $SVN_TRUNK_BMESH --verbose --xml -r$REV_TRUNK_FROM:$REV_TRUNK > $TEMP_XML
# cat $TEMP_XML | svn_log_author.py --wiki --author=campbellbarton > log_bmesh.txt
# echo "Written: log_bmesh.txt"

svn log $SVN_EXTEN --verbose --xml  -r$REV_EXTEN_FROM:$REV_EXTEN   > $TEMP_XML
cat $TEMP_XML | svn_log_author.py --wiki --author=campbellbarton > log_ext.txt
echo "Written: log_ext.txt"

rm $TEMP_XML

echo "New Revs: $REV_TRUNK $REV_EXTEN"
