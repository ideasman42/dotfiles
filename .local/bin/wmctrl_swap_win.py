#!/usr/bin/env python3

# first get the active window
import os

VERBOSE = False


def run(cmd):
    if VERBOSE:
        print(">>> ", cmd)

    import subprocess
    proc = subprocess.Popen(cmd,
                            shell=True,
                            stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE)

    result = proc.stdout.read()
    return result


def win_act():
    # sometimes gives values like: 0x2800a21, 0x0
    ret = run(r'xprop -root _NET_ACTIVE_WINDOW | grep -o "0x.*" | cut -f1 -d","')
    ret = ret.strip()
    if ret.startswith(b'0x'):
        return ret.decode('ascii')
    else:
        return ''


def screen_size():
    ret = run("xwininfo -root")

    w = h = -1

    for l in ret.split(b"\n"):
        if w == -1 and l.startswith(b"  Width:"):
            w = int(l.rpartition(b' ')[-1])
        elif h == -1 and l.startswith(b"  Height:"):
            h = int(l.rpartition(b' ')[-1])

        if -1 not in (w, h):
            break

    return w, h


def win_geom(win_id):
    ret = run("xwininfo -all -id " + win_id)

    w = h = x = y = -1
    frame = None

    for l in ret.split(b"\n"):
        if w == -1 and l.startswith(b"  Width:"):
            w = int(l.rpartition(b' ')[-1])
        elif h == -1 and l.startswith(b"  Height:"):
            h = int(l.rpartition(b' ')[-1])
        elif x == -1 and l.startswith(b"  Absolute upper-left X:"):
            x = int(l.rpartition(b' ')[-1])
        elif y == -1 and l.startswith(b"  Absolute upper-left Y:"):
            y = int(l.rpartition(b' ')[-1])
        elif frame is None and l.startswith(b"      Frame extents:"):
            frame = [int(_) for _ in l.strip().rsplit(b": ", 1)[1].split(b", ")]

        if (frame is not None) and (-1 not in (x, y, w, h)):
            break

    if frame is not None:
        # l,r,t,b
        x -= frame[0]
        y -= frame[2]

    return x, y, w, h


def win_geom_set(win_id, x, y, w, h):
    run('wmctrl -i -r ' + win_id + (' -e 1,%d,%d,%d,%d' % (x, y, w, h)))


def win_maximized_get(win_id):
    ret = run('xprop _NET_WM_STATE -id ' + win_id)
    return b'STATE_MAXIMIZED' in ret


def win_maximized_set(win_id, is_maximized):
    ret = run('wmctrl -i -r %s -b %s,maximized_horz,maximized_vert' % (win_id, 'add' if is_maximized else 'remove'))


def win_swap(win_id):

    is_maximized = win_maximized_get(win_id)

    if is_maximized:
        win_maximized_set(win_id, False)

    geom = win_geom(win_id)
    if -1 in geom:
        print("Cant find window geom:", geom)

    x, y, w, h = geom

    sx, sy = screen_size()

    # hard coded
    SCREEN_COUNT = 2

    # swap
    if x + (w // 2) < sx // 2:
        x += sx // 2
    else:
        x -= sx // 2

    win_geom_set(win_id, x, y, w, h)

    if is_maximized:
        win_maximized_set(win_id, True)


if __name__ == "__main__":
    win_id = win_act()
    win_swap(win_id)
