#!/usr/bin/env python3

"""
Continually check for a sha1 string in the clipboard.

Clears terminal and shows diff each time.
"""


VERBOSE = False


def run(cmd, return_result=True):
    if VERBOSE:
        print(">>> ", cmd)

    import subprocess

    if return_result:
        proc = subprocess.Popen(
            cmd,
            shell=False,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE)

        result = proc.stdout.read()
        return result
    else:
        proc = subprocess.Popen(cmd, shell=False)
        proc.wait()


def console_clear():
    import os
    if os.name == "posix":
        # also clears scrollback
        os.system("tput reset")
    else:
        print('\x1b[2J')  # clear


def main():
    import time

    clip_prev = None
    while 1:
        clip_curr = run(["xclip", "-out"])

        # first time only, don't do anything
        if clip_prev is None:
            clip_prev = clip_curr

        if clip_curr != clip_prev:
            clip_text = clip_curr.decode('utf-8')
            clip_text = clip_text.strip(";:\"'`^.,?![]()")
            if clip_text:
                console_clear()
                out = run(["git", "show", clip_text], return_result=False)

        clip_prev = clip_curr
        time.sleep(0.05)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("finishing up!")

