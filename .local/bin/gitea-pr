#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024 Campbell Barton
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""
Apply a patch from GITEA.
"""

import argparse
import os
import shlex
import shutil
import subprocess
import sys
import tempfile

from typing import (
    Optional,
    Sequence,
    Tuple,
)


MODES = ("patch", "diff")

def download_bytes_from_url(url: str) -> Optional[bytes]:
    """
    Download text as bytes or return None on error.
    """
    import urllib.request
    req = urllib.request.Request(url=url)
    data: Optional[bytes] = None
    try:
        with urllib.request.urlopen(req) as fh:
            data = fh.read()
    except BaseException as ex:
        sys.stderr.write("Failed to download URL: %s, error: %r\n" % (url, ex))
    return data


def extract_commit_author_from_patch(patch: str, *, default: str) -> str:
    commit_author = default
    commit_author_prefix = "\nFrom: "
    i = 0
    while (i := patch.find(commit_author_prefix, i)) != -1:
        i += len(commit_author_prefix)
        i_end = patch.find("\n", i)
        line = patch[i:i_end].strip()
        if " <" in line and line.endswith(">"):
            commit_author = line
            break

        if i_end == -1:
            break
        i = i_end + 1
    return commit_author


def extract_commit_message_from_patch(patch: str) -> str:
    commit_messages = []

    search_head = "\nSubject: "
    search_tail = "\n---\n"
    # Commit messages may contain these.
    search_tail_skip = "\n---\n\n"

    i = 0
    while True:
        i = patch.find(search_head, i)
        if i == -1:
            break
        i += len(search_head)

        # Search for `search_tail`, skip `search_tail_skip`.
        i_test = i
        while True:
            i_test = patch.find(search_tail, i_test)
            if i_test == -1:
                i_next = len(patch)
                break
            if patch[i_test : i_test + len(search_tail)+1] != search_tail_skip:
                i_next = i_test
                break
            i_test += len(search_head)
        del i_test

        body = patch[i:i_next].rstrip("\n")

        # Remove single patch prefix.
        if body.startswith("[PATCH] "):
            body = body[8:]
        # Keep multiple (for context) the message needs to be manually edited anyway.
        elif body.startswith("[PATCH "):
            # `[PATCH 1/2]` -> `[1/2]` for brevity.
            body = "[" + body[7:]

        # Unwrap subject which can be split awkwardly onto multiple lines.
        subject_end = body.find("\n\n")
        if subject_end == -1:
            subject_end = len(body)

        subject_wrap = body.find("\n", 0, subject_end)
        if subject_wrap != -1:
            body = "{:s} {:s}".format(
                body[:subject_wrap + 1].rstrip(),
                body[subject_wrap:].lstrip(),
            )

        commit_messages.append(body)
        i = i_next

    return "\n\n---\n\n".join(commit_messages)



def create_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "-b",
        "--base-url",
        # Initialized using GIT when left blank.
        default="",
        required=False,
        metavar="BASE_URL",
        help="The base URL (detected from the \"origin\").",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        default=False,
        required=False,
        action="store_true",
        help="Show extra information abount commands that run.",
    )
    parser.add_argument(
        "-m",
        "--mode",
        default=MODES[0],
        choices=MODES,
        required=False,
        metavar="MODE",
        help=(
            "The method use for downloading the PR.\n"
            "- patch: apply all commits in the PR using \"git am\"\n"
            "- diff: apply the patch as a single diff.\n"
            "  The diff is not committed as a patch."
        ),
    )

    parser.add_argument(
        "pr",
        type=int,
        metavar="PR",
        help="The pull request ID.",
    )

    return parser


def base_url_from_repo() -> Optional[str]:
    # Expects either:
    # - `https://projects.blender.org/blender/blender.git`
    # - `git@projects.blender.org:blender/blender.git`
    # The resulting URL should be:
    # - `https://projects.blender.org/blender/blender`
    base_url = subprocess.check_output(["git", "remote", "get-url", "origin"]).rstrip().decode("utf-8")
    if base_url.startswith("http"):
        if base_url.endswith(".git"):
            base_url = base_url[:-4]
    elif base_url.startswith("git@"):
        base_url = "https://" + base_url[4:].replace(":", "/")
        if base_url.endswith(".git"):
            base_url = base_url[:-4]
    else:
        sys.stderr.write("Unable to find the base URL from: %s\n" % base_url)
        return None
    return base_url


def main(argv: Sequence[str]) -> int:
    args = create_parser().parse_args(argv)
    base_url = args.base_url
    if not base_url:
        base_url = base_url_from_repo()
        if base_url is None:
            return 1

    # Check there are no local changes: (diff with HEAD to accounts for both staged & un-staged changes).
    files_changed = subprocess.check_output(("git", "diff", "HEAD", "--name-only"))
    if files_changed:
        sys.stderr.write("Error, files changed:\n")
        sys.stderr.write(files_changed.decode("utf-8"))
        sys.stderr.write("\n")
        return 1

    # Otherwise a diff.
    is_patch = args.mode == "patch"

    url_full_patch = base_url + "/pulls/%d.patch" % args.pr
    url_full_diff = base_url + "/pulls/%d.diff" % args.pr

    if is_patch:
        url_full = url_full_patch
    else:
        url_full = url_full_diff

    if args.verbose:
        print("Downloading:", url_full)
    patch = download_bytes_from_url(url_full)
    if patch is None:
        return 1

    if not is_patch:
        # Ugh, the diff doesn't have a useful commit message, so extract it from the "patch"
        # even if we don't need it for anything else.
        patch_for_msg = download_bytes_from_url(url_full_patch)
        if patch_for_msg is None:
            return 1
        patch_for_msg_str = patch_for_msg.decode("utf-8", errors="surrogateescape")
        commit_author = extract_commit_author_from_patch(patch_for_msg_str, default="Unknown <>")
        commit_message = extract_commit_message_from_patch(patch_for_msg_str)
        del patch_for_msg, patch_for_msg_str

    with tempfile.NamedTemporaryFile(suffix="_%d.patch" % args.pr) as fh:
        fh.write(patch)
        fh.flush()

        if is_patch:
            cmd = (
                "git",
                "am",
                # Use the date the patch is applied (the date the patch was created isn't so important).
                "--committer-date-is-author-date",
                fh.name,
            )
        else:
            cmd = (
                "git",
                "apply",
                # Use the date the patch is applied (the date the patch was created isn't so important).
                fh.name,
            )

        if args.verbose:
            print("Run:", cmd)

        ok = False
        try:
            subprocess.check_call(cmd)
            ok = True
        except BaseException as ex:
            sys.stderr.write("Failed to apply the patch: %r\n" % ex)

        if ok:
            if is_patch:
                # Add the "Ref !{PR}" text to the bottom of the commit message.
                cmd = (
                    "git",
                    "commit",
                    "--amend",
                )
                subprocess.check_call(
                    cmd,
                    env={
                        **os.environ,
                        "GIT_EDITOR": "".join([
                            """{:s} -c "import sys\n""".format(shlex.quote(sys.executable)),
                            """file = sys.argv[-1]\n""",
                            """with open(file, 'r', encoding='utf-8') as fh: data = fh.read()\n""",
                            """data = '\\n'.join([l for l in data.split('\\n') if not l.startswith('#')])\n""",
                            """data = data.rstrip() + '\\n\\nRef: !{:d}\\n'\n""".format(args.pr),
                            """with open(file, 'w', encoding='utf-8') as fh: fh.write(data)\n" - """,
                        ]),
                    }
                )
                del cmd
            else:
                # FIXME: support adding new files.
                commit_message = "{:s}\n\nRef: !{:d}".format(commit_message, args.pr)
                # Add the "Ref !{PR}" text to the bottom of the commit message.
                cmd = (
                    "git",
                    "commit",
                    "-a",
                    "--author", commit_author,
                    "-m",
                    commit_message,
                )
                subprocess.check_call(
                    cmd,
                    # env=
                    # {
                    #     **os.environ,
                    #     "GIT_EDITOR": "".join([
                    #         """{:s} -c "import sys\n""".format(shlex.quote(sys.executable)),
                    #         """file = sys.argv[-1]\n""",
                    #         """with open(file, 'r', encoding='utf-8') as fh: data = fh.read()\n""",
                    #         """data = '\\n'.join([l for l in data.split('\\n') if not l.startswith('#')])\n""",
                    #         """data = data.rstrip() + '\\n\\nRef: !%d\\n'\n""" % args.pr,
                    #         """with open(file, 'w', encoding='utf-8') as fh: fh.write(data)\n" - """,
                    #     ]),
                    # }
                )
                del cmd

                pass
    return 0


sys.exit(main(sys.argv[1:]))
