#!/bin/sh
mkdir -p ~/Desktop/scan 2> /dev/null
FILE=~/Desktop/scan-web/scan_`date +"%Y_%m_%d_%H_%M_%S"`.png
echo "Scanning to file:" $FILE
scanimage --resolution=300 --mode Color | convert - $FILE
