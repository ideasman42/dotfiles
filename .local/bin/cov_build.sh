
# nicer debugging
set -x

# --------------------------
# Hard coded paths (for now)
BLENDER_SRCDIR="/dsk/src/blender_cov"
BLENDER_BLDDIR="/dsk/src/cmake_cov"
COV_BUILD="/dsk/src/cov-analysis-linux64/bin/cov-build"


# -----------------
# Command line args
MAKE_ARGS="-j6"

# ------
# Update
echo "Updating..."
# svn up --ignore-externals $BLENDER_SRCDIR
cd $BLENDER_SRCDIR
git pull
cd -

# -------------------
# Find Version String

blender_version=$(grep "BLENDER_VERSION\s" $BLENDER_SRCDIR/source/blender/blenkernel/BKE_blender.h | awk '{print $3}')
blender_version_cycle=$(grep BLENDER_VERSION_CYCLE $BLENDER_SRCDIR/source/blender/blenkernel/BKE_blender.h | awk '{print $3}')
blender_subversion=$(grep BLENDER_SUBVERSION $BLENDER_SRCDIR/source/blender/blenkernel/BKE_blender.h | awk '{print $3}')
BLENDER_VERSION=$(expr $blender_version / 100)_$(expr $blender_version % 100)_$blender_subversion

BLENDER_REV=$(git --git-dir=$BLENDER_SRCDIR/.git rev-parse HEAD)

echo "Blender Version: "$BLENDER_VERSION-$BLENDER_REV


# -----
# Build
cd $BLENDER_BLDDIR
rm -rf cov-int
rm blender_archive.*

cmake \
	-B$BLENDER_BLDDIR \
	-H$BLENDER_SRCDIR

make clean

echo "Building..."

make $MAKE_ARGS -C extern/
if [ $? -ne 0 ]; then echo "error -> $?" ; exit 1 ; fi

# other libs we dont want to check
make $MAKE_ARGS bf_editor_datafiles
if [ $? -ne 0 ]; then echo "error -> $?" ; exit 1 ; fi


time $COV_BUILD --dir cov-int \
    make $MAKE_ARGS
if [ $? -ne 0 ]; then echo "error -> $?" ; exit 1 ; fi
echo "Done Building!"


# --------
# Compress
echo "Compressing..."
export GZIP=-9
export XZ_OPT=-9
tar cJf blender_archive.xz cov-int

# ------
# Upload
echo "Uploading "$(du -h blender_archive.xz)"..."

time curl \
  --form token=D5CeCQib \
  --form email=ideasman42@gmail.com \
  --form file=@$BLENDER_BLDDIR/blender_archive.xz \
  --form version=$BLENDER_VERSION-$BLENDER_REV \
  --form description="Description" \
  https://scan.coverity.com/builds?project=blender

echo "Done Uploading!"
