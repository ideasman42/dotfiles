--
-- Notion tiling module configuration file
--

-- Bindings for the tilings.

i42 = {}

-- This function is like 'ioncore.goto_next' but it moves the active frame too
function i42.moveto_next(region, direction, param)
    ioncore.tagged_clear()
    subregion = WRegion.current(region)
    WRegion.set_tagged(subregion, 'set')
    region_new = ioncore.goto_next(region, direction, param)
    ioncore.tagged_attach(region_new, param)
    -- warp. else cursor is left behind
    WRegion.goto_(subregion)
end

defbindings("WTiling", {
    bdoc("Split current frame vertically."),
    kpress(META.."backslash", "WTiling.split_at(_, _sub, 'right', true)"),
    kpress(META.."Shift+backslash", "WTiling.split_at(_, _sub, 'bottom', true)"),

    -- Just use application quit key. works fine
    -- kpress(META.."Shift+X", "WTiling.unsplit_at(_, _sub)"),
    --
    -- kpress(META.."Up", "ioncore.goto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."Shift+Up",    "i42.moveto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."Shift+Down",  "i42.moveto_next(_sub, 'down', {no_ascend=_})"),
    kpress(META.."Shift+Left",  "i42.moveto_next(_sub, 'left', {no_ascend=_})"),
    kpress(META.."Shift+Right", "i42.moveto_next(_sub, 'right', {no_ascend=_})"),
    -- vim!
    kpress(META.."Shift+K", "i42.moveto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."Shift+J", "i42.moveto_next(_sub, 'down', {no_ascend=_})"),
    kpress(META.."Shift+H", "i42.moveto_next(_sub, 'left', {no_ascend=_})"),
    kpress(META.."Shift+L", "i42.moveto_next(_sub, 'right', {no_ascend=_})"),

    -- XF86Launch6, is mapped to Esc
    -- XF86Launch7, is mapped to pause play
    -- kpress("XF86Launch8", "ioncore.goto_next(_sub, 'right', {no_ascend=_})"),

    bdoc("Go to frame above/below current frame."),
    kpress(META.."Up",    "ioncore.goto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."Down",  "ioncore.goto_next(_sub, 'down', {no_ascend=_})"),
    kpress(META.."Left",  "ioncore.goto_next(_sub, 'left', {no_ascend=_})"),
    kpress(META.."Right", "ioncore.goto_next(_sub, 'right', {no_ascend=_})"),
    -- vim!
    kpress(META.."K", "ioncore.goto_next(_sub, 'up', {no_ascend=_})"),
    kpress(META.."J", "ioncore.goto_next(_sub, 'down', {no_ascend=_})"),
    kpress(META.."H", "ioncore.goto_next(_sub, 'left', {no_ascend=_})"),
    kpress(META.."L", "ioncore.goto_next(_sub, 'right', {no_ascend=_})"),

    -- works with xinerama off
    kpress(META.."Shift+Z", "WTiling.flip_at(_, _sub)"),
    -- kpress(META.."Z", "WTiling.flip_at(_)"),
    kpress(META.."Z", "i42.moveto_next(_sub, 'right', {no_ascend=_})"),
    -- flip entire workspace
    kpress(META.."Control+Z", "WTiling.flip_at(_)"),


    -- unsplit (merge)
    kpress(META.."Shift+X", "WTiling.unsplit_at(_, _sub)"),
--[[
    bdoc("Go to frame right/left of current frame."),
    kpress(META.."Tab", "ioncore.goto_next(_sub, 'right')"),
    submap(META.."K", {
        kpress("Tab", "ioncore.goto_next(_sub, 'left')"),

        bdoc("Split current frame horizontally."),
        kpress("S", "WTiling.split_at(_, _sub, 'right', true)"),

        bdoc("Destroy current frame."),
        kpress("X", "WTiling.unsplit_at(_, _sub)"),
    }),
--]]
})

-- Frame bindings.

--[[
defbindings("WFrame.floating", {
    submap(META.."K", {
        bdoc("Tile frame, if no tiling exists on the workspace"),
        kpress("B", "mod_tiling.mkbottom(_)"),
    }),
})
  ]]


-- Context menu for tiled workspaces.
defctxmenu("WTiling", "Tiling", {
    menuentry(
        "Destroy frame",
        "WTiling.unsplit_at(_, _sub)"),

    menuentry(
        "Split vertically",
        "WTiling.split_at(_, _sub, 'bottom', true)"),
    menuentry(
        "Split horizontally",
        "WTiling.split_at(_, _sub, 'right', true)"),

    menuentry(
        "Flip", "WTiling.flip_at(_, _sub)"),
    menuentry("Transpose", "WTiling.transpose_at(_, _sub)"),

    menuentry("Untile", "mod_tiling.untile(_)"),

    submenu("Float split", {
        menuentry(
            "At left",
            "WTiling.set_floating_at(_, _sub, 'toggle', 'left')"),
        menuentry(
            "At right",
            "WTiling.set_floating_at(_, _sub, 'toggle', 'right')"),
        menuentry(
            "Above",
            "WTiling.set_floating_at(_, _sub, 'toggle', 'up')"),
        menuentry(
            "Below",
            "WTiling.set_floating_at(_, _sub, 'toggle', 'down')"),
    }),

    submenu("At root", {
        menuentry(
            "Split vertically",
            "WTiling.split_top(_, 'bottom')"),
        menuentry(
            "Split horizontally",
            "WTiling.split_top(_, 'right')"),
        menuentry("Flip", "WTiling.flip_at(_)"),
        menuentry("Transpose", "WTiling.transpose_at(_)"),
    }),
})

-- Extra context menu extra entries for floatframes.
defctxmenu("WFrame.floating", "Floating frame", {
    append = true,
    menuentry("New tiling", "mod_tiling.mkbottom(_)"),
})
