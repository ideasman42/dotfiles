--
-- Options to get some programs work more nicely (or at all)
--

-- Better would be to apply these settings automatically whenever a window
-- has type _NET_WM_WINDOW_TYPE_NOTIFICATION.
defwinprop {
    class = "Xfce4-notifyd",
    float = true,
    userpos = true,
    switchto = false,
}

defwinprop {
    class = "stalonetray",
    float = true,
    userpos = false,
    switchto = false,
    transparent = true,
    transient_mode = "off",
    statusbar = "systray",
}

defwinprop {
    -- class = "mate-panel",
    name = "Top Panel",
    switchto = false,
    float = true,
}

-- Put all dockapps in the statusbar's systray, also adding the missing size
-- hints necessary for this to work.
defwinprop {
    is_dockapp = true,
    statusbar = "systray",
    max_size = { w = 64, h = 24},
    min_size = { w = 64, h = 24},
}

-- Make an exception for Docker, which sets correct size hints.
defwinprop {
    is_dockapp = true,
    class = "Docker",
    statusbar = "systray",
}

-- Define some additional title shortening rules to use when the full title
-- doesn't fit in the available space. The first-defined matching rule that
-- succeeds in making the title short enough is used.
ioncore.defshortening("(.*) - Mozilla(<[0-9]+>)", "$1$2$|$1$<...$2")
ioncore.defshortening("(.*) - Mozilla", "$1$|$1$<...")
ioncore.defshortening("[^:]+: (.*)(<[0-9]+>)", "$1$2$|$1$<...$2")
ioncore.defshortening("[^:]+: (.*)", "$1$|$1$<...")
ioncore.defshortening("(.*)(<[0-9]+>)", "$1$2$|$1$<...$2")
ioncore.defshortening("(.*)", "$1$|$1$<...")
