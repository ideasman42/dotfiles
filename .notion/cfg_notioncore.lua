--
-- Notion core configuration file
--


--
-- Bindings. This includes global bindings and bindings common to
-- screens and all types of frames only. See modules' configuration
-- files for other bindings.
--


-- WScreen context bindings
--
-- The bindings in this context are available all the time.
--
-- The variable META should contain a string of the form 'Mod4+'
-- where Mod4 maybe replaced with the modifier you want to use for most
-- of the bindings. Similarly ALTMETA may be redefined to add a
-- modifier to some of the F-key bindings.

defbindings("WScreen", {
    bdoc("Switch to n:th object (workspace, full screen client window) "..
         "within current screen."),
    kpress(META.."1", "WScreen.switch_nth(_, 0)"),
    kpress(META.."2", "WScreen.switch_nth(_, 1)"),
    kpress(META.."3", "WScreen.switch_nth(_, 2)"),
    kpress(META.."4", "WScreen.switch_nth(_, 3)"),
    kpress(META.."5", "WScreen.switch_nth(_, 4)"),
    kpress(META.."6", "WScreen.switch_nth(_, 5)"),
    kpress(META.."7", "WScreen.switch_nth(_, 6)"),
    kpress(META.."8", "WScreen.switch_nth(_, 7)"),
    kpress(META.."9", "WScreen.switch_nth(_, 8)"),
    kpress(META.."0", "WScreen.switch_nth(_, 9)"),

    bdoc("Switch to next/previous object within current screen."),
    kpress(META.."Next", "WScreen.switch_prev(_)"),
    kpress(META.."Prior", "WScreen.switch_next(_)"),
    -- vim style keybindings
    kpress(META.."Control+j", "WScreen.switch_prev(_)"),
    kpress(META.."Control+k", "WScreen.switch_next(_)"),
    -- kpress("XF86Launch7", "WScreen.switch_next(_)"),

--[[
    submap(META.."K", {
        bdoc("Go to first region demanding attention or previously active one."),
        kpress("K", "mod_menu.grabmenu(_, _sub, 'focuslist')"),
        -- Alternative without (cyclable) menu
        --kpress("K", "ioncore.goto_activity() or ioncore.goto_previous()"),

        --bdoc("Go to previous active object."),
        --kpress("K", "ioncore.goto_previous()"),

        --bdoc("Go to first object on activity/urgency list."),
        --kpress("I", "ioncore.goto_activity()"),

        bdoc("Clear all tags."),
        kpress("T", "ioncore.tagged_clear()"),
    }),
   ]]

--[[
    bdoc("Go to n:th screen on multihead setup."),
    kpress(META.."Shift+1", "ioncore.goto_nth_screen(0)"),
    kpress(META.."Q", "ioncore.goto_nth_screen(0)"),
    kpress(META.."Shift+2", "ioncore.goto_nth_screen(1)"),
    kpress(META.."W", "ioncore.goto_nth_screen(1)"),
    kpress(META.."Shift+3", "ioncore.goto_nth_screen(2)"),
    kpress(META.."E", "ioncore.goto_nth_screen(2)"),

    bdoc("Go to next/previous screen on multihead setup."),
    kpress(META.."Shift+comma", "ioncore.goto_prev_screen()"),
    kpress(META.."I", "ioncore.goto_prev_screen()"),
    kpress(META.."O", "ioncore.goto_next_screen()"),
    kpress(META.."grave", "ioncore.goto_next_screen()"),
  ]]

    bdoc("Create a new workspace of chosen default type."),
    kpress(META.."Control+Return", "ioncore.create_ws(_)"),
    kpress(META.."Control+Shift+Return", "mod_query.query_workspace(_)"),

    -- bdoc("Display the main menu."),
    -- kpress(ALTMETA.."F12", "mod_query.query_menu(_, _sub, 'mainmenu', 'Main menu:')"),
    --kpress(ALTMETA.."F12", "mod_menu.menu(_, _sub, 'mainmenu', {big=true})"),
    mpress("Button3", "mod_menu.pmenu(_, _sub, 'mainmenu')"),

    bdoc("Display the window list menu."),
    mpress("Button2", "mod_menu.pmenu(_, _sub, 'windowlist')"),
    kpress(META.."Tab", "mod_menu.grabmenu(_, _sub, 'windowlist', {big=true})"),
    kpress("F13", "mod_menu.menu(_, _sub, 'windowlist', {big=true})"),

    bdoc("Forward-circulate focus."),
    -- '_chld' used here stands to for an actual child window that may not
    -- be managed by the screen itself, unlike '_sub', that is likely to be
    -- the managing group of that window. The right/left directions are
    -- used instead of next/prev, because they work better in conjunction
    -- with tilings.
--[[
    kpress(META.."Tab", "ioncore.goto_next(_chld, 'right')",
           "_chld:non-nil"),
    submap(META.."K", {
        bdoc("Backward-circulate focus."),
        kpress("AnyModifier+Tab", "ioncore.goto_next(_chld, 'left')",
               "_chld:non-nil"),

        bdoc("Raise focused object, if possible."),
        kpress("AnyModifier+R", "WRegion.rqorder(_chld, 'front')",
               "_chld:non-nil"),
    }),
--]]
})


-- Client window bindings
--
-- These bindings affect client windows directly.

--[[
defbindings("WClientWin", {
    submap(META.."K", {
       bdoc("Nudge the client window. This might help with some "..
         "programs' resizing problems."),
       kpress_wait(META.."L", "WClientWin.nudge(_)"),

       bdoc("Kill client owning the client window."),
       kpress("C", "WClientWin.kill(_)"),

       bdoc("Send next key press to the client window. "..
            "Some programs may not allow this by default."),
       kpress("Q", "WClientWin.quote_next(_)"),
    }),
})
  ]]


-- Client window group bindings

defbindings("WGroupCW", {
    bdoc("Toggle client window group full-screen mode"),
    kpress(META.."F", "WGroup.set_fullscreen(_, 'toggle')"),
})


-- WMPlex context bindings
--
-- These bindings work in frames and on screens. The innermost of such
-- contexts/objects always gets to handle the key press.

defbindings("WMPlex", {
    bdoc("Close current object."),
    kpress(META.."Q", "WRegion.rqclose_propagate(_, _sub)"),
})

-- Frames for transient windows ignore this bindmap
defbindings("WMPlex.toplevel", {
    bdoc("Toggle tag of current object."),
    kpress(META.."T", "WRegion.set_tagged(_sub, 'toggle')", "_sub:non-nil"),

--[[
    bdoc("Lock screen"),
    kpress(META.."L", "notioncore.exec_on(_, notioncore.lookup_script('notion-lock'))"),

    bdoc("Query for manual page to be displayed."),
    kpress(ALTMETA.."F1", "mod_query.query_man(_, ':man')"),

    bdoc("Show the Notion manual page."),
    kpress(META.."F1", "ioncore.exec_on(_, ':man notion')"),
--]]

    bdoc("Run a terminal emulator."),
    kpress(META.."Return", "mod_query.exec_on_merr(_, XTERM or 'xterm')"),

    bdoc("DMenu."),
    kpress(META.."P", "mod_query.exec_on_merr(_, 'dmenu_custom')"),
    kpress(META.."Control+P", "mod_query.exec_on_merr(_, 'dmenu_custom_system')"),

    bdoc("Query for command line to execute."),
    kpress(META.."Shift+P", "mod_query.query_exec(_)"),

    -- take that websites that don't let you paste!
    bdoc("Simulate MMB"),
    -- kpress(META.."V", "mod_query.exec_on_merr(_, 'xdotool getwindowfocus key --window %1 click 2')"),
    kpress(META.."V", "mod_query.exec_on_merr(_, 'x11_fake_paste.sh')"),

    bdoc("Close notification."),
    kpress(META.."Escape", "mod_query.exec_on_merr(_, 'dunst_restart')"),

--[[
    bdoc("Query for Lua code to execute."),
    kpress(META.."F3", "mod_query.query_lua(_)"),
--]]

    bdoc("Music."),
    -- kpress("F14", "mod_query.exec_on_merr(_, 'audacious --play-pause')"),
    kpress("F14", "mod_query.exec_on_merr(_, 'tts_clipboard')"),
    kpress("Shift+F14", "mod_query.exec_on_merr(_, 'audacious --fwd')"),

    kpress("XF86Search", "mod_query.exec_on_merr(_, 'dmenu_custom_context_menu')"),

    -- Media keys (if we have them)
    kpress("XF86AudioPlay", "mod_query.exec_on_merr(_, 'audacious --play-pause')"),
    kpress("XF86AudioPrev", "mod_query.exec_on_merr(_, 'audacious --rew')"),
    kpress("XF86AudioNext", "mod_query.exec_on_merr(_, 'audacious --fwd')"),
    kpress("XF86AudioRewind",  "mod_query.exec_on_merr(_, 'audacious-seek -')"),
    kpress("XF86AudioForward", "mod_query.exec_on_merr(_, 'audacious-seek +')"),

    -- kpress("XF86AudioRaiseVolume", "mod_query.exec_on_merr(_, 'amixer sset Master 5%+')"),
    -- kpress("XF86AudioLowerVolume", "mod_query.exec_on_merr(_, 'amixer sset Master 5%-')"),

    -- pulse audio
    kpress("XF86AudioRaiseVolume", "mod_query.exec_on_merr(_, 'pactl set-sink-volume $(pactl list short sinks | head -n1 | cut -f1) +5%')"),
    kpress("XF86AudioLowerVolume", "mod_query.exec_on_merr(_, 'pactl set-sink-volume $(pactl list short sinks | head -n1 | cut -f1) -5%')"),

    bdoc("Clock."),
    kpress(META.."F10", "mod_query.exec_on_merr(_, 'xosd_time')"),

    bdoc("Quit Notion."),
    kpress(META.."Shift+Q", "ioncore.shutdown()"),

    bdoc("Restart Notion."),
    kpress(META.."Shift+R", "ioncore.restart()"),


    -- laptop only!
    bdoc("Laptop keys."),
    kpress("XF86MonBrightnessUp",   "mod_query.exec_on_merr(_, 'xbacklight -inc 10')"),
    kpress("XF86MonBrightnessDown", "mod_query.exec_on_merr(_, 'xbacklight -dec 10')"),

    -- invert colors on current screen!
    kpress(META.."I", "mod_query.exec_on_merr(_, 'xcalib -invert -alter -s $(wmctrl_current_screen.sh)')"),

    -- bdoc("Query for Lua code to execute."),
    -- kpress(META.."F3", "mod_query.query_lua(_)"),

    -- bdoc("Query for host to connect to with SSH."),
    -- kpress(ALTMETA.."F4", "mod_query.query_ssh(_, ':ssh')"),

    -- bdoc("Query for file to edit."),
    -- kpress(ALTMETA.."F5",
    --        "mod_query.query_editfile(_, 'run-mailcap --action=edit')"),

    -- bdoc("Query for file to view."),
    -- kpress(ALTMETA.."F6",
    --        "mod_query.query_runfile(_, 'run-mailcap --action=view')"),

    -- bdoc("Query for workspace to go to or create a new one."),
    -- kpress(ALTMETA.."F9", "mod_query.query_workspace(_)"),

    bdoc("Query for a client window to go to."),
    kpress(META.."slash", "mod_query.query_gotoclient(_)"),

    bdoc("Minimize/Restore"),
    kpress(META.."W", "mod_query.exec_on_merr(_, 'wmctrl_scratchpad.sh minimize')"),
    kpress(META.."Shift+W", "mod_query.exec_on_merr(_, 'wmctrl_scratchpad.sh restore')"),

    -- My keyboard has this listed as:
    -- Only the command is executed without selecting a monitor (`mod_query.exec_on_merr`)
    -- because this causes conflict in FIREFOX, which loses focus in the text field.
    kpress("F15", function() ioncore.exec('nerd-dictation-begin --full-sentence') end),
    kpress("F16", function() ioncore.exec('nerd-dictation-end') end),

    kpress("F17", function() ioncore.exec('nerd-dictation-begin') end),
    kpress("F18", function() ioncore.exec('nerd-dictation-end') end),

--[[

    bdoc("Display context menu."),
    --kpress(META.."M", "mod_menu.menu(_, _sub, 'ctxmenu')"),
    kpress(META.."M", "mod_query.query_menu(_, _sub, 'ctxmenu', 'Context menu:')"),

    submap(META.."K", {
        bdoc("Detach (float) or reattach an object to its previous location."),
        -- By using _chld instead of _sub, we can detach/reattach queries
        -- attached to a group. The detach code checks if the parameter
        -- (_chld) is a group 'bottom' and detaches the whole group in that
        -- case.
        kpress("D", "ioncore.detach(_chld, 'toggle')", "_chld:non-nil"),
    }),
--]]

    kpress(META.."X", "ioncore.detach(_chld, 'toggle')", "_chld:non-nil"),
})


-- WFrame context bindings
--
-- These bindings are common to all types of frames. Some additional
-- frame bindings are found in some modules' configuration files.

defbindings("WFrame", {

    -- ideasman42: Cool. but not that useful
--[[
    submap(META.."K", {
        bdoc("Maximize the frame horizontally/vertically."),
        kpress("H", "WFrame.maximize_horiz(_)"),
        kpress("V", "WFrame.maximize_vert(_)"),
    }),
--]]

    -- - disabling this is weak, ideasman42
    -- bdoc("Maximize (fullscreen)."),
    -- kpress(META.."f", "WFrame.maximize_horiz(_); WFrame.maximize_vert(_)"),

    bdoc("Display context menu."),
    mpress("Button3", "mod_menu.pmenu(_, _sub, 'ctxmenu')"),

    -- ideasman42: Disable this, seems not very useful
    -- bdoc("Begin move/resize mode."),
    -- kpress(META.."R", "WFrame.begin_kbresize(_)"),

    bdoc("Switch the frame to display the object indicated by the tab."),
    mclick("Button1@tab", "WFrame.p_switch_tab(_)"),
    mclick("Button2@tab", "WFrame.p_switch_tab(_)"),

    bdoc("Resize the frame."),
    mdrag("Button1@border", "WFrame.p_resize(_)"),
    mdrag(META.."Button1", "WFrame.p_resize(_)"),

    bdoc("Move the frame."),
    mdrag(META.."Button3", "WFrame.p_move(_)"),

    bdoc("Move objects between frames by dragging and dropping the tab."),
    mdrag("Button1@tab", "WFrame.p_tabdrag(_)"),
    mdrag("Button2@tab", "WFrame.p_tabdrag(_)"),

    bdoc("Switch to next/previous object within the frame."),
    mclick(META.."Button4", "WFrame.switch_next(_)"),
    mclick(META.."Button5", "WFrame.switch_prev(_)"),

    kpress(ALTMETA.."Tab", "WFrame.switch_next(_)"),
    kpress(ALTMETA.."Shift+Tab", "WFrame.switch_prev(_)"),
--     kpress(META.."Tab", "ioncore.goto_next(_chld, 'right')",

})

-- Frames for transient windows ignore this bindmap

defbindings("WFrame.toplevel", {
    bdoc("Query for a client window to attach."),
    kpress(META.."Shift+A", "mod_query.query_attachclient(_)"),
    kpress(META.."A", "ioncore.tagged_attach(_)")

--[[
    submap(META.."K", {
        -- Display tab numbers when modifiers are released
        submap_wait("ioncore.tabnum.show(_)"),

        bdoc("Switch to n:th object within the frame."),
        kpress("1", "WFrame.switch_nth(_, 0)"),
        kpress("2", "WFrame.switch_nth(_, 1)"),
        kpress("3", "WFrame.switch_nth(_, 2)"),
        kpress("4", "WFrame.switch_nth(_, 3)"),
        kpress("5", "WFrame.switch_nth(_, 4)"),
        kpress("6", "WFrame.switch_nth(_, 5)"),
        kpress("7", "WFrame.switch_nth(_, 6)"),
        kpress("8", "WFrame.switch_nth(_, 7)"),
        kpress("9", "WFrame.switch_nth(_, 8)"),
        kpress("0", "WFrame.switch_nth(_, 9)"),

        bdoc("Switch to next/previous object within the frame."),
        kpress("N", "WFrame.switch_next(_)"),
        kpress("P", "WFrame.switch_prev(_)"),

        bdoc("Move current object within the frame left/right."),
        kpress("comma", "WFrame.dec_index(_, _sub)", "_sub:non-nil"),
        kpress("period", "WFrame.inc_index(_, _sub)", "_sub:non-nil"),

        bdoc("Maximize the frame horizontally/vertically."),
        kpress("H", "WFrame.maximize_horiz(_)"),
        kpress("V", "WFrame.maximize_vert(_)"),

        bdoc("Attach tagged objects to this frame."),
        kpress("A", "ioncore.tagged_attach(_)"),
    }),
  ]]
})

-- Bindings for floating frames

defbindings("WFrame.floating", {
    bdoc("Toggle shade mode"),
    mdblclick("Button1@tab", "WFrame.set_shaded(_, 'toggle')"),

    bdoc("Raise the frame."),
    mpress("Button1@tab", "WRegion.rqorder(_, 'front')"),
    mpress("Button1@border", "WRegion.rqorder(_, 'front')"),
    mclick(META.."Button3", "WRegion.rqorder(_, 'front')"),

    bdoc("Lower the frame."),
    mclick(META.."Button2", "WRegion.rqorder(_, 'back')"),

    bdoc("Move the frame."),
    mdrag("Button1@tab", "WFrame.p_move(_)"),
})


--
-- Menu definitions
--

-- Main menu
defmenu("mainmenu", {
    menuentry("Run...",         "mod_query.query_exec(_)"),
    menuentry("Terminal",       "mod_query.exec_on_merr(_, XTERM or 'xterm')"),
    menuentry("Help",           "mod_query.query_man(_)"),
    menuentry("About Notion",      "mod_query.show_about_ion(_)"),
    submenu("Styles",           "stylemenu"),
    submenu("Session",          "sessionmenu"),
})


-- Session control menu
defmenu("sessionmenu", {
    menuentry("Save",           "ioncore.snapshot()"),
    menuentry("Restart",        "ioncore.restart()"),
    menuentry("Restart TWM",    "ioncore.restart_other('twm')"),
    menuentry("Exit",           "ioncore.shutdown()"),
})


-- Context menu (frame actions etc.)
defctxmenu("WFrame", "Frame", {
    -- Note: this propagates the close to any subwindows; it does not
    -- destroy the frame itself, unless empty. An entry to destroy tiled
    -- frames is configured in cfg_tiling.lua.
    menuentry("Close",          "WRegion.rqclose_propagate(_, _sub)"),
    -- Low-priority entries
    menuentry("Attach tagged", "ioncore.tagged_attach(_)", { priority = 0 }),
    menuentry("Clear tags",    "ioncore.tagged_clear()", { priority = 0 }),
    menuentry("Window info",   "mod_query.show_tree(_, _sub)", { priority = 0 }),
})


-- Context menu for groups (workspaces, client windows)
defctxmenu("WGroup", "Group", {
    menuentry("Toggle tag",     "WRegion.set_tagged(_, 'toggle')"),
    menuentry("De/reattach",    "ioncore.detach(_, 'toggle')"),
})


-- Context menu for workspaces
defctxmenu("WGroupWS", "Workspace", {
    menuentry("Close",          "WRegion.rqclose(_)"),
    menuentry("Rename",         "mod_query.query_renameworkspace(nil, _)"),
    menuentry("Attach tagged",  "ioncore.tagged_attach(_)"),
})


-- Context menu for client windows
defctxmenu("WClientWin", "Client window", {
    menuentry("Kill",           "WClientWin.kill(_)"),
})
