#!/bin/zsh

#
# :tocdepth: 3
#
# %%%%%%%%%%%%%
# Z Shell (Zsh)
# %%%%%%%%%%%%%

# Misc Opts
#

# type paths direct
# setopt autocd


# expand cd
#

autoload -U compinit
compinit -i

# arrow key select
#
#~ zstyle ':completion:*' menu select
#~ setopt menu_complete

# rather user bash style (avoids backspacing wrong completion!)
zstyle ':completion:::*:default' menu no select

# complete as much of a completion until it gets ambiguous.
setopt list_ambiguous

# don't ask me! 'rm *'
setopt rmstarsilent

# don't attempt to expand globbing args (annoying)
unsetopt nomatch

is_interactive=""
if [[ $- == *i* ]]; then
    is_interactive="1"
fi


# -------
# Plugins
# #######

# Interactive mode only
if [ "$is_interactive" -eq '1' ]; then
    if [ "$(uname)" == "Linux" ]; then
        # Syntax highlighting: (zsh-syntax-highlighting)
        ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
        source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
        # Regular blue is too dark.
        ZSH_HIGHLIGHT_STYLES[globbing]='fg=blue,bold'

        # Git prompt (comes with git)
        source /usr/share/git/completion/git-prompt.sh

        # Show auto-completion as you type, like fish: (zsh-autosuggestions)
        ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=236'

        source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

        # Search (fzf)
        source /usr/share/fzf/key-bindings.zsh
        FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS --height=100% +i"
        zle -N fzf-file-widget
        zle -N fzf-history-widget
    else
        # For macOS.
        __git_ps1 () {
            __git_branch="$(git -C '.' rev-parse 2>/dev/null && git branch --show-current)"
            if [[ -n __git_branch ]]; then
                printf " (%s)" $__git_branch
            fi
        }

        eval "$(fzf --zsh)"
    fi
fi


# ------------
# Key Bindings
# ############

# Interactive mode only
if [ "$is_interactive" -eq '1' ]; then

    # emacs mode by default
    bindkey -e

    # -----------
    # Unbind Keys
    # ===========

    bindkey -r "^W"         # unbind Ctrl-W

    # for separator
    WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

    # delimit dirs when killing words
    autoload -U select-word-style
    select-word-style bash

    bindkey  "^[[H"   beginning-of-line  # home
    bindkey  "^[[F"   end-of-line  # end

    # and again for tmux
    bindkey  "^[[1~"   beginning-of-line  # home
    bindkey  "^[[4~"   end-of-line  # end

    bindkey "\e[3~"   delete-char  # delete

    bindkey '\e[1;5C' forward-word            # C-Right
    bindkey '\e[1;5D' backward-word           # C-Left

    # search history
    # bindkey '^R' history-incremental-search-backward

    # vim up/down, avoid using arrows
    bindkey '^K' up-history
    bindkey '^J' down-history

    # typical line editing keys
    # bindkey '^?' backward-kill-word  # Ctrl+Backspace
    # bindkey '^[^?' backward-kill-word
    # bindkey '^[[3^' backward-kill-word

    # Tested on 'st' and 'urxvt'

    # Ctrl-Delete: forward kill word
    bindkey '^[[3;5~'  kill-word
    # Ctrl-BackSpace
    # note! - this breaks xterm and bsd's, works for alt+backspace too
    bindkey '^H' backward-kill-word

    # FZF keys
    bindkey -M emacs -r '^F'
    bindkey -M emacs '^F' fzf-file-widget

    bindkey -M emacs -r '^R'
    bindkey -M emacs '^R' fzf-history-widget

    fzf-complete-from-tmux-widget() {
        LBUFFER="${LBUFFER}$(fzf-complete-from-tmux.sh)"
        local ret=$?
        zle reset-prompt
        return $ret
    }

    zle     -N fzf-complete-from-tmux-widget
    bindkey -M emacs '^N' fzf-complete-from-tmux-widget

    # Ctrl-N: Complete based on the tmux buffer content.
    # bind \cn "commandline -i (fzf-complete-from-tmux.sh) 2>/dev/null"


    # ------------
    # Shell Prompt
    # ############

    if [ "$is_interactive" -eq '1' ]; then
        setopt PROMPT_SUBST
        if [ "$EUID" -ne 0 ]; then
            PS1=\
"$(tput setab 234) %{%F{cyan}%}%~%{%f%}"\
"%{%F{green}%}\$(__git_ps1)%{%f%} $(tput setab 0)"\
$'\n'\
" %F{white}%}>%{%f%} "
        else
            PS1="%{%F{red}%}%~%{%f%} %F{white}%}>%{%f%} "
        fi

        export PS2=$PS1
    fi
fi


# --------------
# Shared History
# ==============

export HISTFILE=~/.zsh_history
export HISTSIZE=1000000
export SAVEHIST=$HISTSIZE
export HISTORY_IGNORE="(ls|cd|cd ..)"

setopt inc_append_history
setopt share_history
setopt hist_ignore_all_dups
setopt hist_ignore_space  # hide starting with space
setopt hist_reduce_blanks # trim blanks

# do not overwrite new entries, but append them to the history file
setopt append_history
# save and fetch the entries of multiple instances of the ZSH correctly
# ... disable this since it causes time to be added as prefix to each entry.
# setopt extended_history

# Load in history
if [ -f "$HOME/.config/any_shell/history_permanent" ]; then
    fc -R "$HOME/.config/any_shell/history_permanent"
fi

#~ ideasman42: not happy with command_not_found_handler, prints nothing!
# source /usr/share/doc/pkgfile/command-not-found.zsh
#~ add 'else'
if [ "$is_interactive" -eq '1' ]; then
    if [ -f "/etc/pacman.conf" ]; then
        command_not_found_handler() {
            local pkgs cmd="$1"
            pkgs=(${(f)"$(pkgfile -b -v -- "$cmd" 2>/dev/null)"})
            if [[ -n "$pkgs" ]]; then
                printf '%s may be found in the following packages:\n' "$cmd"
                printf '  %s\n' $pkgs[@]
                return 0
            else
                printf 'zsh: command not found: %s\n' "$cmd"
            fi
            return 127
        }
    fi
fi

# --------------------
# Terminal Title (X11)
# ####################

if [ "$is_interactive" -eq '1' ]; then
    case $TERM in
        xterm*|rxvt*)
            precmd () {
                print -Pn "\e]0;zsh: [ %~ ]\a"
            }
            ;;
        tmux*)
            precmd () {
                ## Simple case
                # tmux rename-window "zsh:($PWD) "

                ## Use abbreviated "$PWD": '/a../b../.c../directory'
                ## first letter except for when it begins with a '.'.
                ## From: https://unix.stackexchange.com/questions/247004
                tmux rename-window \
                    "zsh:($(awk -F/ '{for (i=1;i<NF;i++) $i=substr($i,1,1+($i~/^[.]/))(i==1||length($i)<=1?"":"‥")} 1' OFS=/ <<<$PWD))"
                print -Pn "\e]0;zsh: [ %~ ]\a"
            }
            ;;
    esac
fi

unset is_interactive

# shared aliases and exports
if [ -f ~/.config/any_shell/rc.sh ]; then
    source ~/.config/any_shell/rc.sh
fi

# STARTX
# [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx -- -quiet
